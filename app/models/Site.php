<?php
 
class Site extends Eloquent {
  protected $table = 'sites';

  public static function getFirstSiteId() {
    $site = DB::table('sites')->first();
    return $site->id;
  }

  public static function getSiteId($site){
  	$siteId = Site::getFirstSiteId();
  	switch ($site) {
  		case 'TU Clausthal':
  			return $siteId;
  			break;

  		case 'TU Braunschweig':
  			return $siteId + 1;
  			break;
  		
  		case 'University of Göttingen':
  			return $siteId + 2;
  			break;
  		
  		case 'Leibniz University of Hanover':
  			return $siteId + 3;
  			break;
  		
  		default:
  			return 0;
  			break;
  	}
  }
    
  // Get name of the site using its id
  public static function getSiteName($id) {
    return Site::where('id', $id)->pluck('site_name');
	}
  public static function getUniName($id) {
    return Site::where('id', $id)->pluck('site_name');
  }
	
	// To take all site names into an array from site table
    // this array display in a dropdown list 
	public static function makeSiteArray() {
		
		$sites = DB::table('sites')->get();
		$siteToPush[''] = 'Site';

		foreach ($sites as $site) {
			$siteToPush[$site->id] = $site->site_name;
		}
		return $siteToPush;
	}
}
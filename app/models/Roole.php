<?php
 
class Roole extends Eloquent {
  
  //this is to maintain the users_rooles pivot table
  public function users() {
    return $this->belongsToMany('User', 'users_rooles');
  }

  //this is to maintain the laws_rooles pivot table
  public function laws() {
    return $this->belongsToMany('Law', 'laws_rooles', 'law_id', 'roole_id');
  }

  public static function getRoleId($role) {
    $firstRoleId = Law::getFirstRoleId();
    switch ($role) {
      case 'Studiendekan':
        return $firstRoleId;
        break;

      case 'Admin':
        return $firstRoleId + 1;
        break;

      case 'Coordinator':
        return $firstRoleId + 2;
        break;

      case 'Professor':
        return $firstRoleId + 3;
        break;

      case 'Assistant':
        return $firstRoleId + 4;
        break;

      case 'Student':
        return $firstRoleId + 5;
        break;

      case 'Candidate':
        return $firstRoleId + 6;
        break;                                                
      
      default:
        # code...
        break;
    }
  }

  //this function calling from views 
  // to list the role in dropdown menu
  // it will return list of role and respected id
  public static function getRooles() {
		$roles = DB::table('rooles')->get();
		$roleToPush[''] = 'Role';
    $rId = DB::table('users_rooles')
                    ->where('user_id', '=', Auth::id())
                    ->select('roole_id')
                    ->get();
    $rId =  (array) $rId[0];

    // print_r($roles);
		foreach ($roles as $r) {
      if ($r->id >= $rId['roole_id']) {
        $roleToPush[$r->id] = $r->role;
      }
		}
		
		return $roleToPush;  	
  }
  //To get the role id of any user using their id and by using the role id get the role of the user.
  //Used in Export CSV
  public static function getRooleName($uId)
  {

        $rId = DB::table('users_rooles')
                    ->where('user_id', '=', $uId)
                    ->pluck('roole_id');

        $rName = DB::table('rooles')
                ->where('id', '=', $rId)
                ->pluck('role');

              return $rName;
  }
  
  public static function getRooleId($uId)
  {

        $rId = DB::table('users_rooles')
                    ->where('user_id', '=', $uId)
                    ->pluck('roole_id');
               return $rId;
  }

}
<?php
 
class HelperActions  extends Eloquent {

  //its has boolean variable to switch the view mode Eg : very detailed view and short view
  // done by thishanth thevarajah
  public static function getViewMode() {
    $pageModeKey = 'detailedViewOf_'.Auth::id();
    if (!Session::has($pageModeKey)) {
       Session::put($pageModeKey, 0);
    }
    return Session::get($pageModeKey);
  }

  //its switch the boolean variable to switch the view mode Eg : very detailed view and short view
  // done by thishanth thevarajah
  public static function swtichViewMode() {
    $pageModeKey = 'detailedViewOf_'.Auth::id();
    $pageMode = !(HelperActions::getViewMode());
    Session::put($pageModeKey, $pageMode);
    return $pageMode;
  }

  // its a filter for string
  // its add newline for long strings
  // after this filter, a long string will be break into many lines 
  // it will look and feel beautiful
  // done by thishanth thevarajah
  public static function makeMultipleLineString($str, $NrOfChar){
    $str = chunk_split($str, $NrOfChar, "-<br/>");
    return substr($str, 0, -6);
  }

  // store message from session
  // this messagre using to show successfully created or updated messagre
  // done by thishanth thevarajah
  public static function putMessageToSession($msg){
    $messageKey = 'messageTo_'.Auth::id();
    Session::put($messageKey, $msg);
  }

  // retrive message from session
  // this messagre using to show successfully created or updated messagre
  // done by thishanth thevarajah
  public static function getMessageToSession(){
    $messageKey = 'messageTo_'.Auth::id();
    $msg = Session::get($messageKey);
    Session::forget($messageKey);
    return $msg;
  }

  // this is store the page number to the session 
  // it using key "pageNrOf_"with used_id
  // done by thishanth thevarajah
  public static function putPageNumberToSession($pageNumber){
    $pageNumberKey = 'pageNrOf_'.Auth::id();
    Session::put($pageNumberKey, $pageNumber);
  }

  // it will return page_number 
  // this page_number using to redirect the exact page after user edit Course or Module or User
  // its taking page number from session already stored 
  // after retrive the needed session it will delete the particular session
  // done by thishanth thevarajah
  public static function getPageNumberFromSession()  {
    $pageNumberKey = "pageNrOf_".Auth::id();
    $pageNumber = Session::get($pageNumberKey );
    Session::forget($pageNumberKey);
    return $pageNumber;
  }
  //it will return the current page number
  // get uri as input eg : itis_db/showallcourses?page=19
  // if the page number not available in uri it will return page number 1
  // done by thishanth thevarajah
  public static function getPageNumber($uri) {
    $parts = array_filter(explode("=",$uri));
    if (sizeof($parts) > 1) {
      $pageNr = $parts[1];
    } else {
      $pageNr = '1';
    }
    return $pageNr;
  }
  
  // To make CSV files form database tables
  //done by Fatema Tuj Johora
  public static function csvFileMaker()
     {

      $output = "";

      $tables = HelperActions::desiredTable();
      
       $l=0;
      foreach ($tables as $table) {
          
          $i=0;
          $j=0;
          $k=0;

          foreach ($table as $key => $value)
          {$i++;} 

          foreach ($table as $key => $value) {
          $k++;

           if($l==0){

            if($k!=$i){
              if($key=="id")
              {
               if(Roole::getRooleId($value) <1)
              {
                $output .=$key.',';
              }
              else
              {
                  $output .=$key.','.'role_id'.',';
            }
              }
              else
               $output .=$key.',';

            }
            else
                {$output .=$key;}
            }

          }
          $l=1;
           $output .="\n";
        

          foreach ($table as $key => $value) {
            $j++;
            if($key!="password"&&$key!="created_at"&&$key!="updated_at"&&$key!="remember_token")
            {
            if($j!=$i){
              if($key=="site")
              {
                $output .=Site::getSiteName($value).',';
              }
              elseif($key=="id")
              { 
                if(Roole::getRooleId($value) <1)
                {
             $output.=',';}
                else
               $output.=','.Roole::getRooleName($value).',';
              }
              
              else
              $output .=$value.',';

            }
            else
             { if ($key=="university") {
              $output .=Site::getSiteName($value);}
               else
               $output .=$value;}
             }
             else
            {$output.=',';}
          }
           
        }
        
         return($output);
         
  } 

  //To get the export files name
  //done by Fatema Tuj Johora
  public static function identifyFileName()
  { 
    if(Request::url() === URL::route('moduleCsv'))
      {
       $filename = "modules.csv";}

      elseif(Request::url() === URL::route('courseCsv'))
      {
       $filename = "courses.csv"; }

      elseif(Request::url() === URL::route('userCsv'))
      {
       $filename = "users.csv";}

      elseif(Request::url() === URL::route('logCsv'))
      {
       $filename = "logs.csv";}

       if(Request::url() === URL::route('modulePdf'))
      {
       $filename = "modules.pdf";}

      elseif(Request::url() === URL::route('coursePdf'))
      {
       $filename = "courses.pdf"; }

      elseif(Request::url() === URL::route('userPdf'))
      {
       $filename = "users.pdf";}

      elseif(Request::url() === URL::route('logPdf'))
      {
       $filename = "logs.pdf";}

       if(Request::url() === URL::route('moduleXml'))
      {
       $filename = "modules.xml";}

      elseif(Request::url() === URL::route('courseXml'))
      {
       $filename = "courses.xml"; }

      elseif(Request::url() === URL::route('userXml'))
      {
       $filename = "users.xml";}

      elseif(Request::url() === URL::route('logXml'))
      {
       $filename = "logs.xml";}

      return($filename);
  }
  
  // To get desired table data
  //done by Fatema Tuj Johora
  public static function desiredTable()
  {
     if(Request::url() === URL::route('moduleCsv')|| Request::url() === URL::route('moduleXml')||Request::url() === URL::route('modulePdf'))
      {
      $tables= DB::table('modules')->where('visible', '=', 1)->get();
      }

      elseif(Request::url() === URL::route('courseCsv')||Request::url() === URL::route('courseXml')||Request::url() === URL::route('coursePdf'))
      {
      $tables= DB::table('courses')->where('visible', '=', 1)->get();
      }

      elseif(Request::url() === URL::route('userCsv')||Request::url() === URL::route('userXml')||Request::url() === URL::route('userPdf'))
      {
      $tables= DB::table('users')->where('visible', '=', 1)->get();
      }

      elseif(Request::url() === URL::route('logCsv')||Request::url() === URL::route('logXml')||Request::url() === URL::route('logPdf'))
      
      {$tables= DB::table('logs')->get();
      }

      return($tables);
  }
  
  // To identify the table name
  //done by Fatema Tuj Johora
  public static function identifyTable()
  { 
    if(Request::url() === URL::route('moduleXml'))
      {
       $dbtable = "modules";}

      elseif(Request::url() === URL::route('courseXml'))
      {
       $dbtable = "courses"; }

      elseif(Request::url() === URL::route('userXml'))
      {
       $dbtable = "users";}

      elseif(Request::url() === URL::route('logXml'))
      {
       $dbtable = "logs";}

       return ($dbtable);
  }

  // To make XML files form database tables
  //done by Fatema Tuj Johora 
  public static function xmlFileMaker(){
  
  $tab = "\t";
  $br = "\n";
  $database = "itis_db";
  $output = '<?xml version="1.0" encoding="UTF-8"?>'.$br;
  $output.= '<database name="'.$database.'">'.$br;
  
  $dbtable = HelperActions::identifyTable();
  
  $output.= $tab.'<table name="'.$dbtable.'">'.$br;
  $output.= $tab.$tab.'<columns>'.$br;
  
  if(Request::url() === URL::route('moduleXml'))
  {$table = new Module;}
  elseif(Request::url() === URL::route('userXml'))
  {$table = new User;}
  elseif(Request::url() === URL::route('courseXml'))
  {$table = new Course;}
  elseif(Request::url() === URL::route('logXml'))
  {$table = new Log;}

 
    $meta = $table->describe();

    foreach($meta as $metan)
    {
      $i=0;
      $j=0;
       //$currentRow++;  
        foreach ($metan as $key => $value)
          {$i++;}  
      foreach($metan as $key => $value)
        { 
          
            
          if(is_numeric($key)){
           continue;
             }
          $j++;
          if($key=="Field"){
          $output.=$tab.$tab.$tab.'<column name ='."'$value'"." ";
          }
          else{
            if($j==($i-1))
            {
            $output.=$key.'='."'$value'";}
            else
              
              $output.=$key.'='."'$value'"." ";
          }
           
        }

      $output .='/>'.$br;
    }
  
  $output.= $tab.$tab.'</columns>'.$br;

//stick the records
 
  $tables = HelperActions::desiredTable();
  foreach ($tables as $table){
  $output.= $tab.$tab.'<records>'.$br;

  foreach ($table as $key => $value) {
  if($key!="password")
  $output.= $tab.$tab.$tab.'<'.$key.'>'.$value.'</'.$key.'>'.$br;
  }
  $output.= $tab.$tab.'</records>'.$br;
}

  $output.= $tab.'</table>'.$br;

  $output.= '</database>';
  return ($output);
    
 }
 
 // To make PDF files form database tables
 //done by Fatema Tuj Johora
 public Static function test()
  { 
    $output="";
    $output='<html><head><style>'
             .'table, th, td {'
    .'border: 1px solid black;'
    .'border-collapse: collapse;}'
    .'th, td { padding: 5px;}'
    .'</style></head>'
    .'<body>'
     . '<table style="width:100%"><tr>';

    //$tables=DB::table('courses')->get();
     $tables = HelperActions::desiredTable();
     $j=0;
    foreach ($tables as $table){


     if(Request::url() === URL::route('coursePdf')){

     foreach ($table as $key => $value) {
        if($j==0){
      if($key=="course"||$key=="semester"||$key=="lecturer"||$key=="second_lecturer"||$key=="time"||$key=="room"){
      
        $output.='<th>'.$key.'</th>';
      }
      elseif($key=="module_id")
      {
        $output.='<th>'.'examId'.'</th>';
      }
    }
  }
      $j=1;
      $output.='</tr>';
      $output.='<tr>';

     foreach ($table as $key => $value) {

      if($key=="course"||$key=="semester"||$key=="lecturer"||$key=="second_lecturer"||$key=="time"||$key=="room"){

        $output.='<td>'.$value.'</td>';
      }
      elseif($key=="module_id")
      { 
        $x=Course::getExamID($value);
        $output.='<th>'.$x.'</th>';
      }
  }
   $output.='</tr>';
  }

  elseif(Request::url() === URL::route('userPdf')){

     foreach ($table as $key => $value) {
        if($j==0){
      if($key=="first_name"||$key=="last_name"||$key=="institute"||$key=="email"||$key=="title"||$key=="personal_website_url"||$key=="university"||$key=="occupation"){
  
        $output.='<th>'.$key.'</th>';
     }
    }
  }
    $j=1;
    $output.='</tr>';
    $output.='<tr>';

    foreach ($table as $key => $value) {

      if($key=="first_name"||$key=="last_name"||$key=="institute"||$key=="email"||$key=="title"||$key=="personal_website_url"||$key=="university"||$key=="occupation"){

        $output.='<td>'.$value.'</td>';
    }
  }
   $output.='</tr>';     
}

elseif(Request::url() === URL::route('modulePdf')){

    foreach ($table as $key => $value) {
      if($j==0){
      if($key=="module"||$key=="examId"||$key=="semester"||$key=="topic_category"||$key=="type"||$key=="weekly_composition"||$key=="exam"||$key=="ECTS_credits"){
        $output.='<th>'.$key.'</th>';
      }
     }
    }
    $j=1;
    $output.='</tr>';
    $output.='<tr>';

    foreach ($table as $key => $value) {
      if($key=="module"||$key=="examId"||$key=="semester"||$key=="topic_category"||$key=="type"||$key=="weekly_composition"||$key=="exam"||$key=="ECTS_credits"){
      
      $output.='<td>'.$value.'</td>';
    }
  }
       $output.='</tr>';
}

elseif(Request::url() === URL::route('logPdf')){

     foreach ($table as $key => $value) {
      if($j==0){
      if($key!="id"&&$key!="raw_data_string"){
      
        $output.='<th>'.$key.'</th>';
      }
    }
  }
     $j=1;
     $output.='</tr>';
     $output.='<tr>';

     foreach ($table as $key => $value) {
      if($key!="id"&&$key!="raw_data_string"){
      
      $output.='<td>'.$value.'</td>';
    }
  }
     $output.='</tr>';
}
}
    $output.='</table></body></html>';
    return($output);
}
  
}
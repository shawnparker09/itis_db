<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	//blacklist in import
	protected $guarded = array('id', 'created_at', 'updated_at', 'remember_token');
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function rooles() {
    return $this->belongsToMany('Roole', 'users_rooles');
  }

  //store the user language in session English or deutsch
  // its a toggle function
  public static function changeUserLanguage($key){
  	$language = User::getUserLanguage("languageOf".Auth::id()) == "en" ? "de" : "en" ;
  	Session::put($key, $language);
  }

  public static function getUserLanguageWithInvertion($key){
  	return User::getUserLanguage($key) == "en" ? "de" : "gb" ;
  }

  // get the user language in session English or deutsch
  public static function getUserLanguage($key){
  	return Session::has($key) ? Session::get($key) : "en";
  }

	// check the email id is unique or not in update user profile
	public static function isUniqueEmail($email, $id)	{
		$user = new User;
    return (($user ->where('id', $id)->pluck('email') == $email && ($user ->where('email', $email)->count()) == 1) || (($user ->where('email', $email)->count()) == 0));
	}

	public static function isDeletedUser($emailId)	{
		$user= new User;
    return $user->where('email', $emailId)->pluck('visible');
	}

	// making array for update user
	public static function updateArrayMaker() {
		return array('first_name' => Input::get('first_name'),
					 			'last_name' => Input::get('last_name'),
					 			'email' => Input::get('email'),
					 			'title' => Input::get('title'),
					 			'university' => Input::get('site'),
					 			'institute' => Input::get('institute'),
					 			'institute_url'=>Input::get('institute_url'),
					 			'personal_website_url'=>Input::get('personal_website_url'),
					 			'photo_url'=>Input::get('photo_url'),
					 			'location'=>Input::get('location'),
					 			'academic_career'=>Input::get('academic_career'),
					 			'occupation'=>Input::get('occupation'),
					 			'research_development_projects'=>Input::get('research_development_projects'),
					 			'Cooperation_practice'=>Input::get('Cooperation_practice'),
					 			'Patents_intellectual_property'=>Input::get('Patents_intellectual_property'),
					 			'num_of_publications'=>Input::get('num_of_publications'),
					 			'selected_publications'=>Input::get('selected_publications'),
					 			'activity'=>Input::get('activity'),
					 			'post_address'=>Input::get('post_address'),
					 			'telephone'=>Input::get('telephone')

				);
	}

	// making rules for validation to update user without role_type 
	// and its provide rest of the rules to create a new user
	public static function casualValidationRuleMaker()	{
		return $rules =  array(
			'title'             => 'required', 						
			'first_name'        => 'required', 						
			'last_name'         => 'required', 						
			'university'        => 'required',
			'email'             => 'required|email',
			'institute'         => 'required'
		);
	}

	public static function validationRuleMakerForUpload() {
		$toPush = array( 'role_id' 						=> 'required',
										 'email'            	=> 'required|email|unique:users',
										 'password'      			=> 'required|min:3');
		return array_merge(User::casualValidationRuleMaker(), $toPush);
	}

	// making rules for validation to create a new user rest of the rules used from casualValidationRuleMaker function
	public static function validationRuleMakerForCreate()	{
		$toPush = array( 'role_id' 						=> 'required',
										 'email'            	=> 'required|email|unique:users');
		return array_merge(User::casualValidationRuleMaker(), $toPush);
	}

	// making validation rule to password
	public static function passwordValidationRuleMaker($input)	{
		return $rules = array(
			'password'           => 'required|min:3', 						
			'newPassword'        => 'required|min:3', 						
			'conformPassword'    => 'required|min:3'						
		);
	}

	// match the password with confirm user
	public static function passwordMacher() {
		return (Input::get('newPassword') === Input::get('conformPassword'));
	}

	// this will return the course name, find by id
	public static function getEmail($userId) {
    return User::where('id', $userId)->pluck('email');
	}

	public static function updatePassword($id, $passwordUpdateArray) {
		$user= new User; 
		$user ->where('id', $id)
              ->update($passwordUpdateArray);
	}

	// check the password which is mach with old
	public static function oldPasswordChecker(){
		$user= new User; 
		return (Hash::check(Input::get('password'), $user->where('id', Auth::id())->pluck('password')));		
	}

	// make update array for password
	public static function passwordUpdateArrayMaker() {
		return array('password' => Hash::make(Input::get('newPassword')));
	}

	// delete a user a user using id
	public static function deleteU($idToDelete)	{
		Logs::saveLog("users", User::userMitName($idToDelete). " User Deleted", $idToDelete, "", 0,0, 1);

		$user= new User; 
		$user ->where('id', $idToDelete)
              ->update(array('visible' => 0));
    // echo View::make('layouts.messages.successfullyDeleted');              
	}

	// retrive a user from delete using its id
	public static function undoDelete($idToUndo)	{
		Logs::saveLog("users", "Deleted User ". User::userMitName($idToUndo). " Retrived", $idToUndo, "", 0,1, 0);
		$user= new User; 
		$user ->where('id', $idToUndo)
              ->update(array('visible' => 1));
    // echo View::make('layouts.messages.successfullyUndoDeleted');
	}

	//  this will return the user full name mit greetings
	public static function userMitName($id)	{
		$user= new User; 
		$user = $user->where('id', $id);
		$fullString = $user ->where('id', $id)->pluck('title'). " ";

		$user= new User; 
		$user = $user ->where('id', $id);
		$fullString = $fullString. $user ->where('id', $id)->pluck('first_name'). " ";
		
		$user= new User; 
		$user = $user ->where('id', $id);
		$fullString = $fullString. $user ->where('id', $id)->pluck('last_name');
		return $fullString;
	}

	//rear users table with pagination
  public static function readUserMitPagination($pagination) {
		return DB::table('users_rooles')
                ->join('users', 'users.id', '=', 'users_rooles.user_id' )
                ->where('first_name', 'LIKE', '%' . Input::get('searchInput') . '%')
								->orderBy('users.updated_at', 'DESC')
								->paginate($pagination);
  }

  // this is read whole user table and convert from DB object to array
  public static function readAllUser() {
  	return User::all()->toArray();
  }
  
  //this function is used for getting the last updated columns in user table
  public static function getEditedColumn($cId)
  {   

    $output="";
    $tables= DB::table('users')->where('id', $cId)->get();

    foreach ($tables as $table) {

    foreach ($table as $key => $value) {

      $i=input::get($key);
      
      if($value!=$i&& $key!='updated_at'&&$key!='created_at'&&$key!='visible'&&$key!='password'&&$key!='remember_token')
      { 
         $output .=$key.',';  
      }
    }
   }
     return($output);
 }

 	// it will generate random password
 	// Done by Thishanth Thevarajah
	public static function randomPassword() {
	  $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	  $pass = array(); //remember to declare $pass as an array
	  $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	  for ($i = 0; $i < 8; $i++) {
	      $n = rand(0, $alphaLength);
	      $pass[] = $alphabet[$n];
	  }
	  return trim(implode($pass)); //turn the array into a string
	}
  
  //To get information about the columns in a table
  public function describe(){
        $table = $this->getTable();
        $pdo = \DB::connection()->getPdo();
        $do= $pdo->query("describe $table")->fetchAll();  
        return $do;
  }
    //This function will take a user information
  // Done by Humaun Rashid
   public static function getUserInfo($user_id = null) {
        $data = DB::table('users')->where('id', $user_id)->get();
        $res = json_decode(json_encode($data), true);
        return array_pop($res);
    }
}

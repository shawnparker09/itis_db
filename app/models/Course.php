<?php
 
class Course extends Eloquent {
  protected $table = 'courses';
  //blacklist in import
  protected $guarded = array('id', 'created_at', 'updated_at');

  //read course table with pagination
  public static function readCourseMitPagination($pagination) {
  	return Course::where('course', 'LIKE', '%' . Input::get('searchInput') . '%')
  				 ->orderBy('updated_at', 'DESC')
  				 ->paginate($pagination);
  }

  public static function validationRuleMakerForMultipleCourse() {
    return $rules = array(
      'course'              => 'required',            
      'module'              => 'required',            
      'mode'                => 'required',            
      'site'                => 'required'           
    );
  }

  // this is rules for validation for make new course or update a course without credit
  public static function CasualValidationRuleMaker()  {
  	$rules = array(
			'semester'            => 'required', 						
			'startDate'           => 'required|date', 						
		);
    return array_merge(Course::validationRuleMakerForMultipleCourse(), $rules);
  }

  // this is rules for validation for make new course or update a course
  public static function validationRuleMaker($input)	{
		$creditsToPush = array('credits' => 'required');
		return array_merge(Course::CasualValidationRuleMaker(), $creditsToPush);
	}

	// this is making array for course
	// this array not include credit
	public static function casualUpdateArrayMaker() {
		return array( 'course'    => Input::get('course'),
						 	'module'                          => Module::getModuleName(Input::get('module')),
						 	'module_id'                       => Input::get('module'),
						 	'semester'                        => Input::get('semester'),
						 	'mode'                            => Input::get('mode'),
						 	'startdate'                       => Input::get('startDate'),
						 	'site'                            => Input::get('site'),
						 	'lecturer'                        => Input::get('lecturer'),
              'lecturer_url'                    => Input::get('lecturer_url'),
              'time'                            => Input::get('time'),
              'room'                            => Input::get('room'),
              'video_recording'                 => Input::get('video_recording'),
              'lecture_hall_transmission'       => Input::get('lecture_hall_transmission'),
              'video_recording_download_url'    => Input::get('video_recording_download_url'),
              'live_stream'                     => Input::get('live_stream'),
              'live_web_feed_url'               => Input::get('live_web_feed_url'),
              'interactive_live_stream'         => Input::get('interactive_live_stream'),
              'attendance_required'             => Input::get('attendance_required'),
              'partial_attendance_required'     => Input::get('partial_attendance_required'),
              'transmission_room'               => Input::get('transmission_room'),
              'assistant'                       => Input::get('assistant'),
              'comment'                         => Input::get('comment')
							  );
	}

	// make array for store credit of a course
	// credit is so sensitive, so we prevent it edit from normal user like proff. We allow to edit it by admin or super admin 
	public static function importantUpdateArrayMaker()	{
		$creditsToPush = array('credits' => Input::get('credits'));
		return array_merge(Course::casualUpdateArrayMaker(Input::all()), $creditsToPush);
	}

	// this will return the course name, find by id
	public static function getCourseName($id) {
    return Course::where('id', $id)->pluck('course');
	}

	// this is for delete course
	public static function deleteCourse($idToDelete) {
		Logs::saveLog("courses", Course::getCourseName($idToDelete). " Course Deleted", $idToDelete, "", 0,0, 1);

		$course = new Course;
		$course ->where('id', $idToDelete)
            ->update(array('visible' => 0));
		// echo View::make('layouts.messages.successfullyDeleted');            
	}

	// this is for retrive from delete
	public static function undoDeleteCourse($undoId)	{
		Logs::saveLog("courses", "Deleted Course ". Course::getCourseName($undoId). " Retrived", $undoId, "", 0,1, 0);

		$course = new Course;
		$course ->where('id', $undoId)
            ->update(array('visible' => 1));
		// echo View::make('layouts.messages.successfullyUndoDeleted');            
	}

  //To get information about the columns in a table
	public function describe()
    {
        $table = $this->getTable();
        $pdo = \DB::connection()->getPdo();
        $do= $pdo->query("describe $table")->fetchAll();  
        return $do;
    }
   /**
     * @param $semester
     * @return array
     */
   //This will take all course of a semester
   //Done by Humaun Rashid
   public static function getCourseData($semester)
    {
        $data = array();
        $course_data = DB::table('courses')
            ->join('modules', 'courses.module_id', '=', 'modules.id')
            ->join('categories', 'modules.topic_category', '=', 'categories.id')
            ->select('courses.id', 'courses.course','courses.credits','modules.examId', 'courses.semester','modules.id as module_id','modules.module','categories.category_name')
            ->where('courses.semester', $semester)
            ->get();
        $course_data = json_decode(json_encode($course_data), true);
        $category_names = array_unique(array_column($course_data, 'category_name'));
        #BaseController::_setTrace($category_names);
        foreach($category_names as $key=> $category){
            $temp = array();
            $course = BaseController::search($course_data, 'category_name', $category);
            $modules_names = array_unique(array_column($course, 'module'));
            #####################################  Find Course wrt module ############################################
            foreach($modules_names as $module){
                $modules = BaseController::search($course, 'module', $module);
                array_push($temp, $modules);
            }
            array_push($data, $temp);
        }
        return $data;
    }


    // create a date using year and semester
    // done by Thevarajah Thishanth
    public static function semesterToDate($year, $semester) {
      if ($semester == 1) {
        $date = '1/3/'.$year;
      } else {
        $date = '1/9/'.$year;
      }
      return $date;
    }
    // generate the semester array to create morethan one course
    // done by Thevarajah Thishanth
    public static function calculateNoOfSemesterV2() {
      $both = 0;
      $winter = 0;
      $summer = 0;
      $start = Input::get('semesterStart');
      $end = Input::get('semesterEnd');
      $semesterForCourse = Input::get('semesterForCourse');
      $thisYear = date("Y");

      if ($semesterForCourse == 1) {
        $winter = 1; 
      } elseif ($semesterForCourse == 2) {
        $summer = 1;
      } elseif ($semesterForCourse == 3) {
        $both = 1;
      }

      $startSemester = mb_substr($start, 0, 5);
      $endSemester = mb_substr($end, 0, 5);
      $startSemester = ($startSemester == 'SoSe ' ? 1 : 2 );
      $endSemester = ($endSemester == 'SoSe ' ? 1 : 2 );

      $startYear = mb_substr($start, 5, 4);
      $endYear = mb_substr($end, 5, 4);
      $startYear = strlen($startYear) == 4 ? $startYear : $thisYear;
      $endYear = strlen($endYear) == 4 ? $endYear : $thisYear;
      $sDate = Course::semesterToDate($startYear, $startSemester);

      $noOfSemester = (($endYear - $startYear) * 2) + (($endSemester - $startSemester) + 1);

      $semesters = array_values(array_filter(array_keys(Course::generateListOfSemester($sDate, $noOfSemester, $both, $winter, $summer))));
      return $semesters;
    }

    // done by Thevarajah Thishanth
    // public static function calculateNoOfSemester() {
    //   $semesterForCourse = Input::get('semesterForCourse');
    //   $both = 0;
    //   $winter = 0;
    //   $summer = 0;
    //   $i = 1;
    //   $noOfSemester = 0;

    //   if ($semesterForCourse == 1) {
    //     $winter = 1; 
    //   } elseif ($semesterForCourse == 2) {
    //     $summer = 1;
    //   } elseif ($semesterForCourse == 3) {
    //     $both = 1;
    //   }
      
    //   $startDate = Input::get('semesterStart');
    //   $sDate = $startDate; // to prevent startDate modification in the following while loop
    //   $endDate = Input::get('semesterEnd');

    //   while ($i) {
    //     $startDate = new DateTime($startDate);
    //     $interval = new DateInterval('P6M');
    //     $startDate->add($interval);
    //     $startDate = $startDate->format('Y/m/d');
    //     $noOfSemester++;
    //     $j = (int)abs((strtotime($startDate) - strtotime($endDate))/(60*60*24*30));
    //     if ($j <= 3) {
    //       $i = 0;
    //     } 
    //   }
    //   $semesters = array_values(array_filter(array_keys(Course::generateListOfSemester($sDate, $noOfSemester, $both, $winter, $summer))));
    //   return $semesters;
    // }

    // done by Thevarajah Thishanth
    public static function increaseMonth($month) {
      if ($month <= 6) {
        $month += 6;
      } elseif ($month > 6) {
        $month = (6 - (12 - $month));
      }
      return $month;
    }

    public static function generateListOfSemesterWithSemesterString($date) {
      $heading[''] = 'Semesters';
      $semesters = array_filter(Course::generateListOfSemester($date));
      return (array_merge($heading, $semesters));
    }

    // this will generate list of semester from today to neyt ten years
    // done by Thevarajah Thishanth
    public static function generateListOfSemester($startDate, $j = 10, $both = 1, $winter = 0, $summer = 0) {
      $time = strtotime($startDate);
      $month = date("m",$time);
      $year = date("Y",$time);
      $i = 1;
      $semesters[''] = '';
      $strr = "year". $year ."month". $month;

      while ($i <= $j) {
        if (($month >= 3 && $month < 9) && ($both || $summer)) {
          $key = "SoSe ". $year;
          $value = $year ."SS";
          $semesters[$key] = $value;

        } elseif ($month < 3 || $month >= 9){
          if ($both || $winter) {
            $y = $year+1;
            $key = "WiSe ". $year ."/". $y;
            $value = $year ."WS";
            $semesters[$key] = $value;
          }
          $year++;
        }

        $month = Course::increaseMonth($month);
        $i++;
      }
      return $semesters;
    }
    
    //this function is used for getting the last updated columns in course table
    public static function getEditedColumn($cId){   

    $output="";
    $tables= DB::table('courses')->where('id', $cId)->get();

    foreach ($tables as $table) {

    foreach ($table as $key => $value) {
      
      if ($key=='module'){
      $i= Module::getModuleName(input::get($key));
      } 
      elseif($key=='module_id')
       {$i=input::get('module');}
      elseif($key=='startdate')
       {$i=input::get('startDate');}
      else
      {$i=input::get($key);}
      
      if($value!=$i&& $key!='updated_at'&&$key!='created_at'&&$key!='visible')
      { 
         $output .=$key.',';  
      }
    }
  }
     return($output);
  }

    //get the current semester using todays date
    //done by Fatema Tuj Johora
     public static function getSemester()  {

     if(date("n")>=3&&date("n")<9)
     	{return "SoSe"." ".date("Y"); }
     else
     	{return "WiSe"." ".date("Y",strtotime("-1 year"))."/".date("Y");}

 }  

    //get the current semester using todays date
    //V2 implemented by Thishanth Thevarajah
    public static function getSemesterV2()  {

     if(date("n")>=3&&date("n")<9) {
        return "SoSe"." ".date("Y"); 
      } else {
        return "WiSe"." ".date("Y")."/".date("Y", strtotime("+1 year"));
      }
    } 

    //check which courses are offered in current semester
    //done by Fatema Tuj Johora
    public static function checkCurrentSemesterCourse($id){

     $dateValue= Course::where('id', $id)->pluck('semester');
     $currentSemester=Course::getSemesterV2();
     if($dateValue == $currentSemester)
     {
     	return 1;
     }
     else
     	{return 0;}
    }

      public static function getExamID($mid){
      $x= Module::where('id',$mid)->pluck('examId');
      return $x;
      }

     

}


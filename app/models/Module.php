<?php
 
class Module extends Eloquent {
  protected $table = 'modules';
	//blacklist in import
	protected $guarded = array('id', 'created_at', 'updated_at');  

	public static function readModuleMitPagination($page) {
		return Module::where('module', 'LIKE', '%' . Input::get('searchInput') . '%')
							->orderBy('updated_at', 'DESC')
							->paginate($page);
	}

  // validation rule maker for module create
  public static function validationRuleMaker($input)	{
		return $rules = array(
			'module'                   => 'required|unique:modules', 						
			'module_no'                => 'required|unique:modules',
			'topic_category'           => 'required',
			'examId'		               => 'required|unique:modules'
		);
	}

	// validation rule maker for module update
  public static function validationRuleMakerForUpdate($input)	{
		return $rules = array(
			'module'                   => 'required', 						
			'module_no'                => 'required',
			'topic_category'           => 'required',
			'examId'				           => 'required'
		);
	}

	public static function isModuleExistByModuleName($module)	{
		return Module::getModuleId($module) ? 0 : 1 ;
	}

	// array for update the module
	public static function updateArrayMaker() {
		return array( 
								'module'                => Input::get('module'),
								'module_no'             => Input::get('module_no'),
								'topic_category'        => Input::get('topic_category'),
								'weekly_composition'    => Input::get('weekly_composition'),
								'exam'                  => Input::get('exam'),
								'exam_duration'         => Input::get('exam_duration'),
								'ECTS_credits'	      	=> Input::get('ECTS_credits'),
								'required_hours_of_work'=> Input::get('required_hours_of_work'),
								'required_work_in_hours'=> Input::get('required_work_in_hours'),
								'site'                  => Input::get('site'),
								'site_2'                => Input::get('site_2'),
								'person_responsible'	  => Input::get('person_responsible'),
								'person_responsible_2'  => Input::get('person_responsible_2'),
								'semester'	         		=> Input::get('semester'),
								'teaching_methods'      => Input::get('teaching_methods'),
								'module_description'    => Input::get('module_description'),
								'miscellaneous_Comments'=> Input::get('miscellaneous_Comments'),
								'recommended_prerequisite_knowledge'	=> Input::get('recommended_prerequisite_knowledge'),
								'recommended_literature'=> Input::get('recommended_literature'),
								'outcomes'              => Input::get('outcomes'),
								'examId'                => Input::get('examId')
							  );
	}

	// first it read module_no using id
	// then module_no using to make different style classes
	// done by thishanth
	public static function moduleStyleClassById($module_id) {
		return Module::moduleStyleClass(Module::getModuleNo($module_id));
	}

	// return different style classes using the module_no
	// done by thishanth
	public static function moduleStyleClass($module_no){
		// $module_no = round($module_no, 0, PHP_ROUND_HALF_DOWN);
		$module_no = $module_no[0];
		switch ($module_no) {
			case $module_no == 1:
				$cls = "red-background";
				break;

			case $module_no == 2:
				$cls = "blue-background";
				break;

			case $module_no == 3:
				$cls = "green-background";
				break;

			case $module_no == 4:
				$cls = "purple-background";
				break;

			case $module_no == 5:
				$cls = "yellow-background";
				break;																
			
			default:
				$cls = "gray-background";
				break;
		}
		return $cls;
	}

	// get number of the module using its id eg : 1.08
	public static function getModuleNo($id) {
    return Module::where('id', $id)->pluck('module_no');
	}

	// get name of the module using its id
	public static function getModuleName($id) {
    return Module::where('id', $id)->pluck('module');
	}

	public static function getModuleId($module)	{
		return Module::where('module', $module)->pluck('id'); 
	}

	// to delete a module using its id
	public static function deleteModule($idToDelete) {
		Logs::saveLog("modules", Module::getModuleName($idToDelete). " Module Deleted", $idToDelete, "", 0,0, 1);

		$module = new Module;
		$module ->where('id', $idToDelete)
            ->update(array('visible' => 0));
		// echo View::make('layouts.messages.successfullyDeleted');            
	}

	// retrive a deleted module using its id
	public static function undoDeleteModule($undoId)	{
		Logs::saveLog("modules", "Deleted Module ". Module::getModuleName($undoId). " Retrived", $undoId, "",0, 1, 0);

		$module = new Module;
		$module ->where('id', $undoId)
            ->update(array('visible' => 1));
		// echo View::make('layouts.messages.successfullyUndoDeleted');            
	}

	// this will return true if the module_no is not unique
	// which means this module_no is already taken
	public static function isNotUnique($module_no)	{
		$module = new Module;
		return (($module ->where('module_no', $module_no)->count()) > 0);
	}

	// completely mofidy by Thevarajah Thishanth
	public static function isUnique($id)	{
    // return ($module ->where('id', $id)->pluck('module_no') == $module_no || ($module ->where('module_no', $module_no)->count()) == 0);
		$errorArray[''] = '';
		$module = new Module;

		$moduleNr = $module ->where('module_no', Input::get('module_no'))->pluck('id');
		$examId = $module ->where('examId', Input::get('examId'))->pluck('id'); 
		$moduleName = $module ->where('module', Input::get('module'))->pluck('id'); 
		
		if ($moduleNr != 0 && $moduleNr != $id) {
			$errorArray['module_no'] = 'Module Id is Already Taken. Please use Different Module ID.';
		}
		if ($examId != 0 && $examId != $id) {
			$errorArray['examId'] = 'Exam Id is Already Taken. Please use Different Exam ID.';
		}
		if ($moduleName != 0 && $moduleName != $id) {
			$errorArray['module'] = 'Module Name is Already Taken. Please use Different Module Name.';
		}
	
		return $errorArray;
	}

	// this moduleToPush array using in create new course 
	// this array display in a dropdown list 
	// use can select a module to create a new course or edit courses
	public static function makeModuleArray() {
		
		$modules = DB::table('modules')->where('visible', '=', 1)->get();
		$moduleToPush[''] = '';

		foreach ($modules as $module) {
			$moduleToPush[$module->id] = $module->module;
		}
		return $moduleToPush;
	}
    
    //this function is used for getting the last updated columns in module table
	public static function getEditedColumn($mId){   

    $output="";
    $tables= DB::table('modules')->where('id', $mId)->get();

    foreach ($tables as $table) {

    foreach ($table as $key => $value) {

      if($key=='topic_category')
      {$i=input::get('topicCategory');}
      else
      {$i=input::get($key);}

      if($value!=$i&& $key!='updated_at'&&$key!='created_at'&&$key!='visible')
      { 
         $output .=$key.',';  
      }
     }
    }
        return($output);
    }

	//To get information about the columns in a table
    public function describe()
    {
        $table = $this->getTable();
        $pdo = \DB::connection()->getPdo();
        $do= $pdo->query("describe $table")->fetchAll();  
        return $do;
    }
   //take module with respect to semester
  // Humaun Rashid
public static function getModules($semester){
     return DB::table('modules')
         ->join('courses', 'courses.module_id', '=', 'modules.id')
         ->select('courses.course','modules.examId','courses.semester','courses.credits','modules.id','modules.module', 'modules.module_no','modules.exam', 'modules.topic_category')
         ->where('courses.semester', '=', $semester)
         ->get();
  }
  // Humaun Rashid
  public static function getAllModules(){
     return DB::table('modules')
         ->join('courses', 'courses.module_id', '=', 'modules.id')
         ->select('courses.course','modules.examId','courses.credits','courses.semester','modules.id','modules.module', 'modules.module_no','modules.exam', 'modules.topic_category')
         ->get();
  }
}
	

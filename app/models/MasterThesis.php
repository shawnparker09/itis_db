<?php

class MasterThesis extends \Eloquent {
	protected $fillable = [];
    protected $table = 'master_theses';
    //blacklist in import
    protected $guarded = array('id', 'created_at', 'updated_at');
}
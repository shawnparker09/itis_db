<?php
 
class Login extends Eloquent {
  
  // this will make logout the user
  public static function logOut() {
    Auth::logout(); // log the user out of our application
    return View::make('layouts.login');
  }

  // this will create session and put "user_id as key and tole_id as value"
  // this is very importanrt to maintain the role based authendication system
  public static function createSession(){
  	if (!Session::has(Auth::id())){
			$uId = Auth::id();
  		$rId = DB::table('users_rooles')->where('user_id', $uId)->pluck('roole_id');
  		Session::put($uId, $rId);
		}
  }
}
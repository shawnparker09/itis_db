<?php

class SemesterCourse extends \Eloquent {
	protected $fillable = [];
    protected $table = 'semester_courses';
    //blacklist in import
    protected $guarded = array('id', 'created_at', 'updated_at');
}
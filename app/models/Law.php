<?php
 
class  Law extends Eloquent {
  
  // pivot table maintanance
  public function rooles() {
    return $this->belongsToMany('Roole', 'laws_rooles', 'law_id', 'roole_id');
  }

  // read all the laws from Law table
  public static function getLawId($actionAndControllerName) {
    $law = DB::table('laws')->where('controller_and_action', $actionAndControllerName)->first();
    return $law->id;
  }

  // check the role type from users_rooles pivot table
  // its use Authendication users id to retrive role type (more secure)
  public static function RoleIdInUsersRoole() {
    $r = DB::table('users_rooles')->where('user_id', Auth::id())->first();
    return $r->roole_id;
  }

  // reading the laws_rooles pivot table to retrive all role types to given law_id
  // retun the size of the result
  public static function readLaw($lawId) {
    return sizeof(DB::table('laws_rooles')->where('law_id', $lawId)->get());
  }

  public static function getFirstRoleId() {
    $roles = DB::table('rooles')->first();
    return $roles->id;
  }

  public static function sessionIdBalancer($ssn){
    $firstRoleId = Law::getFirstRoleId();
    return ($ssn - $firstRoleId) + 1;
  }

  //this is very important core function to check role based authendication system
  // join two pivot tables (users_rooles & laws_rooles)
  //reading from this join
  // return true or false
  // login decision make using this boolean value
  public static function checkRights($actionAndControllerName)  {
    $authid = Auth::id();
    $lawId  = Law::getLawId($actionAndControllerName);
    $firstRoleId = Law::getFirstRoleId();
    $ssn = Session::get($authid, $firstRoleId);

    $sql = DB::table('laws_rooles')
                ->join('users_rooles', 'users_rooles.roole_id', '=', 'laws_rooles.roole_id' )
                ->where('laws_rooles.law_id', '=', $lawId)
                ->where('users_rooles.user_id', '=', $authid)
                ->where('laws_rooles.roole_id', '<=', $ssn)
                ->get();

    if (Law::RoleIdInUsersRoole() == $ssn) {
      return (sizeof($sql)) > 0 ? 0 : 1;
    } else {

      $balancedSSN = Law::sessionIdBalancer($ssn);
      return (Law::readLaw($lawId)) < $balancedSSN ? 1 : 0;
    }
  }

  //this function using to insert into Law table
  // one time (when install on server) used function
  public static function insertIntoLaw() {
    $firstRoleId = Law::getFirstRoleId();
    foreach (ListOFLaw::getLaw() as $law) {
   
      $lawObj = new Law;
      $lawObj->name = $law['name'];
      $lawObj->controller_and_action = $law['controller_and_action'];
      $lawObj->save();

      $rn = ($firstRoleId - 1) + $law['role'];
      $law = Law::find(DB::getPdo()->lastInsertId());


      for ($i = $rn; $i >= $firstRoleId; $i--) { 
        $role_type = $i;
        $law->rooles()->attach($role_type); //this executes the insert-query
      }


    }
  }
}
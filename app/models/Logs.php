<?php
 
class Logs extends Eloquent {
  protected $table = 'logs';

  public static function readLogMitPagination($noOfPage){
    return DB::table('logs')->orderBy('id', 'DESC')->paginate($noOfPage);
  }

  // this is for store the logs for all action eg: create, deleted, retrived 
  // this is for all type of attributes eg: user, course module and users
  public static function saveLog($tableName, $changeString, $rawId, $rawData, $field, $created, $deleted) {

    $Log = new Logs;

    $Log->table_name       = $tableName;
    $Log->change_string    = $changeString;
    $Log->raw_id_in_table  = $rawId;
    $Log->raw_data_string  = $rawData;
    $Log->field_name       = $field;
    $Log->created          = $created;
    $Log->deleted          = $deleted;
    $Log->done_by          = User::userMitName(Auth::id());
    $Log->done_by_id       = Auth::id();

    $Log->save();
  }

  // this is only will be call when table raw update.
  // it take the whole raw from table befor update
  // and make it string here
  public static function makeString($rawData) {
    $str = '\u100|';
    foreach ($rawData as $key => $value) {
      $str = $str. $value. '\u100|';
    }
    return $str;
  }

  // this make string to array functionality 
  // its uses \u100| to make divition between arrays
  public static function makeArray($str) {
    return array_pad(explode("\u100|", $str),null,null);
  }

  // get whole raw from course table using id
  public static function getLastRawOfCourse($cId)  {
    $course= new Course;
    $rawData = DB::table('courses')->where('id', $cId)->first();
    return (array) $rawData;
  }

  // get whole raw from module table using id
  public static function getLastRawOfModule($mId)  {
    $module= new Module;
    $rawData = DB::table('modules')->where('id', $mId)->first();
    return (array) $rawData;
  }

  // get whole raw from  table user using id
  public static function getLastRawOfUser($uId)  {
    $rawData = DB::table('users')->where('id', $uId)->first();
    return (array) $rawData; 
  }

  // get whole raw from table study using id
  public static function getLastRawOfStudyPlan($id) {
      $rawData = DB::table('study_plans')->where('id', $id)->first();
      return (array) $rawData;
  }
  
  // get role_id from users_rooles id
  public static function getRoleId($uId)  {
    return DB::table('users_rooles')->where('user_id', $uId)->pluck('roole_id');
  }

  // ready everything to store a course log
  public static function prepCourseLog($cId) {
    $rawData = Logs::getLastRawOfCourse($cId);
    $str = Logs::makeString($rawData);
    $field = Course::getEditedColumn($cId);
    Logs::saveLog('courses',  $rawData['course']. " Course Updated", $cId, $str,$field, 0, 0);
    
  }

  // ready everything to store a module log
  public static function prepModuleLog($mId) {
    $rawData = Logs::getLastRawOfModule($mId);
    $str = Logs::makeString($rawData);
    $field = Module::getEditedColumn($mId);
    Logs::saveLog('modules',  $rawData['module']. " Module Updated", $mId, $str,$field, 0, 0);
    
  }

  // ready everything to store a user log
  public static function prepUserLog($cId) {
    $rawData = Logs::getLastRawOfUser($cId);
    $roleId = Logs::getRoleId($cId);

    $str = Logs::makeString($rawData);
    $str = '\u100|'. $roleId. $str;
    $field = User::getEditedColumn($cId);
    Logs::saveLog('users',  User::userMitName($cId). "User Undo", $cId, $str,$field, 0, 0);
   
  }
  
   // ready everything to store a mentor log
  public static function prepMentorLog($msg, $id){
    $rawData = Logs::getLastRawOfStudyPlan($id);
    $user = (array) DB::table('users')->where('id', $rawData['user_id'])->first();
    $change_str = $msg . ' for '. $user['first_name'] . ' ' . $user['last_name'];
    $str = Logs::makeString($rawData);
    Logs::saveLog('study_plans', $change_str, $id, $str, 'mentor_name', 0, 0);
  }
  
  // ready everything to store a course log after undo the changes
  public static function prepUndoCourseLog($cId) {
    $rawData = Logs::getLastRawOfCourse($cId);
    $str = Logs::makeString($rawData);
    Logs::saveLog('courses',  $rawData['course']. " Course Undo", $cId, $str,0, 0, 0);
    
  }

  // ready everything to store a module log after undo the changes
  public static function prepUndoModuleLog($mId) {
    $rawData = Logs::getLastRawOfModule($mId);
    $str = Logs::makeString($rawData);
    Logs::saveLog('modules',  $rawData['module']. " Module Undo", $mId, $str,0, 0, 0);
    
  }
  // ready everything to store a mentor log after undo the changes
  public static function prepUndoMentorLog($id) {
    $rawData = Logs::getLastRawOfStudyPlan($id);
    $user = (array) DB::table('users')->where('id', $rawData['user_id'])->first();
    $change_str = 'Undo Mentor name for '. $user['first_name'] . ' ' . $user['last_name'];
    $str = Logs::makeString($rawData);
    Logs::saveLog('study_plans', $change_str, $id, $str, 'mentor_name', 0, 0);
  }
  
  // ready everything to store a user log after undo the changes
  public static function prepUndoUserLog($cId) {
    $rawData = Logs::getLastRawOfUser($cId);
    $roleId = Logs::getRoleId($cId);

    $str = Logs::makeString($rawData);
    $str = '\u100|'. $roleId. $str;

    Logs::saveLog('users',  User::userMitName($cId). " User Updated", $cId, $str,0, 0, 0);
   
  }

  // get stored raw data (as string) from logs table
  public static function getRawDataString($id) {
    $logs = new Logs; 
    return $logs ->where('id', $id)->pluck('raw_data_string');
  }

  //create user update array to store raw to previous stage
  public static function userUpdateArray($array) {
    return array( 
                  'first_name'           => $array[3],
                  'last_name'            => $array[4],
                  'email'                => $array[6],
                  'title'                => $array[7],
                  'university'           => $array[27],
                  'institute'            => $array[12],
                  'institute_url'        => $array[13],
                  'personal_website_url' => $array[14],
                  'photo_url'            => $array[15],
                  'location'             => $array[16],
                  'academic_career'      => $array[17],
                  'occupation'           => $array[18],
                  'activity'             => $array[24],
                  'post_address'         => $array[25],
                  'telephone'            => $array[26]

                  // 'role_id'    => $array[1]
                );
  }

  //create course update array to store raw to previous stage
  public static function courseUpdateArray($array) {
    return array( 'course'                       => $array[2],
                  'module'                       => $array[3],
                  'semester'                     => $array[4],
                  'startdate'                    => $array[5],
                  'site'                         => $array[19],
                  'credits'                      => $array[6],
                  'lecturer'                     => $array[12],   
                  'lecturer_url'                 => $array[13],     
                  'time'                         => $array[14],      
                  'room'                         => $array[15],              
                  'video_recording_download_url' => $array[16],    
                  'live_web_feed_url'            => $array[17],   
                  'interactive_live_stream'      => $array[26],   
                  'video_recording'              => $array[23],   
                  'lecture_hall_transmission'    => $array[24],   
                  'live_stream'                  => $array[25],   
                  'attendance_required'          => $array[27],
                  'partial_attendance_required'  => $array[28],    
                  'assistant'                    => $array[20],  
                  'comment'                      => $array[18],
                  'mode'                         => $array[21],

                );
  }

  //create module update array to store raw to previous stage
  public static function moduleUpdateArray($array) {
    return array( 'module'                      => $array[2],
                  'module_no'                   => $array[3],
                  'topic_category'              => $array[4],
                  'type'                        => $array[8],
                  'weekly_composition'          => $array[9],
                  'exam'                        => $array[10],
                  'exam_duration'               => $array[11],
                  'ECTS_credits'                => $array[12],
                  'required_hours_of_work'      => $array[13],
                  'required_work_in_hours'      => $array[14],
                  'person_responsible'          => $array[15],
                  'person_responsible_2'        => $array[16],
                  'semester'                    => $array[17],
                  'teaching_methods'            => $array[18],
                  'module_description'          => $array[19],
                  'miscellaneous_Comments'      => $array[20],
                  'recommended_prerequisite_knowledge'=> $array[21],
                  'recommended_literature'      => $array[22],
                  'outcomes'                    => $array[23],
                  'site'                        => $array[24],
                  'site_2'                      => $array[25],
                  'examId'                      => $array[26],

                );
  }

  // send the raw data string of course and preparing array to update
  public static function getCourseUpdateArray($id) {
    $array = Logs::makeArray(Logs::getRawDataString($id));
    return Logs::courseUpdateArray($array);
  }
  
   //create mentor update array to store raw to previous stage
  // Humaun Rashid
  public static function mentorUpdateArray($array) {
      $user = (array) DB::table('users')->where('id', $array[2])->first();
      return array(
          'user_id'                      => $array[2],
          'name'                         => $user['first_name'].' '.$user['last_name'],
          'mentor_name'                  => $array[4]
      );
  }

  // send the raw data string of study_plan_mentor and preparing array to update
  public static function getMentorUpdateArray($id) {
    $array = Logs::makeArray(Logs::getRawDataString($id));
    //BaseController::_setTrace($array);        
    return Logs::mentorUpdateArray($array);
  }

  public static function updateUserRoolePivot($rId, $userId) {
    $user = User::find($userId);
    $user->rooles()->detach(); 
    $user->rooles()->attach($rId); //this executes the insert-query
    Session::set($userId, $rId);
  }

  // send the raw data string of user and preparing array to update
  public static function getUserUpdateArrayForView($id, $userId) {
    $array = Logs::makeArray(Logs::getRawDataString($id));
    return Logs::userUpdateArray($array);
  }

  // send the raw data string of user and preparing array to update
  public static function getUserUpdateArray($id, $userId) {
    $array = Logs::makeArray(Logs::getRawDataString($id));
    Logs::updateUserRoolePivot($array[1], $userId);
    return Logs::userUpdateArray($array);
  }


  // send the raw data string of module and preparing array to update
  public static function getModuleUpdateArray($id) {
    $array = Logs::makeArray(Logs::getRawDataString($id));
    return Logs::moduleUpdateArray($array);
  }

  // show all logs for perticular course or user or module 
  public static function getLogsById($id, $table, $str) {
    $logs = Logs::where('raw_id_in_table', '=', $id)
                     ->where('table_name', '=', $table)
                     ->orderBy('id', 'DESC')
                     ->paginate(6);
    echo View::make('layouts.logs.showAllLogs', ['logs' => $logs, 'str' => "Log History of ".$str, 'successMsg' => 0]);
  }

  // this is read whole logs table and convert from DB object to array
  public static function readAllLogs() {
    return Logs::all()->toArray();
  }
  
  //To get information about the columns in a table
  public function describe(){
        $table = $this->getTable();
        $pdo = \DB::connection()->getPdo();
        $do= $pdo->query("describe $table")->fetchAll();  
        return $do;
  }

}
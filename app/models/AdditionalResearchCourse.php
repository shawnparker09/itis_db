<?php

class AdditionalResearchCourse extends \Eloquent {
	protected $fillable = [];
    protected $table = 'additional_research_courses';
    //blacklist in import
    protected $guarded = array('id', 'created_at', 'updated_at');
}
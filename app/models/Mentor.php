<?php

class Mentor extends \Eloquent {
	protected $fillable = [];
    protected $table = 'mentors';
    //blacklist in import
    protected $guarded = array('id', 'created_at', 'updated_at');

    public static function readLogMitPagination($noOfPage){
        return DB::table('mentors')->paginate($noOfPage);
    }

    // making rules for validation to create a new user rest of the rules used from casualValidationRuleMaker function
    public static function validationRuleMakerForCreate()	{
        $rules = array();
        $toPush = array( 'mentor_name' => 'required');
        return array_merge($rules, $toPush);

    }

    public static function allMentors() {
        //$data = DB::table('mentors')->get();
        $data = array();
        $others = DB::table('users')
            ->join('users_rooles', 'users.id', '=', 'users_rooles.user_id' )
            ->whereIn('roole_id', [1,4,5])
            ->get(array('users.id','first_name','last_name','title'));
        foreach($others as $new) {
            $new->mentors_name = $new->title . " " . $new->first_name . " " . $new->last_name;
            array_push($data,$new);
        }
        #BaseController::_setTrace($data);
        return $data;
    }

}
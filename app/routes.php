<?php
// this is share the language localization variable to all the view
// share once and use N time
// View::share('languageDecision', User::getUserLanguage("languageOf".Auth::id()));

function isActiveRoute($route, $output = "active")
{
    if (Route::currentRouteName() == $route) return $output;
}
Route::group(['before' => 'auth|rolAuth|localization'], function() {
	// sets of routes to course handeling
	Route::get('/showallcourses', array('as' => 'showallcourses','uses' => 'CourseController@index'));
	Route::post('showallcourses', array('as' => 'showallcourses','uses' => 'CourseController@index'));  // with search string
	Route::get('/createcourse', array('uses' => 'CourseController@create'));
	Route::post('createcourse', array('uses' => 'CourseController@store'));

	Route::get('/sortbyreq', array('uses' => 'CourseController@sortByReq'));  // to year and semester selection
	Route::post('/sortbyreq', array('uses' => 'CourseController@sortByReq'));  // to year and semester selection

	Route::post('/editcourses/{id}/{pageNumber?}', array('uses' => 'CourseController@edit'));
	Route::get('/editcourses/{id}/{pageNumber?}', array('uses' => 'CourseController@edit'));
	Route::post('updatecourse', array('uses' => 'CourseController@update'));
	Route::get('updatecourse', array('uses' => 'CourseController@update'));

	Route::post('/deletecourse/{id}/{name}', array('uses' => 'CourseController@delete'));
	Route::post('/undodeleteedcourse/{id}/{name}', array('uses' => 'CourseController@undoDelete'));
	
    // routes for UserController
	Route::get('/showallusers', array('as' => 'showallusers','uses' => 'UserController@index'));
	Route::post('/showallusers', array('as' => 'showallusers','uses' => 'UserController@index')); // with search string
	
	Route::post('/sortbyrequser', array('uses' => 'UserController@sortByReq'));
	Route::get('/sortbyrequser', array('uses' => 'UserController@sortByReq')); 

	Route::get('/createuser', array('uses' => 'UserController@create'));
	Route::post('createuser', array('uses' => 'UserController@store'));

	Route::post('/edituser/{id}/{pageNumber}', array('uses' => 'UserController@edit'));
	Route::get('/edituser/{id}/{pageNumber}', array('uses' => 'UserController@edit'));
	Route::post('updateuser', array('uses' => 'UserController@update'));

	Route::get('/editpassword', array('uses' => 'UserController@editpassword'));
	Route::post('/updatepassword', array('uses' => 'UserController@updatepassword'));

	Route::post('/deleteuser/{id}', array('uses' => 'UserController@deleteUser'));
	Route::post('/undodeleteeduser/{id}', array('uses' => 'UserController@undoDeleteUser'));

	Route::post('viewAs', array('uses' => 'UserController@viewAs'));
	
	// routes for LogController
	Route::post('showlogs', array('as' => 'showlogs','uses' => 'LogController@index'));
	Route::get('/showlogs', array('as' => 'showlogs','uses' => 'LogController@index'));

	Route::post('/undologs/{id}/{cid}/{table}/{pageNumber}', array('uses' => 'LogController@undo'));
	Route::post('/viewlog/{id}/{cid}/{table}/{pageNumber}', array('uses' => 'LogController@viewLog'));

	Route::post('/deletebylogs/{id}/{table}/{pageNumber}', array('uses' => 'LogController@checkDelete'));
	Route::post('/undodeletebylogs/{id}/{table}/{pageNumber}', array('uses' => 'LogController@checkUndoDelete'));

	Route::post('historylogs/{id}/{table}/{course}', array('uses' => 'LogController@history'));
	Route::get('historylogs/{id}/{table}/{course}', array('uses' => 'LogController@history'));
	
	// routes for modules
	Route::post('createmodule', array('uses' => 'ModuleController@store'));
	Route::get('/createmodule', array('uses' => 'ModuleController@create'));

	Route::post('showallmodules', array('as' => 'showallmodules','uses' => 'ModuleController@index'));
	Route::get('/showallmodules', array('as' => 'showallmodules','uses' => 'ModuleController@index'));
	Route::post('sortbyreqmodule', array('uses' => 'ModuleController@sortByReq'));  // to Topic Category selection
	Route::get('/sortbyreqmodule', array('uses' => 'ModuleController@sortByReq'));  // to Topic Category selection

	// following five routes should be access by admin level user
	// changes maded in view
	// only need to update in database
	Route::post('editmodules/{id}/{pageNumber}', array('uses' => 'ModuleController@edit'));
	Route::get('/editmodules/{id}/{pageNumber}', array('uses' => 'ModuleController@edit'));
	Route::post('updatemodule', array('uses' => 'ModuleController@update'));
	Route::post('/deletemodule/{id}/{table}', array('uses' => 'ModuleController@destroy'));
	Route::post('/undodeleteedmodule/{id}/{table}', array('uses' => 'ModuleController@undoDelete'));

    // new routes to grouping
    // --------------------------------------------------
    // --------------------------------------------------
    // --------------------------------------------------
    // --------------------------------------------------

    // to log out
    Route::get('logout', array('uses' => 'LoginController@doLogout'));
    //language setting
    Route::get('/changeLanguage', array('uses' => 'UserController@changeLanguage'));
    // change view mode Eg : detaled view vs short view
    Route::get('/changeViewMode', array('uses' => 'HelperController@changeViewMode'));
    // sets of routes to course handeling
    Route::get('/showallcourseswith', array('uses' => 'CourseController@indexWithPage'));
    // routes for UserController
    Route::get('/showalluserswith', array('uses' => 'UserController@indexWithPage'));
    Route::get('/showdetails', array('uses' => 'UserController@details'));
    Route::post('/showdetails', array('uses' => 'UserController@details'));
    // routes for LogController
    Route::get('/showlogswith', array('uses' => 'LogController@indexWithPage'));
    // routes for modules
    Route::get('/showallmoduleswith', array('uses' => 'ModuleController@indexWithPage'));
    // routes for exporting XML files
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'ModuleController@export', 'role' => Assistant],
    Route::get('/exportmodule',array('uses' => 'ModuleController@export'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'ModuleController@export', 'role' => Assistant],
    Route::post('/exportmodule',array('uses' => 'ModuleController@export'));
    Route::get('/help', array('as' => 'help','uses' => 'HelpController@index'));
    // routes for upload files
    // one time use routes
    Route::get('/import', array('uses' => 'ImportController@index'));
    Route::post('/upload', array('uses' => 'ImportController@uploader'));

    // it should verify by fatema
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'UserController@export', 'role' => Assistant],
    Route::get('/exportuser',array('uses' => 'UserController@export'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'UserController@export', 'role' => Assistant],
    Route::post('/exportuser',array('uses' => 'UserController@export'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'LogController@export', 'role' => Assistant],
    Route::get('/exportlog',array('uses' => 'LogController@export'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'LogController@export', 'role' => Assistant],
    Route::post('/exportlog',array('uses' => 'LogController@export'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'CourseController@export', 'role' => Assistant],
    Route::get('/exportcourse',array('uses' => 'CourseController@export'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'CourseController@export', 'role' => Assistant],
    Route::post('/exportcourse',array('uses' => 'CourseController@export'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getFile', 'role' => Assistant],
    Route::get('/downloaduserxml',array('as' => 'userXml','uses' => 'HelperController@getFile'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getFile', 'role' => Assistant],
    Route::get('/downloadcoursexml',array('as' => 'courseXml','uses' => 'HelperController@getFile'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getFile', 'role' => Assistant],
    Route::get('/downloadlogxml',array('as' => 'logXml','uses' => 'HelperController@getFile'));
    // routes for exporting CSV files
    Route::get('/downloadmodulecsv',array('as' => 'moduleCsv','uses' => 'HelperController@getFile'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getFile', 'role' => Assistant],
    Route::get('/downloadusercsv',array('as' => 'userCsv','uses' => 'HelperController@getFile'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getFile', 'role' => Assistant],
    Route::get('/downloadcoursecsv',array('as' => 'courseCsv','uses' => 'HelperController@getFile'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getFile', 'role' => Assistant],
    Route::get('/downloadlogcsv',array('as' => 'logCsv','uses' => 'HelperController@getFile'));
    // routes for exporting PDF files
    Route::get('/downloadmodulepdf',array('as' => 'modulePdf','uses' => 'HelperController@getPdfFile'));

    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getFile', 'role' => Assistant],

    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getPdfFile', 'role' => Assistant],
    Route::get('/downloaduserpdf',array('as' => 'userPdf','uses' => 'HelperController@getPdfFile'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getPdfFile', 'role' => Assistant],
    Route::get('/downloadcoursepdf',array('as' => 'coursePdf','uses' => 'HelperController@getPdfFile'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getPdfFile', 'role' => Assistant],
    Route::get('/downloadlogpdf',array('as' => 'logPdf','uses' => 'HelperController@getPdfFile'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getPdfFile', 'role' => Assistant],
    Route::get('/showallcontacts', array('as' => 'showallcontacts','uses' => 'UserController@studentContacts'));
    Route::post('/showallcontacts', array('as' => 'showallcontacts','uses' => 'UserController@studentContacts')); // with search string
     // ['name' => "will handel by thishanth", 'controller_and_action' => 'UserController@studentContacts', 'role' => Assistant, Procesor, Student],
     // ['name' => "will handel by thishanth", 'controller_and_action' => 'UserController@sortContactByReq', 'role' => Assistant, Procesor, Student],
    Route::post('/sortcontactsbyrequser', array('uses' => 'UserController@sortContactByReq'));
    Route::get('/sortcontactsbyrequser', array('uses' => 'UserController@sortContactByReq'));
    // ['name' => "will handel by thishanth", 'controller_and_action' => 'HelperController@getFile', 'role' => Assistant],
    Route::get('/downloadmodulexml',array('as' => 'moduleXml','uses' => 'HelperController@getFile'));

    // Routes from Humaum Rashid
    Route::post('/coursestore', array('uses' => 'SemesterCoursesController@courseStore'));
    Route::get('/select_mentor', array('uses' => 'StudyPlanController@selectMentor'));
    Route::post('/update_mentor', array('uses' => 'SemesterCoursesController@updateMentor'));
    Route::post('/editstudyplan/{id}/', array('uses' => 'StudyPlanController@edit'));
    Route::get('/editstudyplan/{id}/', array('uses' => 'StudyPlanController@edit'));
    Route::post('/editcourse/', array('uses' => 'SemesterCoursesController@editCourse'));
    Route::post('/getlatestcourses/', array('uses' => 'SemesterCoursesController@getLatestCourses'));
    Route::post('/studyplan', array('as' => 'studyplan','uses' => 'StudyPlanController@createStudyPlan'));
    Route::get('/studyplan', array('as' => 'studyplan','uses' => 'StudyPlanController@createStudyPlan'));
    Route::post('generatePasswordAndSend/{id}/', array('uses' => 'UserController@generatePasswordAndSend'));


    //Route::get('/showallmentors', array('uses' => 'MentorController@index'));
    //Route::get('/editmentor', array('uses' => 'MentorController@edit'));
    //Route::get('/deletementor', array('uses' => 'MentorController@destroy'));
    //Route::get('/undodeletedmentor', array('uses' => 'MentorController@undo_delete'));
});

//to insert into filter

//only for create first user when install app on server
//after that please remove this route
//otherwise its harmfull
Route::get('/createsuperadmin2412008900', array('uses' => 'UserController@createSuperAdmin'));
Route::post('createusersuperadmin2412008900', array('uses' => 'UserController@storeSuperAdmin'));
// Route::get('/atest', array('uses' => 'LawController@index'));
Route::get('/install', array('uses' => 'SettingController@index'));
Route::get('/underwork', array('uses' => 'LogController@underWork'));

// Authendication filters to-be add
// Login
// route to show the login form
Route::get('/', array('uses' => 'LoginController@showLogin'));
Route::get('login', array('uses' => 'LoginController@showLogin'));
// route to process the form
Route::post('login', array('uses' => 'LoginController@doLogin'));
Route::get('mail', array('uses' =>'UserController@sendStudyplanReminder'));


Route::get('/showallmentors', array('uses' => 'MentorController@index'));
Route::get('/creatementor', array('uses' => 'MentorController@create'));
Route::post('creatementor', array('uses' => 'MentorController@store'));
Route::post('/editmentor/{id}/{pageNumber}', array('uses' => 'MentorController@edit'));
Route::post('/updatementor', array('uses' => 'MentorController@update'));
Route::post('/deletementor/{id}', array('uses' => 'MentorController@destroy'));
//Route::get('/undodeletedmentor', array('uses' => 'MentorController@undo_delete'));






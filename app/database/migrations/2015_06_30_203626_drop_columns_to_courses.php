<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsToCourses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('courses', function(Blueprint $table)
		{
			  $table->dropColumn(['second_lecturer', 'second_lecturer_url', 'time_2','room_2','transmission_room_clausthal_2','transmission_room_hannover','transmission_room_hannover_2','transmission_room_göttingen','transmission_room_göttingen_2','transmission_room_braunschweig','transmission_room_braunschweig_2','assistant_braunschweig','assistant_göttingen','assistant_hannover']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('courses', function(Blueprint $table)
		{
			//
		});
	}

}

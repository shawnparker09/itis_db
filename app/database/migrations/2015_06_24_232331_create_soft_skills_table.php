<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftSkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('soft_skills', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable();
		    $table->string('soft_skills_name',500)->nullable();
		    $table->string('site_id')->nullable();
		    $table->float('credit')->nullable();
		    $table->integer('semester')->nullable();
		    $table->string('session')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('soft_skills');
	}

}

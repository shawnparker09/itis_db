<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropInstitute extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()	{
		Schema::table('users', function($table) {
    	$table->dropColumn('institute');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}

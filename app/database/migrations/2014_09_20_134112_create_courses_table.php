<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
    Schema::create('courses', function(Blueprint $table) {
     $table->increments('id');
     $table->string('course');
     $table->string('module');
     $table->string('semester');
     $table->string('type');
     $table->string('startdate');
     $table->string('email');
     $table->integer('credits');
     $table->integer('moduleexamid');
     $table->integer('proffid');
     $table->string('site');
     $table->string('weeklycomposition');
     $table->string('hoursofwork');
     $table->string('link');
     $table->integer('mode');
     $table->boolean('visible');
     $table->timestamps();
     });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		 Schema::drop('courses');
	}

}

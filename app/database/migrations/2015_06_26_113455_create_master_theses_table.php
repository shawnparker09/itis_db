<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterThesesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('master_theses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable();
		    $table->string('master_theses_name',1000)->nullable();
		    $table->float('credit')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('master_theses');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToModulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('modules', function(Blueprint $table)
		{
			$table->string('type',255);
			$table->string('weekly_composition',255)->nullable();
			$table->string('exam',255)->nullable();
			$table->string('exam_duration',255)->nullable();
			$table->integer('ECTS_credits');
			$table->string('required_hours_of_work',255)->nullable();
			$table->string('required_work_in_hours',255)->nullable();
			$table->string('site',255);
			$table->string('site_2',255)->nullable();
			$table->string('person_responsible',255)->nullable();
			$table->string('person_responsible_2',255)->nullable();
			$table->string('semester',255);
			$table->string('teaching_methods',255)->nullable();
			$table->text('module_description')->nullable();
			$table->text('miscellaneous_Comments')->nullable();
			$table->text('recommended_prerequisite_knowledge')->nullable();
			$table->text('recommended_literature')->nullable();
			$table->text('outcomes')->nullable();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('modules', function(Blueprint $table)
		{
			//
		});
	}

}

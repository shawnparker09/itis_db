<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
				Schema::create('roles', function(Blueprint $table)
        {
         $table->integer('user_id');
         $table->boolean('Super_admin');
         $table->boolean('admin');
         $table->boolean('proff');
         $table->boolean('assistance');
         $table->boolean('student');
         $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roles');
	}

}

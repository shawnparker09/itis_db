<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyPlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('study_plans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable();
		    $table->integer('matriculation')->nullable();
		    $table->string('mentor_name',500)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('study_plans');
	}

}

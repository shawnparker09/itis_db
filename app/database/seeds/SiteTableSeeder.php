<?php
 
class SiteTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $vader = DB::table('sites')->insert([
         'site_name'=> 'TU Clausthal'
        ]);


        DB::table('sites')->insert([
         'site_name'=> 'TU Braunschweig'
        ]);

        DB::table('sites')->insert([
         'site_name'=> 'University of Göttingen'
        ]);

        DB::table('sites')->insert([
         'site_name'=> 'Leibniz University of Hanover'
        ]);
                       
    }
}
<?php

class ImportController extends \BaseController {

	//it return view for uploading files
	// one view for all files
	public function index()	{
		return View::make('layouts.settings.import');
	}

	// this will handel the file upload and insert into DB 
	// one function for all files
	public function uploader() {
    Import::storeFile();
	}
}

<?php

class ModuleController extends BaseController {

	// show all modules with pagination und without pagination when search module
	public function index()	{
		if (Input::get('searchInput')) {
			$page = 150;
		}  else {
			$page = 6;
		}

		$modules = Module::readModuleMitPagination($page);
    return View::make('layouts.modules.showAllModules', ['modules' => $modules]);
	}

		//function for send courses with success message
	public function indexWithPage()	{
		// this is for read the courses from database
		$modules = Module::readModuleMitPagination(6);
    // this is for make View
    return View::make('layouts.modules.showAllModules', ['modules' => $modules]);
	}


	// its for show modules by topic category eg: data and information, Networking ect	
	public function sortByReq()	{
    if (!Input::get('categoryName')) {
			$modules = Module::orderBy('updated_at', 'DESC')->paginate(6);
    	// return View::make('layouts.modules.showAllModules', ['modules' => $modules]);

    }elseif (Input::get('categoryName')) {

			$modules = Module::where('topic_category', '=', Input::get('categoryName'))
											->orderBy('updated_at', 'DESC')
											->paginate(200);
    }
	  return View::make('layouts.modules.showAllModules', ['modules' => $modules]);
	}

	// make view for make a new module
	public function create()	{
		echo View::make('layouts.modules.createNewModule');
	}

	// store a new model 
	public function store()	{
		// create rules for validation
		$rules = Module::validationRuleMaker(Input::all());
		// check the validation
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			// get the error messages from the validator
			$messages = $validator->messages();
			// redirect our user back to the form with the errors from the validator
			return Redirect::to('/createmodule')
				->withErrors($validator)->withInput();

	  } elseif (Module::isNotUnique(Input::get('module_no'))) {
	  	return Redirect::back()->withErrors(['Module Id is Already Taken. Please use Different Module ID'])->withInput();

	  } else {

	  $module = new Module;
	  
      $module->module                             = Input::get('module');
      $module->module_no                          = Input::get('module_no');
      $module->topic_category                     = Input::get('topic_category');
      $module->visible                            = 1;
		  $module->weekly_composition                 = Input::get('weekly_composition');
		  $module->exam                               = Input::get('exam');
		  $module->exam_duration	                  = Input::get('exam_duration');
		  $module->ECTS_credits                       = Input::get('ECTS_credits');
		  $module->required_hours_of_work	          = Input::get('required_hours_of_work');
		  $module->required_work_in_hours             = Input::get('required_work_in_hours');
		  $module->site	                              = Input::get('site');
		  $module->site_2                             = Input::get('site_2');
          $module->person_responsible	              = Input::get('person_responsible');
		  $module->person_responsible_2               = Input::get('person_responsible_2');
		  $module->semester	                          = Input::get('semester');
		  $module->teaching_methods	                  = Input::get('teaching_methods');
		  $module->module_description	              = Input::get('module_description');
		  $module->miscellaneous_Comments	          = Input::get('miscellaneous_Comments');
		  $module->recommended_prerequisite_knowledge = Input::get('recommended_prerequisite_knowledge');
		  $module->recommended_literature	          = Input::get('recommended_literature');
		  $module->outcomes	                          = Input::get('outcomes');
		  $module->examId                             = Input::get('examId');
      
      $module->save();

      // log for saved new module
      Logs::saveLog("modules", Input::get('module'). " Module Created", DB::getPdo()->lastInsertId(), "", 0,1, 0);

      //echo View::make('layouts.messages.successfullyCreated');
      HelperActions::putMessageToSession("Module created successfully :)");
      return Redirect::to('/showallmoduleswith?page=');
	  }
	}

	// its for make view for edit a created module
	public function edit($id, $pageNumber)	{
		$module= new Module;
	  $edit=$module->where('id',array($id))->first();
  	$editModule = array($edit);
  	$sites = Site::makeSiteArray();

  	HelperActions::putPageNumberToSession($pageNumber);
  
  	return View::make('layouts.modules.editModule', array('module' => $editModule, 'sites' => $sites));
	}

	// its for save editied module
	public function update()	{
		$module= new Module;
		$editId = Input::get('id');
		$pageNumber = HelperActions::getPageNumberFromSession();

		// create rules for validation
		$rules = Module::validationRuleMakerForUpdate(Input::all());
		// check the validation
		$validator = Validator::make(Input::all(), $rules);
		$errorArray = Module::isUnique($editId);

		if ($validator->fails()) {
			// get the error messages from the validator
			$messages = $validator->messages();
			// redirect our user back to the form with the errors from the validator
			return Redirect::to('/editmodules/'.$editId.'/'.$pageNumber)
				->withErrors($validator)->withInput();

	  } elseif (sizeof($errorArray) > 1) {
	  	return Redirect::back()->withErrors($errorArray)->withInput();

	  } else {

		$updateArray = Module::updateArrayMaker(Input::all());
		Logs::prepModuleLog($editId);

		$module->where('id', $editId)
            ->update($updateArray);
    HelperActions::putMessageToSession("Your Changes updated successfully :)");
    return Redirect::to('/showallmoduleswith?page='.$pageNumber);
		//echo View::make('layouts.messages.successMessage');
	  }
	}

	// this is for delete a module
	public function destroy($id) {
		Module::deleteModule($id);
		HelperActions::putMessageToSession("Module deleted successfully :)");
		return Redirect::to('/showallmoduleswith?page='.HelperActions::getPageNumberFromSession());
	}

	// this is for retrive module from delete
	public function undoDelete($id) {
		Module::undoDeleteModule($id);
		HelperActions::putMessageToSession("Module retrieved successfully :)");
		return Redirect::to('/showallmoduleswith?page='.HelperActions::getPageNumberFromSession());
	}
    
    //To understand which export format user need
	public function export()
    {   
    if (Input::get('exportType')==='xml')
      {return Redirect::to('/downloadmodulexml');}
    elseif(Input::get('exportType')==='csv')
      {return Redirect::to('/downloadmodulecsv');}
    else
      {return Redirect::to('/downloadmodulepdf');}
    }



}

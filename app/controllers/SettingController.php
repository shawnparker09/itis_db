<?php

class SettingController extends \BaseController {

	public function index()	{
		Settings::isMigrationSuccess();
		$msg = Settings::isSeedingAndInsertSuccess();
		return View::make('layouts.settings.install', ['msg' => $msg]);
	}

}

<?php

class CourseController extends BaseController {

	// this function for show all courses regards with pagination and without pagination
	public function index() {
    // this if condition check, is this request for search course or not
		if (Input::get('searchInput')) {
			$page = 150;
		}  else {
			$page = 6;
		}
		// this is for read the courses from database
		$courses = Course::readCourseMitPagination($page);
        // this is for make View
        return View::make('layouts.courses.showAllCourses', ['courses' => $courses]);
	}

	//function for send courses with success message
	public function indexWithPage()	{
		// this is for read the courses from database
		$courses = Course::readCourseMitPagination(6);
        // this is for make View
        return View::make('layouts.courses.showAllCourses', ['courses' => $courses]);
	}

	// this function for show courses regards semester und year
	public function sortByReq()	{
		// this if condition activate when there is no semester selected
        if (!Input::get('semester')) {
			$courses = Course::orderBy('updated_at', 'DESC')->paginate(6);
    	    // return View::make('layouts.courses.showAllCourses', ['courses' => $courses]);

    		// this if condition activate when there is semester selected
        } elseif (Input::get('semester')) {

			$courses = Course::where('semester', '=', Input::get('semester'))
											->orderBy('updated_at', 'DESC')
											->paginate(200);
        }
	  
	    return View::make('layouts.courses.showAllCourses', ['courses' => $courses]);
	}

    // this is create view for make new courses
	public function create(){
			echo View::make('layouts.courses.createNewCourse');
	}

  // here is the storing action of make new course
	public function store()	{


  	if (Input::get('toggler') == 2) {
  		$semesters = Course::calculateNoOfSemesterV2(Input::all());
  		$noOfSemester = sizeof($semesters);
    	// creating validation rules 
			$rules = Course::validationRuleMakerForMultipleCourse(Input::all());

  	} else {
  		$noOfSemester = 1;
  		$semesters = array(Input::get('semester'));
    	// creating validation rules 
			$rules = Course::validationRuleMaker(Input::all());
  	}

		// validating the result with rules and actualinput by the help of default functions
		$validator = Validator::make(Input::all(), $rules);
		// this condition check the validation result

		if ($validator->fails()) {

			// get the error messages from the validator
			$messages = $validator->messages();

			// redirect our user back to the form with the errors from the validator
			return Redirect::to('/createcourse')
				->withErrors($validator)->withInput();
	  } else {

	  	$i = 0;
	  	while ($i < $noOfSemester) {
		  	$course = new Course;
	      $course->course                          = Input::get('course');
	      $course->module                          = Module::getModuleName(Input::get('module'));
	      $course->module_id                       = Input::get('module');
	      $course->semester                        = $semesters[$i];
	      $course->mode                            = Input::get('mode');
	      $course->startdate                       = Input::get('startDate');
	      $course->credits                         = Input::get('credits');
	      $course->visible                         = 1;
	      $course->site                            = Input::get('site');
	      $course->lecturer                        = Input::get('lecturer');
	      $course->lecturer_url                    = Input::get('lecturer_url');
	      $course->time                            = Input::get('time');
	      $course->room                            = Input::get('room');                
			  $course->video_recording          			 = Input::get('video_recording');
			  $course->lecture_hall_transmission       = Input::get('lecture_hall_transmission');
			  $course->video_recording_download_url    = Input::get('video_recording_download_url');
			  $course->live_stream                     = Input::get('live_stream');
			  $course->live_web_feed_url               = Input::get('live_web_feed_url');
			  $course->interactive_live_stream         = Input::get('interactive_live_stream');
			  $course->attendance_required        		 = Input::get('attendance_required');
			  $course->partial_attendance_required   	 = Input::get('partial_attendance_required');
			  $course->transmission_room               = Input::get('transmission_room');
			  $course->assistant                       = Input::get('assistant');
			  $course->comment                         = Input::get('comment');
	      
			  $i++;
	 			// this is storing into DB
	      $course->save();
	      // this is for storing logs
	      Logs::saveLog("courses", Input::get('course'). " Course Created", DB::getPdo()->lastInsertId(), "", 0,1, 0);
	  	}
      HelperActions::putMessageToSession("Course created successfully :)");
      return Redirect::to('/showallcourseswith?page=1');
	  }
	}

	// this is for make view to edit a course
	public function edit($id, $pageNumber = '1')	{
			$course= new Course;
	    $editCourse = array($course->where('id',array($id))->first());
	    $modules = Module::makeModuleArray();
	    $site = Site::makeSiteArray();

	  	HelperActions::putPageNumberToSession($pageNumber);

	    echo View::make('layouts.courses.editCourse', array('course' => $editCourse, 'modules' => $modules, 'site'=> $site));
	}

	// this is for update the edited course
	public function update()	{
		$editId = Input::get('id');
		$sessionRole = Session::get(Auth::id());
	  $pageNumber = HelperActions::getPageNumberFromSession();
		
		if ($sessionRole <= 2) {
			$updateArray = Course::importantUpdateArrayMaker(Input::all());
			$rules = Course::validationRuleMaker(Input::all());
		} else {
			$updateArray = Course::casualUpdateArrayMaker(Input::all());
			$rules = Course::CasualValidationRuleMaker(Input::all());
		}
		
		// validating the result with rules and actualinput by the help of default functions
		$validator = Validator::make(Input::all(), $rules);

		// this condition check the validation result
		if ($validator->fails()) {

			// get the error messages from the validator
			$messages = $validator->messages();

			// redirect our user back to the form with the errors from the validator
			return Redirect::to('/editcourses/'.$editId.'/'.$pageNumber)
				->withErrors($validator)->withInput();
	  } else {

	    $course = new Course;

			Logs::prepCourseLog($editId);
			$course ->where('id', $editId)
	                ->update($updateArray);
			//echo View::make('layouts.messages.successMessage');
	  	HelperActions::putMessageToSession("Your Changes updated successfully :)");
	    return Redirect::to('/showallcourseswith?page='.$pageNumber);

	  }		 
	}

	// this is for retrive a course from delete
	public function undoDelete($id, $name)	{
		Course::undoDeleteCourse($id);
		HelperActions::putMessageToSession("Course retrieved successfully :)");
	  return Redirect::to('/showallcourseswith?page='.HelperActions::getPageNumberFromSession());
	}

	// this is for delete a course
	public function delete($idToDelete, $name)	{
		Course::deleteCourse($idToDelete);
		HelperActions::putMessageToSession("Course deleted successfully :)");
	    return Redirect::to('/showallcourseswith?page='.HelperActions::getPageNumberFromSession());
	}
   
    //To understand which export format user need
    public function export()
    {  
        if (Input::get('exportType')==='xml')
        {
            return Redirect::to('/downloadcoursexml');
        }
        elseif(Input::get('exportType')==='csv')
        {
            return Redirect::to('/downloadcoursecsv');
        }
        else
        {
            return Redirect::to('/downloadcoursepdf');
        }
    }
}

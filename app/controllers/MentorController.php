<?php

class MentorController extends BaseController {

    /**
     * Display a listing of the resource.
     * GET /mentors
     *
     * @return Response
     */
    public function index()
    {
        return View::make('layouts.mentors.showAllMentors', ['mentors' => Mentor::readLogMitPagination(6), 'successMsg' => 0]);
    }

    /**
     * Show the form for creating a new resource.
     * GET /mentors/create
     *
     * @return Response
     */
    public function create()
    {
        echo View::make('layouts.mentors.createNewMentor');
    }

    /**
     * Store a newly created resource in storage.
     * POST /mentors
     *
     * @return Response
     */
    public function store()	{

        $rules = Mentor::validationRuleMakerForCreate();
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            // get the error messages from the validator
            $messages = $validator->messages();
            // redirect our user back to the form with the errors from the validator
            return Redirect::to('/creatementor')
                ->withErrors($validator)->withInput();
        } else {

            $mentor = new Mentor;
            $mentor->mentors_name  =  Input::get('mentor_name');
            $mentor->save();
            //$idToSend = DB::getPdo()->lastInsertId();

            //Logs::saveLog("users", Input::get('title')." ".Input::get('first_name')." ".Input::get('last_name'). " User Created", $idToSend, "",0, 1, 0);

            HelperActions::putMessageToSession("Mentor created successfully :)");
            return Redirect::to('/showallmentors');
        }
    }

    /**
     * Display the specified resource.
     * GET /mentors/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /mentors/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($mId, $pageNumber = '1')
    {
        $mentor= new Mentor;
        $edit = DB::table('mentors')->where('id',array($mId))->first();
        $editMentor = array($edit);

        HelperActions::putPageNumberToSession($pageNumber);
        #BaseController::_setTrace($editMentor);
        return View::make('layouts.mentors.editMentor', array('mentor' => $edit));
    }

    /**
     * Update the specified resource in storage.
     * PUT /mentors/{id}
     *
     * @param  int  $id
     * @return Response
     */
    // this is for store the edited information
    public function update() {

        $editId = Input::get('id');
        $updateArray = array('mentors_name' => Input::get('mentor_name'));
        $rules = Mentor::validationRuleMakerForCreate();
        $validator = Validator::make(Input::all(), $rules);
        $pageNumber = HelperActions::getPageNumberFromSession();

        if ($validator->fails()) {
            // get the error messages from the validator
            $messages = $validator->messages();
            // redirect our user back to the form with the errors from the validator
            return Redirect::to('/creatementor')
                ->withErrors($validator)->withInput();
        } else {

            $mentor = new Mentor;
            $mentor->where('id', $editId)
                ->update($updateArray);
            //$idToSend = DB::getPdo()->lastInsertId();

            //Logs::saveLog("users", Input::get('title')." ".Input::get('first_name')." ".Input::get('last_name'). " User Created", $idToSend, "",0, 1, 0);

            HelperActions::putMessageToSession("Mentor updated successfully :)");
            return Redirect::to('/showallmentors');

            //return $pageNumber > 0 ? Redirect::to('/showalluserswith?page='.$pageNumber) : Redirect::to('/showallcourseswith');
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /mentors/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($deleteMentor)
    {
        $mentor = new Mentor;
        $mentor->where('id',array($deleteMentor))->delete();
        HelperActions::putMessageToSession("Mentor deleted successfully :)");
        return Redirect::to('/showallmentors');
    }

}
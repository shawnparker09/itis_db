<?php

class BaseController extends Controller {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }

    
    /**
     * @param null $data
     * @param bool $die
     */
    public static function _setTrace($data=null,$die=true){
        if(is_string($data)){
            print $data;
        }else{

            print "<pre>";
            print_r($data);
            print "</pre>";
        }
        print "<hr />";
        if($die){
            exit();
        }
    }

    /**
     * @param $array
     * @param $key
     * @param $value
     * @return array
     */
    public static function search($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, self::search($subarray, $key, $value));
            }
        }

        return $results;
    }

}

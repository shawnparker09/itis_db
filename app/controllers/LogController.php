<?php

class LogController extends BaseController {

	// this is for show all logs with pagination
	public function index()	{
   	    return View::make('layouts.logs.showAllLogs', ['logs' => Logs::readLogMitPagination(6), 'successMsg' => 0]);
	}

	// this is for show all logs with pagination and success_message
	public function indexWithPage()	{
    // this is for make View
   	return View::make('layouts.logs.showAllLogs', ['logs' => Logs::readLogMitPagination(6), 'successMsg' => 1]);
	}	

	// this function for undo a action (edited, created, deleted und retrived from delete)
	public function undo($id, $cid, $table, $pageNr) {
		// this condition check is this action a course
		if ($table == "courses") {
			$updateArray = Logs::getCourseUpdateArray($id, $cid);
			Logs::prepUndoCourseLog($cid);
			$course= new Course;
    	$course ->where('id', $cid)
            ->update($updateArray);

		// this condition check is this action an user
		} elseif ($table == "users") {
			$updateArray = Logs::getUserUpdateArray($id, $cid);
		 	Logs::prepUndoUserLog($cid);
		 	$user= new User;
    	$user ->where('id', $cid)
            ->update($updateArray);
		
		// this condition check is this action an module
		} elseif ($table == "modules") {
 			$updateArray = Logs::getModuleUpdateArray($id, $cid);
		 	Logs::prepUndoModuleLog($cid);
		 	$module= new Module;
    	$module ->where('id', $cid)
            ->update($updateArray);
		}
		// this condition check is this action an study plan
		elseif ($table == "study_plans") {
            $updateArray = Logs::getMentorUpdateArray($id, $cid);
            unset($updateArray['name']);
            Logs::prepUndoMentorLog($cid);
            DB::table('study_plans')->where('id', $cid)->update($updateArray);
        }
		$pageNr = $pageNr > 0 ? $pageNr : HelperActions::getPageNumberFromSession();
    // return View::make('layouts.messages.successfullyUndo');
    return Redirect::to('/showlogswith?page='.$pageNr);
	}

	// this is for view a log with full details
	public function viewLog($id, $cid, $table, $pageNr)	{
		HelperActions::putPageNumberToSession($pageNr);
		if($table == "courses") {
			$updateArray = Logs::getCourseUpdateArray($id, $cid);
		
		} elseif($table == "users") {
			$updateArray = Logs::getUserUpdateArrayForView($id, $cid);

		} elseif($table == "modules") {
			$updateArray = Logs::getModuleUpdateArray($id, $cid);
		}
		elseif($table == "study_plans") {
            $updateArray = Logs::getMentorUpdateArray($id, $cid);
        }
        #BaseController::_setTrace($updateArray);
    return View::make('layouts.logs.detailOfLog', ['array' => $updateArray, "id" => $id, "cid" => $cid, "table" => $table]);
	}

	// this is for delete a course or user or module from log section (by the buttons available in logs)
	public function checkDelete($id, $table, $pageNr)	{
		if ($table == "courses") {
			Course::deleteCourse($id);
		} elseif ($table == "users") {
			User::deleteU($id);
		} elseif ($table == "modules") {
			Module::deleteModule($id);
		}
    return Redirect::to('/showlogswith?page='.$pageNr);
	}

	// this is for undo delete a course or user or module from log section (by the buttons available in logs)
	public function checkUndoDelete($id, $table, $pageNr)	{
		if ($table == "courses") {
			Course::undoDeleteCourse($id);
		} elseif ($table == "users") {
			User::undoDelete($id);
		} elseif ($table == "modules") {
			Module::undoDeleteModule($id);
		}
    return Redirect::to('/showlogswith?page='.$pageNr);
	}

    //To understand which export format user need
    public function export()
    {   
    if (Input::get('exportType')==='xml')
      {return Redirect::to('/downloadlogxml');}
    else
      {return Redirect::to('/downloadlogcsv');}
    }
    
	// just tester function not come in production
	public function underWork() {
    return View::make('layouts.underConstruction');
  }

    // show all logs for a course or module or user
    public function history($id, $table, $str) {
 		Logs::getLogsById($id, $table, $str);
  }


}

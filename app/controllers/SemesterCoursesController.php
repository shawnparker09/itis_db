<?php

class SemesterCoursesController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /semestercourses
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * GET /semestercourses/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /semestercourses
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * GET /semestercourses/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /semestercourses/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /semestercourses/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /semestercourses/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function courseStore()
    {
        $request_data = Request::Input();
        #BaseController::_setTrace($request_data);
        DB::table('study_plans')->where('user_id', '=', Auth::id())->delete();
        DB::table('semester_courses')->where('user_id', '=', Auth::id())->delete();
        DB::table('soft_skills')->where('user_id', '=', Auth::id())->delete();
        DB::table('research_projects')->where('user_id', '=', Auth::id())->delete();
        DB::table('master_theses')->where('user_id', '=', Auth::id())->delete();
        DB::table('additional_research_courses')->where('user_id', '=', Auth::id())->delete();

        $updated_fields = array();
        $updated_tables = array();

        // create study plan
        $model = new StudyPlan();
        $model->user_id = Auth::id();
        $model->matriculation = null;
        $model->save();
        ################################################ All Courses are entry here ###########################################################
        for ($i = 1; $i <= 3; $i++) {
            ### Credits for Course ########
            $credits = !empty($request_data['credits_' . $i]) ? $request_data['credits_' . $i] : array();
            ##Credit is for Soft skill
            $credit = !empty($request_data['credit_' . $i]) ? $request_data['credit_' . $i] : array();
            $sites = !empty($request_data['site_' . $i]) ? $request_data['site_' . $i] : array();
            if (!empty($request_data['semester_' . $i . '_courses'])) {
                foreach ($request_data['semester_' . $i . '_courses'] as $key => $single) {
                    $model = new SemesterCourse();
                    $model->user_id = Auth::id();
                    $model->course_id = $single;
                    $model->credit = $credits[$key];
                    $model->semester = $i;
                    $model->session = $request_data['sem_' . $i . '_session'];
                    $model->save();
                    #BaseController::_setTrace($model);
                    if(!in_array('course_id', $updated_fields))
                        array_push($updated_fields, 'course_id');
                    if(!in_array('semester_courses', $updated_tables))
                        array_push($updated_tables, 'semester_courses');
                }
            }

            // for additional courses
            if (!empty($request_data['semester_' . $i . '_a_r'])) {
                foreach ($request_data['semester_' . $i . '_a_r'] as $key => $single) {
                    $model = new AdditionalResearchCourse();
                    $model->user_id = Auth::id();
                    $model->course_id = $single;
                    $model->credit = $credits[$key];
                    $model->semester = $i;
                    $model->session = $request_data['sem_' . $i . '_session'];
                    $model->save();
                    #BaseController::_setTrace($model);
                    if(!in_array('course_id', $updated_fields))
                        array_push($updated_fields, 'course_id');
                    if(!in_array('additional_research_courses', $updated_tables))
                        array_push($updated_tables, 'additional_research_courses');
                }
            }

            ############################################### Soft skills entry################################################
            if (!empty($request_data['skill_' . $i])) {
                foreach ($request_data['skill_' . $i] as $key => $single) {
                    $model = new SoftSkill();
                    $model->user_id = Auth::id();
                    $model->soft_skills_name = $single;
                    $model->site_id = $sites[$key];
                    $model->credit = $credit[$key];
                    $model->semester = $i;
                    $model->session = $request_data['sem_' . $i . '_session'];
                    $model->save();
                    #BaseController::_setTrace($model);
                    if(!in_array('soft_skills_name', $updated_fields))
                        array_push($updated_fields, 'soft_skills_name');
                    if(!in_array('soft_skills', $updated_tables))
                        array_push($updated_tables, 'soft_skills');
                }
            }
        }

        if (!empty($request_data["semester_2_project"])) {
            $model = new ResearchProject();
            $model->user_id = Auth::id();
            $model->research_projects_name = $request_data["semester_2_project"];
            $model->credit = 30;
            $model->semester = 2;
            $model->save();

            array_push($updated_fields, 'research_projects_name');
            array_push($updated_tables, 'research_projects');
        }
        if (!empty($request_data["semester_3"])) {
            $model = new ResearchProject();
            $model->user_id = Auth::id();
            $model->research_projects_name = $request_data["semester_3"];
            $model->credit = 30;
            $model->semester = 3;
            $model->save();

            array_push($updated_fields, 'research_projects_name');
            array_push($updated_tables, 'research_projects');
        }
        if (!empty($request_data["semester_4"])) {
            $model = new MasterThesis();
            $model->user_id = Auth::id();
            $model->master_theses_name = $request_data["semester_4"];
            $model->credit = 30;
            $model->save();

            array_push($updated_fields, 'master_theses_name');
            array_push($updated_tables, 'master_theses');
        }

        $user = (array) DB::table('users')->where('id', Auth::id())->first();
        $changeString = "Study Plan created for ".$user['first_name']." ".$user['last_name'];
        $fields = implode(',', $updated_fields);
        $tables = implode(',', $updated_tables);
        $plan_obj = DB::table('study_plans')->where('user_id', '=' , Auth::id())->first();
        #BaseController::_setTrace($plan_id);
        Logs::saveLog($tables, $changeString, $plan_obj->id, "", $fields, 0,0);

        die(json_encode(array('success' => true)));
    }

    public function editCourse()
    {
        $request_data = Request::Input();
        #BaseController::_setTrace($request_data);
        $semester = $request_data['semester'];
        $user_id = $request_data['user_id'];

        $updated_fields = array();
        $updated_tables = array();

        if ($semester == 4) {
            DB::table('master_theses')->where('user_id', '=', $user_id)->delete();
            if (!empty($request_data["semester_4"])) {
                $model = new MasterThesis();
                $model->user_id = $user_id;
                $model->master_theses_name = $request_data["semester_4"];
                $model->credit = 30;
                $model->save();
                array_push($updated_fields, 'master_theses_name');
                array_push($updated_tables, 'master_theses');
            }
        } elseif ($semester == 5) {
            $plan_id = DB::table('study_plans')->where('user_id', '=', $user_id)->first();
            #BaseController::_setTrace($plan_id);
            $model = new StudyPlan();
            $update_array = array(
                'user_id' => $user_id,
                'matriculation' => null,
                'mentor_name' => $request_data["mentor"]
            );
            $data = $model->where('id', $plan_id->id)->update($update_array);
            Logs::prepStudyPlanLog($plan_id->id);
            array_push($updated_fields, 'mentor_name');
            array_push($updated_tables, 'study_plans');
        } else {
            $session = $request_data['session'];
            DB::table('semester_courses')->where('user_id', '=', $user_id)->where('semester', '=', $semester)->where('session', '=', $session)->delete();
            DB::table('soft_skills')->where('user_id', '=', $user_id)->where('semester', '=', $semester)->delete();
            DB::table('research_projects')->where('user_id', '=', $user_id)->where('semester', '=', $semester)->delete();
            DB::table('additional_research_courses')->where('user_id', '=', $user_id)->where('semester', '=', $semester)->where('session', '=', $session)->delete();

            $credits = !empty($request_data['credits']) ? $request_data['credits'] : array();
            ##Credit is for Soft skill
            $credit = !empty($request_data['soft_credits']) ? $request_data['soft_credits'] : array();
            $sites = !empty($request_data['soft_sites']) ? $request_data['soft_sites'] : array();
            #BaseController::_setTrace($credit);            
            if (!empty($request_data['courses'])) {
                foreach ($request_data['courses'] as $key => $single) {
                    $model = new SemesterCourse();
                    $model->user_id = $user_id;
                    $model->course_id = $single;
                    $model->credit = $credits[$key];
                    $model->semester = $semester;
                    $model->session = $request_data['session'];
                    $model->save();
                    if(!in_array('course_id', $updated_fields))
                        array_push($updated_fields, 'course_id');
                    if(!in_array('semester_courses', $updated_tables))
                        array_push($updated_tables, 'semester_courses');

                }
            }

            if (!empty($request_data['additional_research'])) {
                foreach ($request_data['additional_research'] as $key => $single) {
                    $model = new AdditionalResearchCourse();
                    $model->user_id = $user_id;
                    $model->course_id = $single;
                    $model->credit = $credits[$key];
                    $model->semester = $semester;
                    $model->session = $request_data['session'];
                    $model->save();
                    if(!in_array('course_id', $updated_fields))
                        array_push($updated_fields, 'course_id');
                    if(!in_array('additional_research_courses', $updated_tables))
                        array_push($updated_tables, 'additional_research_courses');

                }
            }

            if (!empty($request_data['soft_skills'])) {
                foreach ($request_data['soft_skills'] as $key => $single) {
                    $model = new SoftSkill();
                    $model->user_id = $user_id;
                    $model->soft_skills_name = $single;
                    $model->site_id = $sites[$key];
                    $model->credit = $credit[$key];
                    $model->semester = $semester;
                    $model->session = $request_data['session'];
                    $model->save();
                    if(!in_array('soft_skills_name', $updated_fields))
                        array_push($updated_fields, 'soft_skills_name');
                    if(!in_array('soft_skills', $updated_tables))
                        array_push($updated_tables, 'soft_skills');
                    #BaseController::_setTrace($model);
                }
            }
            if (!empty($request_data["semester_2_project"])) {
                $model = new ResearchProject();
                $model->user_id = $user_id;
                $model->research_projects_name = $request_data["semester_2_project"];
                $model->credit = 30;
                $model->semester = 2;
                $model->save();
                array_push($updated_fields, 'soft_skills_name');
                array_push($updated_tables, 'soft_skills');
            }
            if (!empty($request_data["semester_3"])) {
                $model = new ResearchProject();
                $model->user_id = $user_id;
                $model->research_projects_name = $request_data["semester_3"];
                $model->credit = 30;
                $model->semester = 3;
                $model->save();
                array_push($updated_fields, 'research_projects_name');
                array_push($updated_tables, 'research_projects');
            }
        }
        $fields = implode(',', $updated_fields);
        $tables = implode(',', $updated_tables);
        $plan_obj = DB::table('study_plans')->where('user_id', '=', $user_id)->first();
        #BaseController::_setTrace($plan_id);        
        Logs::saveLog($tables, "Study Plan Updated", $plan_obj->id, "", $fields, 0,0);
        die(json_encode(array('success' => true)));
    }

    public function updateMentor()
    {
        $request_data = Request:: Input();
        #BaseController::_setTrace($request_data);
        $model = new StudyPlan();

        $mentor_full = DB::table('users')
            ->where('users.id', '=', $request_data["mentor"])
            ->get(array('users.id','first_name','last_name','title'));
        $mentor_full[0]->mentors_name = $mentor_full[0]->title . " " . $mentor_full[0]->first_name . " " . $mentor_full[0]->last_name;
        $update_array = array(
            'mentor_id' => $request_data["mentor"],
            'mentor_name' => $mentor_full[0]->mentors_name
        );

        Logs::prepMentorLog($request_data['msg'], $request_data['id']);
        $data = $model->where('id', $request_data['id'])->update($update_array);
        //Logs::saveLog("study_plans", $request_data['msg'],$request_data['id'], "", 0, 0,0);

        return Redirect::to('select_mentor')->with('msg' , $request_data["msg"]);

        /*$study_plans = array();
        $study_plans = StudyPlan::allStudyPlans();
        return View::make('layouts.studyplan.selectMentor',
            array(
                'study_plans' => $study_plans,
                'msg' => $request_data["msg"]
            )
        );*/
    }

//get all courses of an students of all semester
    public function getLatestCourses()
    {
        $user_id = Request::input('user_id');
        $semester = Request::input('semester');
        $sem_1_selected_course = DB::table('semester_courses')
            ->select('semester_courses.*', 'courses.course')
            ->join('courses', 'semester_courses.course_id', '=', 'courses.id')
            ->where('semester_courses.user_id', '=', $user_id)
            ->where('semester_courses.semester', '=', $semester)
            ->orderBy('semester_courses.created_at', 'DESC')
            ->get();
        #BaseController::_setTrace($sem_1_selected_course);
        $sem_1_soft_skills = DB::table('soft_skills')
            ->select('soft_skills.*', 'sites.site_name')
            ->join('sites', 'soft_skills.site_id', '=', 'sites.id')
            ->where('soft_skills.user_id', '=', $user_id)
            ->where('soft_skills.semester', '=', $semester)
            ->orderBy('soft_skills.created_at', 'DESC')
            ->get();
        #BaseController::_setTrace($sem_1_soft_skills);

        $temp_array = array();
        $temp_array2 = array();
        $html = '';
        $html .= '<p><button class="btn btn-primary btn-sm js-back-course" type="button">Back to the Course</button></p>';
        $html .= '<table class="table table-bordered">';
            foreach ($sem_1_selected_course as $item) {
                if (!in_array($item->session, $temp_array)) {
                    array_push($temp_array, $item->session);

                    $html.='<tr><th colspan="3" class="text-center info">Session('.$item->session.')</th></tr>';
                    $html.='<tr><th>Course</th><th>Credit</th><th>Created</th></tr><tr><td>';
                    $html.= strlen($item->course) > 100 ? HelperActions::makeMultipleLineString($item->course, 80) : $item->course;
                    $html.='</td><td>'.$item->credit.'</td><td>'.$item->created_at.'</td></tr>';
                } else {
                    $html.='<tr><td>';
                    $html.=strlen($item->course) > 100 ? HelperActions::makeMultipleLineString($item->course, 80) : $item->course;
                    $html.='</td><td>'.$item->credit.'</td><td>'.$item->created_at.'</td></tr>';
                }
            }

        // for adding soft skills on all courses
        $html .= '<tr><td style="border: none;">&nbsp;</td></tr>';
            foreach ($sem_1_soft_skills as $item) {
                #BaseController::_setTrace($item);
                if (!in_array($item->session, $temp_array2)) {
                    array_push($temp_array2, $item->session);

                    $html.='<tr><th colspan="3" class="text-center info">Soft Skills : Session('.$item->session.')</th></tr>';
                    $html.='<tr><th>Soft Skill Name</th><th>Credit</th><th>Created</th></tr><tr><td>';
                    $html.= strlen($item->soft_skills_name) > 100 ? HelperActions::makeMultipleLineString($item->soft_skills_name, 80) : $item->soft_skills_name;
                    $html.='</td><td>'.$item->credit.'</td><td>'.$item->created_at.'</td></tr>';
                } else {
                    $html.='<tr><td>';
                    $html.=strlen($item->soft_skills_name) > 100 ? HelperActions::makeMultipleLineString($item->soft_skills_name, 80) : $item->soft_skills_name;
                    $html.='</td><td>'.$item->credit.'</td><td>'.$item->created_at.'</td></tr>';
                }
            }
        $html.='</table>';
        die(json_encode($html));
    }
}
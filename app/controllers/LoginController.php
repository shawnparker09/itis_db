<?php

class LoginController extends BaseController {

	
	// this is a vied to test. not will come in production
	public function showWelcome()	{
		return View::make('hello');
	}

	// make the login form view
	public function showLogin()	{
		// show the form
		return View::make('layouts.login');
	}

	// process the user input from login from and process the actual login
	public function doLogin()	{
				// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
			
		} else {

			// create our user data for the authentication
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);
			// attempt to do the login
			if (Auth::attempt($userdata) && User::isDeletedUser(Input::get('email'))) {
				Login::createSession();
				Logs::saveLog("users", "LogIn", Auth::id(), "", 0,0,0);
				return Redirect::action('CourseController@index');
			} else {	 	
				return View::make('layouts.login');
			}

		}
	}

	// this is for logout
	public function doLogout() {
		Logs::saveLog("users", "LogOut", Auth::id(), "", 0, 0,0);
		return Login::logOut();
	}
}

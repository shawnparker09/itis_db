@extends('layouts.main')

@section('content')
@include('layouts.navBar')

  <div class = "container">
      <div class = "raw">
        <h3> <center>Update Course</center> </h3>
        <hr/>
        {{ Form::open(array('url' => 'updatecourse', 'class' => 'form-horizontal')) }}
          @foreach ($course as $c)
          
          {{ Form::hidden('id', ($c->id)) }} 

          <div class="form-group ">
            <label for="" class = "col-sm-3 control-label">Course</label>
            <div class="col-sm-8">
              {{ Form::text('course', ($c->course), array('placeholder' => '', 'class' =>'form-control ')) }}
               @if ($errors->has('course')) <p class="text-danger"><b>{{ $errors->first('course') }}</b></p> @endif          
            </div>
          </div>

          <div class="form-group">
            <label for="" class = "col-sm-3 control-label">Module</label>
            <div class="col-sm-8">
             
              {{ Form::select('module', Module::makeModuleArray(), $c->module_id, array('class' =>'form-control ')) }}
              @if ($errors->has('module')) <p class="text-danger"><b>{{ $errors->first('module') }}</b></p> @endif
            </div>
          </div>

          @if(Session::get(Auth::id()) <= 2)
            <div class="form-group">
              <label for="" class = "col-sm-3 control-label">Credits</label>
              <div class="col-sm-8">
                {{ Form::text('credits', ($c->credits), array('placeholder' => '', 'class' =>'form-control ')) }}
                @if ($errors->has('credits')) <p class="text-danger"><b>{{ $errors->first('credits') }}</b></p> @endif
              </div>
            </div>
          @endif

          <div class="form-group">
            <label for="" class = "col-sm-3 control-label">Semester</label>
            <div class="col-sm-8">
             {{ Form::select('semester', array(null => 'Year Semster', 
                                              'SoSe 2012' => '2012   SS', 
                                              'WiSe 2012/2013' => '2012 WS', 
                                              'SoSe 2013' => '2013   SS', 
                                              'WiSe 2013/2014' => '2013 WS', 
                                              'SoSe 2014' => '2014   SS', 
                                              'WiSe 2014/2015' => '2014 WS', 
                                              'SoSe 2015' => '2015   SS', 
                                              'WiSe 2015/2016' => '2015 WS', 
                                              'SoSe 2016' => '2016   SS', 
                                              'WiSe 2016/2017' => '2016 WS', 
                                              'SoSe 2017' => '2017   SS', 
                                              'WiSe 2017/2018' => '2017 WS', 
                                              'SoSe 2018' => '2018   SS', 
                                              'WiSe 2018/2019' => '2018 WS'),
                                                 $c->semester, array('class' =>'form-control ')) }}
              @if ($errors->has('semester')) <p class="text-danger"><b>{{ $errors->first('semester') }}</b></p> @endif                                             
            </div> 
            
          </div>

          <div class="form-group">
            <label for="" class = "col-sm-3 control-label">Start Data</label>
            <div class="col-sm-8">
              {{ Form::text('startDate', ($c->startdate), array('placeholder' => '', 'class' =>'form-control ')) }}
              @if ($errors->has('startDate')) <p class="text-danger"><b>{{ $errors->first('startDate') }}</b></p> @endif
            </div>
          </div>
          
          <div class="form-group">
            <label for="" class = "col-sm-3 control-label">Mode</label>
            <div class="col-sm-8">
              {{ Form::text('mode', ($c->mode), array('placeholder' => '', 'class' =>'form-control ')) }}
              @if ($errors->has('mode')) <p class="text-danger"><b>{{ $errors->first('mode') }}</b></p> @endif
            </div>
          </div>

          <div class="form-group">
            <label for="" class = "col-sm-3 control-label">Course Avilable on </label>
            {{--*/ $icon = 'twiter_bootstrap/icon/' /*--}}

            <div class="col-sm-3">
              <span class="checkbox">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                {{ Form::checkbox('video_recording', (1), $c->video_recording) }}
                {{ HTML::image($icon.'video_recording.png') }} &nbsp; Video recording
              </span>
              <span class="checkbox">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                {{ Form::checkbox('lecture_hall_transmission', (1), $c->lecture_hall_transmission) }}
                {{ HTML::image($icon.'lecture_hall_transmission.png') }} &nbsp; Lecture hall transmission
              </span>
              <span class="checkbox">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                {{ Form::checkbox('live_stream', (1), $c->live_stream) }}
                {{ HTML::image($icon.'live_stream.png') }} &nbsp; Live stream
              </span>
            </div>

            <div  class="col-sm-3">
              <span class="checkbox">
                {{ Form::checkbox('interactive_live_stream', (1), $c->interactive_live_stream) }}
                {{ HTML::image($icon.'interactive_live_stream.png') }} &nbsp; Interactive live stream
              </span>
              <span class="checkbox">
                {{ Form::checkbox('attendance_required', (1), $c->attendance_required) }}
              {{ HTML::image($icon.'attendance_required.png') }} &nbsp; Attendance required
              </span>
              <span class="checkbox">
                {{ Form::checkbox('partial_attendance_required', (1), $c->partial_attendance_required) }}
                {{ HTML::image($icon.'partial_attendance_required.png') }} &nbsp; Partial attendance required
              </span>
            </div>
          </div>          

          <div class="form-group">
            <label for="" class = "col-sm-3 control-label">Site</label>
            <div class="col-sm-8">

               {{ Form::select('site', Site::makeSiteArray(), $c->site, array('class' =>'form-control')) }}
              @if ($errors->has('site')) <p class="text-danger"><b>{{ $errors->first('site') }}</b></p> @endif                      
            </div>
          </div> 

          <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Lecturer</label>
          <div class="col-sm-8">
          {{ Form::text('lecturer', ($c->lecturer), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('lecturer')) <p class="text-danger"><b>{{ $errors->first('lecturer') }}</b></p> @endif
          </div>
        </div>
      
        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Lecturer URL</label>
          <div class="col-sm-8">
          {{ Form::text('lecturer_url', ($c->lecturer_url), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('lecturer_url')) <p class="text-danger"><b>{{ $errors->first('lecturer_url') }}</b></p> @endif
          </div>
        </div>

        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Time</label>
          <div class="col-sm-8">
          {{ Form::text('time', ($c->time), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('time')) <p class="text-danger"><b>{{ $errors->first('time') }}</b></p> @endif
          </div>
        </div>

        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Room</label>
          <div class="col-sm-8">
          {{ Form::text('room', ($c->room), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('room')) <p class="text-danger"><b>{{ $errors->first('room') }}</b></p> @endif
          </div>
        </div>

<!--         <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Partial Compact Course</label>
          <div class="col-sm-8">
            {{ Form::select('partial_compact_course', array(
                                  '' => '', 
                                  'Yes' => 'Yes',
                                  'No' => 'No'
                                  ), 
                     ($c->partial_compact_course), array('class' =>'form-control ')) }}
             @if ($errors->has('partial_compact_course')) <p class="text-danger"><b>{{ $errors->first('partial_compact_course') }}</b></p> @endif
          </div>
        </div>  -->

<!--          <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Has Video Recording</label>
          <div class="col-sm-8">
            {{ Form::select('has_video_recording', array(
                                  '' => '', 
                                  'Yes' => 'Yes',
                                  'No' => 'No'
                                  ), 
                     ($c->has_video_recording), array('class' =>'form-control ')) }}
             @if ($errors->has('has_video_recording')) <p class="text-danger"><b>{{ $errors->first('has_video_recording') }}</b></p> @endif
          </div>
        </div>   -->

        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Video Recording Download URL</label>
          <div class="col-sm-8">
          {{ Form::text('video_recording_download_url', ($c->video_recording_download_url), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('video_recording_download_url')) <p class="text-danger"><b>{{ $errors->first('video_recording_download_url') }}</b></p> @endif
          </div>
        </div>

<!--         <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Live Stream Offered</label>
          <div class="col-sm-8">
            {{ Form::select('live_stream_offered', array(
                                  '' => '', 
                                  'Yes' => 'Yes',
                                  'No' => 'No'
                                  ), 
                     ($c->live_stream_offered), array('class' =>'form-control ')) }}
             @if ($errors->has('live_stream_offered')) <p class="text-danger"><b>{{ $errors->first('live_stream_offered') }}</b></p> @endif
          </div>
        </div>   -->

        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Live Web Feed URL</label>
          <div class="col-sm-8">
          {{ Form::text('live_web_feed_url', ($c->live_web_feed_url), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('live_web_feed_url')) <p class="text-danger"><b>{{ $errors->first('live_web_feed_url') }}</b></p> @endif
          </div>
        </div>

   <!--      <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Interactive Live Stream</label>
          <div class="col-sm-8">
          {{ Form::text('interactive_live_stream', ($c->interactive_live_stream), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('interactive_live_stream')) <p class="text-danger"><b>{{ $errors->first('interactive_live_stream') }}</b></p> @endif
          </div>
        </div> -->

<!--         <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Transmission Possible</label>
          <div class="col-sm-8">
            {{ Form::select('transmission_is_possible', array(
                                  '' => '', 
                                  'Yes' => 'Yes',
                                  'No' => 'No'
                                  ), 
                     ($c->transmission_is_possible), array('class' =>'form-control ')) }}
             @if ($errors->has('transmission_is_possible')) <p class="text-danger"><b>{{ $errors->first('transmission_is_possible') }}</b></p> @endif
          </div>
        </div>  -->


        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Transmission Room</label>
          <div class="col-sm-8">
          {{ Form::text('transmission_room', ($c->transmission_room), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('transmission_room')) <p class="text-danger"><b>{{ $errors->first('transmission_room') }}</b></p> @endif
          </div>
        </div>  

        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Assistant</label>
          <div class="col-sm-8">
          {{ Form::text('assistant', ($c->assistant), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('assistant')) <p class="text-danger"><b>{{ $errors->first('assistant') }}</b></p> @endif
          </div>
        </div> 

         <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Comments</label>
          <div class="col-sm-8">
          {{ Form::text('comment', ($c->comment), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('comment')) <p class="text-danger"><b>{{ $errors->first('comment') }}</b></p> @endif
          </div>
        </div>     

          <button type="submit" class="btn btn-primary pull-right" style = "margin-left: 11px;margin-right: 100px;margin-bottom: 11px;">
            <span class="glyphicon glyphicon-cloud-upload"></span>
            Update Course
          </button>

          @endforeach
        {{ Form::close() }}

        @if($c->visible == 1)
          @if(Course::checkCurrentSemesterCourse($c->id)!=1 && Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
            {{--*/ $durl = 'deletecourse/'.$c->id.'/'.$c->course /*--}}
            {{ Form::open(array('url' => $durl)) }}
              <button type="submit" class="btn btn-default pull-right">
                <span class="glyphicon glyphicon-remove"></span>
                 Delete Course
              </button>
            {{ Form::close() }}
          @elseif(Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
            <button type="submit" class="btn btn-warning disabled pull-right">
              <span class="glyphicon glyphicon-remove"></span>
               Sorry, Can not delete current semester course
            </button>
          @endif 
        @elseif($c->visible == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
            {{--*/ $udurl = 'undodeleteedcourse/'.$c->id.'/'.$c->course /*--}}
            {{ Form::open(array('url' => $udurl)) }}
            <button type="submit" class="btn btn-default pull-right">
              <span class="glyphicon glyphicon-repeat"></span>
               Undo Delete Course 
            </button>
            {{ Form::close() }}
        @endif 

      </div>
  </div>
  @stop
@extends('layouts.main')

@section('content')
@include('layouts.navBar')

<div class = "container">
  
  <div class = "raw">
    <h3> <center>Create New Course</center> </h3>
    <hr/>
    {{ Form::open(array('url' => 'createcourse', 'method' => 'post', 'class' => 'form-horizontal')) }}
      
      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">Course</label>
        <div class="col-sm-8">
          {{ Form::text('course', Input::old('course'), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('course')) <p class="text-danger"><b>{{ $errors->first('course') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Module</label>
        <div class="col-sm-8">
          <!-- {{ Form::text('module', Input::old('module'), array('placeholder' => '', 'class' =>'form-control ')) }} -->
             {{ Form::select('module', Module::makeModuleArray(),
                              null, array('class' =>'form-control input-sm')) }}
             @if ($errors->has('module')) <p class="text-danger"><b>{{ $errors->first('module') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group">
         <label for="" class = "control-label col-sm-3">Course only for one semester </label>
         &nbsp;&nbsp;&nbsp; 
         <input type="radio" name="toggler" checked="checked" value="1"/> Only one semester &nbsp;&nbsp;
         <input type="radio" name="toggler" value="2" /> Multiple semester 
      </div>

      <div id="blk-2" class="form-group toHide" style="display: none;">
        <label for="" class = "col-sm-3 control-label"></label>
        <div class="well col-sm-8">
          <center>
            <p>Course Start : {{ Form::select('semesterStart', Course::generateListOfSemester(date("Y/m/d")),
                Input::old('semester'), array('class' =>'')) }}
            Course End : {{ Form::select('semesterEnd', Course::generateListOfSemester(date("Y/m/d"), 20),
                Input::old('semester'), array('class' =>'')) }}
          </center>
          <center>
            Course for : &nbsp;
           <input type="radio" name="semesterForCourse" value="1"/> Only Winter
           <input type="radio" name="semesterForCourse" value="2" /> Only Summer 
           <input type="radio" name="semesterForCourse" checked="checked" value="3" /> Both Semester
          </center>
        </div>
      </div>

      <div id="blk-1" class="toHide">
        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Semester</label>
          <div class="col-sm-8">
             {{ Form::select('semester', Course::generateListOfSemester(date("Y/m/d")),
                Input::old('semester'), array('class' =>'form-control ')) }}
               @if ($errors->has('semester')) <p class="text-danger"><b>{{ $errors->first('semester') }}</b></p> @endif
          </div>
        </div>

        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Start Date</label>
          <div class="col-sm-8">
          {{ Form::text('startDate', Input::old('startDate'), array('placeholder' => 'mm/dd/yyyy', 'class' =>'form-control ')) }}
          @if ($errors->has('startDate')) <p class="text-danger"><b>{{ $errors->first('startDate') }}</b></p> @endif
          </div>
        </div>
      </div>      

      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Mode</label>
        <div class="col-sm-8">
        {{ Form::text('mode', Input::old('mode'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('mode')) <p class="text-danger"><b>{{ $errors->first('mode') }}</b></p> @endif
        </div>
      </div>


      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Course Avilable on </label>
        {{--*/ $icon = 'twiter_bootstrap/icon/' /*--}}

        <div class="col-sm-3">
          <span class="checkbox">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            {{ Form::checkbox('video_recording', '1') }}
            {{ HTML::image($icon.'video_recording.png') }} &nbsp; Video recording
          </span>
          <span class="checkbox">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            {{ Form::checkbox('lecture_hall_transmission', '1') }}
            {{ HTML::image($icon.'lecture_hall_transmission.png') }} &nbsp; Lecture hall transmission
          </span>
          <span class="checkbox">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            {{ Form::checkbox('live_stream', '1') }}
            {{ HTML::image($icon.'live_stream.png') }} &nbsp; Live stream
          </span>
        </div>

        <div  class="col-sm-3">
          <span class="checkbox">
            {{ Form::checkbox('interactive_live_stream', '1') }}
            {{ HTML::image($icon.'interactive_live_stream.png') }} &nbsp; Interactive live stream
          </span>
          <span class="checkbox">
            {{ Form::checkbox('attendance_required', '1') }}
          {{ HTML::image($icon.'attendance_required.png') }} &nbsp; Attendance required
          </span>
          <span class="checkbox">
            {{ Form::checkbox('partial_attendance_required', '1') }}
            {{ HTML::image($icon.'partial_attendance_required.png') }} &nbsp; Partial attendance required
          </span>
        </div>
      </div>

      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Credits</label>
        <div class="col-sm-8">
        {{ Form::text('credits', Input::old('credits'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('credits')) <p class="text-danger"><b>{{ $errors->first('credits') }}</b></p> @endif
        </div>
      </div>
      
      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Site</label>
        <div class="col-sm-8">
             {{ Form::select('site', Site::makeSiteArray(),
                              null, array('class' =>'form-control input-sm')) }}
             @if ($errors->has('site')) <p class="text-danger"><b>{{ $errors->first('site') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Lecturer</label>
        <div class="col-sm-8">
        {{ Form::text('lecturer', Input::old('lecturer'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('lecturer')) <p class="text-danger"><b>{{ $errors->first('lecturer') }}</b></p> @endif
        </div>
      </div>
    
      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Lecturer URL</label>
        <div class="col-sm-8">
        {{ Form::text('lecturer_url', Input::old('lecturer_url'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('lecturer_url')) <p class="text-danger"><b>{{ $errors->first('lecturer_url') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Time</label>
        <div class="col-sm-8">
        {{ Form::text('time', Input::old('time'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('time')) <p class="text-danger"><b>{{ $errors->first('time') }}</b></p> @endif
        </div>
      </div>
    

      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Room</label>
        <div class="col-sm-8">
        {{ Form::text('room', Input::old('room'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('room')) <p class="text-danger"><b>{{ $errors->first('room') }}</b></p> @endif
        </div>
      </div>


<!--       <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Partial Compact Course</label>
        <div class="col-sm-8">
          {{ Form::select('partial_compact_course', array(
                                '' => '', 
                                'Yes' => 'Yes',
                                'No' => 'No'
                                ), 
                   Input::old('partial_compact_course'), array('class' =>'form-control ')) }}
           @if ($errors->has('partial_compact_course')) <p class="text-danger"><b>{{ $errors->first('partial_compact_course') }}</b></p> @endif
        </div>
      </div>  -->

<!--        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Has Video Recording</label>
        <div class="col-sm-8">
          {{ Form::select('has_video_recording', array(
                                '' => '', 
                                'Yes' => 'Yes',
                                'No' => 'No'
                                ), 
                   Input::old('has_video_recording'), array('class' =>'form-control ')) }}
           @if ($errors->has('has_video_recording')) <p class="text-danger"><b>{{ $errors->first('has_video_recording') }}</b></p> @endif
        </div>
      </div>  --> 

      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Video Recording Download URL</label>
        <div class="col-sm-8">
        {{ Form::text('video_recording_download_url', Input::old('video_recording_download_url'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('video_recording_download_url')) <p class="text-danger"><b>{{ $errors->first('video_recording_download_url') }}</b></p> @endif
        </div>
      </div>

<!--       <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Live Stream Offered</label>
        <div class="col-sm-8">
          {{ Form::select('live_stream_offered', array(
                                '' => '', 
                                'Yes' => 'Yes',
                                'No' => 'No'
                                ), 
                   Input::old('live_stream_offered'), array('class' =>'form-control ')) }}
           @if ($errors->has('live_stream_offered')) <p class="text-danger"><b>{{ $errors->first('live_stream_offered') }}</b></p> @endif
        </div>
      </div>   -->

      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Live Web Feed URL</label>
        <div class="col-sm-8">
        {{ Form::text('live_web_feed_url', Input::old('live_web_feed_url'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('live_web_feed_url')) <p class="text-danger"><b>{{ $errors->first('live_web_feed_url') }}</b></p> @endif
        </div>
      </div>

   <!--    <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Interactive Live Stream</label>
        <div class="col-sm-8">
        {{ Form::text('interactive_live_stream', Input::old('interactive_live_stream'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('interactive_live_stream')) <p class="text-danger"><b>{{ $errors->first('interactive_live_stream') }}</b></p> @endif
        </div>
      </div> -->

<!--       <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Transmission Possible</label>
        <div class="col-sm-8">
          {{ Form::select('transmission_is_possible', array(
                                '' => '', 
                                'Yes' => 'Yes',
                                'No' => 'No'
                                ), 
                   Input::old('transmission_is_possible'), array('class' =>'form-control ')) }}
           @if ($errors->has('transmission_is_possible')) <p class="text-danger"><b>{{ $errors->first('transmission_is_possible') }}</b></p> @endif
        </div>
      </div>  -->
     
       <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Transmission Room</label>
        <div class="col-sm-8">
        {{ Form::text('transmission_room', Input::old('transmission_room'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('transmission_room')) <p class="text-danger"><b>{{ $errors->first('transmission_room') }}</b></p> @endif
        </div>
      </div>       

      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Assistant </label>
        <div class="col-sm-8">
        {{ Form::text('assistant', Input::old('assistant'), array('placeholder' => '', 'class' =>'form-control ')) }}
        @if ($errors->has('assistant')) <p class="text-danger"><b>{{ $errors->first('assistant') }}</b></p> @endif
        </div>
      </div>

       <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Comments</label>
        <div class="col-sm-8">
        {{ Form::textarea('comment', Input::old('comment'), array('placeholder' => '', 'class' =>'form-control ','size' => '30x4')) }}
        @if ($errors->has('comment')) <p class="text-danger"><b>{{ $errors->first('comment') }}</b></p> @endif
        </div>
      </div>
   
        <button type="submit" class="btn btn-primary pull-right" style = "margin-left: 11px;margin-right: 100px;margin-bottom: 11px;">
          <span class="glyphicon glyphicon-ok"></span>
          Create New Course
        </button>

        <button type="reset" action = "" class="btn btn-warning pull-right">
          <span class="glyphicon glyphicon-refresh"></span>
          Reset
        </button>
    {{ Form::close() }}
  </div>
</div>
@stop
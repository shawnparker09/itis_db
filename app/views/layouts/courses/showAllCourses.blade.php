@extends('layouts.main')

@section('content')
@include('layouts.navBar')

{{--*/ $msg = HelperActions::getMessageToSession(); /*--}}
@if($msg)
	<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success" style="background-color: #AEDB43;padding: 0;margin-top: -20;">
		<p>
			<p> <b><center>
        <span class="glyphicon glyphicon-ok"></span>
        Success
			</center></b> </p>
			<p><center>{{ $msg }}</center></p>
		</p>
	</div>
@endif 			 
<div class = "container">
	<div class="raw">
		<div class="col-sm-12 col-md-4 col-lg-4">
		{{ Form::open(array('url' => 'showallcourses', 'method' => 'post')) }}
			<div class="input-group">
			  <span class="input-group-btn">
			    <button class="btn btn-default disabled btn-sm"> <b>{{ trans('localization.Courses of ITIS') }}</b> </button>
			      </span>		    	
			      {{ Form::text('searchInput', null, array('placeholder' => '', 'class' =>'form-control input-sm')) }}
			      <span class="input-group-btn">
				    <button class="btn btn-default btn-sm" type="submit">
			   		<span class="glyphicon glyphicon-search"> </span>&nbsp;
			    </button>
			  </span>	
			</div>
		{{ Form::close() }}
		</div>

		<div class="col-sm-12 col-md-3 col-lg-3">
		{{ Form::open(array('url' => 'sortbyreq', 'method' => 'post', 'class' => 'form-inline')) }}
				<div class="form-group">
					{{ Form::select('semester', Course::generateListOfSemesterWithSemesterString(date("Y/m/d")),
	                         null, array('class' =>'form-control input-sm')) }}
			  </div>
			  <button type="submit" class="btn btn-default btn-sm">
			  	<span class = "glyphicon glyphicon-refresh"></span>&nbsp;
			  </button>		  
			{{ Form::close() }}
		</div>

		@if(Session::get(Auth::id()) != Roole::getRoleId('Student')&&Session::get(Auth::id()) != Roole::getRoleId('Candidate'))		
		<div class="col-sm-12 col-md-2 col-lg-2">
				{{ Form::open(array('url' => 'exportcourse', 'method' => 'get', 'class' => 'form-inline')) }}
          <div class="form-group">
            {{ Form::select('exportType', array(
                                              ''  => 'Export',
                                              'xml' => 'XML',
                                              'csv' => 'CSV',
                                              'pdf' => 'PDF',
                                          ), 
                             null, array('class' =>'form-control input-sm')) }}
          </div>
          <button type="submit" class="btn btn-default btn-sm">
            <span class = "glyphicon glyphicon-cloud-download"></span>&nbsp;
          </button>     
				{{ Form::close() }}
		</div>
		@endif

		<div class="col-sm-12 col-md-1 col-lg-1">
			@if(Session::get(Auth::id()) == Roole::getRoleId('Studiendekan'))
				{{ Form::open(array('url' => 'import', 'method' => 'get')) }}
               <button type="submit" class="btn btn-default btn-sm">
                 <span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;
                   {{ trans('localization.Import') }}
               </button>
				{{ Form::close() }}
      @endif
		</div>

		<div class="col-sm-12 col-md-2 col-lg-2">
		   @if(Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
					{{ Form::open(array('url' => 'createcourse', 'method' => 'get')) }}
           <button type="submit" class="btn btn-default btn-sm">
           <span class="glyphicon glyphicon-plus"></span>
             {{ trans('localization.Add New Course') }}
          </button>
					{{ Form::close() }}
      @endif
		</div>
	</div>
	@if (HelperActions::getViewMode())
     <div id="table-responsive">
            
        <table class="table table-bordered table-striped">
		
			@foreach ($courses as $course)
			@if (($course->visible) == 1)
				{{--*/ $strikecls = ""; /*--}}
			@else
				{{--*/ $strikecls = "strike-text"; /*--}}
			@endif
			@if ((($course->visible) == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Admin')) || ($course->visible == 1))
				<table class="table table-bordered table-striped">
						<tr>
							<td class="col-md-2 col-lg-2 {{Module::moduleStyleClassById($course->module_id)}}" data-title="Module"> {{trans('localization.Module')}} </td>
							<td class="col-md-10 col-lg-10 {{Module::moduleStyleClassById($course->module_id)}} {{$strikecls}}"> {{ strlen($course->module) > 400 ? HelperActions::makeMultipleLineString($course->module, 400) : $course->module }} </td>
						</tr>
						<tr>
							<td data-title="Courses"> {{trans('localization.Course')}} </td>
							<td class="{{$strikecls}}">{{ strlen($course->course) > 400 ? HelperActions::makeMultipleLineString($course->course, 400) : $course->course }}</td>
						</tr>
						<tr>
							<td data-title="Semester">{{trans('localization.Semester')}}</td>
							<td class="{{$strikecls}}">{{ $course['semester'] }}</td>
						</tr>
						<tr>
							<td data-title="Type">{{trans('localization.Mode')}}</td>
							<td class="{{$strikecls}}">{{ $course['mode'] }}</td>
						</tr>
						<tr>
							<td data-title="Start Date">{{trans('localization.Start Date')}}</td>
							<td class="{{$strikecls}}">{{ $course['startdate'] }}</td>
						</tr>
						<tr>
							<td data-title="Credits">{{trans('localization.Credits')}}</td>
							<td class="{{$strikecls}}">{{ $course['credits'] }}</td>
						</tr>		
						<tr>
							<td data-title="Credits">Site</td>
							<td class="{{$strikecls}}">{{ Site::getSiteName($course->site)}}</td>
						</tr>	
						<tr>
							<td data-title="Module Exam id">{{trans('localization.Module Id/Exam Id')}}</td>
							<td class="{{$strikecls}}">{{ $course['moduleexamid'] }}</td>
						</tr>
						<tr>
							<td data-title="Created At">{{trans('localization.Created At')}}</td>
							<td class="{{$strikecls}}">{{ $course['created_at'] }}</td>
						</tr>
						<tr>
							<td data-title="Last Modify At">{{trans('localization.Last Modify At')}}</td>
							<td class="{{$strikecls}}">{{ $course['updated_at'] }}</td>
						</tr>		
						<tr>
							<td data-title="Lecturer">{{trans('localization.Lecturer')}}</td>
							<td class="{{$strikecls}}">{{ $course['lecturer'] }}</td>
						</tr>	
						<tr>
							<td data-title="Lecture Website">Lecture Website</td>
							<td class="{{$strikecls}}">{{ $course['lecturer_url'] }}</td>
						</tr>						
						<tr>
							<td data-title="Time">Time</td>
							<td class="{{$strikecls}}">{{ $course['time'] }}</td>
						</tr>

						<tr>
							<td data-title="Room">Room</td>
							<td class="{{$strikecls}}">{{ $course['room'] }}</td>
						</tr>
				</table>
				<table class="table table-bordered table-striped">
					<tr>
							<td class="col-md-1 col-lg-1">
								@if(Session::get(Auth::id()) <= Roole::getRoleId('Assistant'))
										{{--*/ $edurl = 'editcourses/'.$course->id.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
										{{ Form::open(array('url' => $edurl, 'method' => 'post')) }}
										  <button type="submit" class="btn btn-default btn-xs">
										  	<span class="glyphicon glyphicon-edit"></span>
										  	Edit
										  </button>
										{{ Form::close() }}
								@endif
							</td>
							<td class="col-md-1 col-lg-1">
								@if(Session::get(Auth::id()) <= Roole::getRoleId('Coordinator'))
									{{--*/ $logsurl = 'historylogs/'.$course->id.'/courses/'.$course->course /*--}}
									{{ Form::open(array('url' => $logsurl, 'method' => 'post')) }}									
									  <button type="submit" class="btn btn-default btn-xs">
									  	<span class="glyphicon glyphicon-time"></span>
									  	Logs
									  </button>
									{{ Form::close() }}
								@endif
							</td>
							<td class="col-md-1 col-lg-1">
								@if($course->visible == 1 && Course::checkCurrentSemesterCourse($course->id)!=1 && Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
										{{--*/ $deleteUrl = 'deletecourse/'.$course->id.'/'.$course->course /*--}}
										{{ Form::open(array('url' => $deleteUrl, 'method' => 'post')) }}                  
                     <button type="submit" class="btn btn-default  btn-xs">
                       <span class="glyphicon glyphicon-remove"></span>
                        Delete
                     </button>
										{{ Form::close() }}
                @elseif($course->visible == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
										{{--*/ $undoDeleteUrl = 'undodeleteedcourse/'.$course->id.'/'.$course->course /*--}}
										{{ Form::open(array('url' => $undoDeleteUrl, 'method' => 'post')) }}                    
                     <button type="submit" class="btn btn-default  btn-xs">
                       <span class="glyphicon glyphicon-repeat"></span>
                        Undo Delete 
                     </button>
										{{ Form::close() }}
                 @endif
							</td>
							
						</tr>
				</table>
			@endif
			<br/>
			@endforeach
		@else
	    <div id="table-responsive">
            
        <table class="col-md-12 col-lg-12 table table-hover cf">
                <thead class="cf">

				<tr>
					<td><b>{{ trans('localization.Modules') }}</b></td>
					<td><b>{{ trans('localization.Exam Id') }}</b></td>
					<td><b>{{ trans('localization.Courses') }}</b></td>
					<td><b>{{ trans('localization.Credits') }}</b></td>
					<td><b>{{ trans('localization.Site') }}</b></td>
					<td><b>{{ trans('localization.Semester') }}</b></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>	
				</tr>


			    </thead>
			    <tbody>
			
				@foreach ($courses as $course)
					@if (($course->visible) == 1)
						{{--*/ $strikecls = ""; /*--}}
					@else
						{{--*/ $strikecls = "strike-text"; /*--}}
					@endif
					@if ((($course->visible) == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Admin')) || ($course->visible == 1))
						<tr class = '{{$strikecls}} {{Module::moduleStyleClassById($course->module_id)}}'>
								<td data-title="Module">
									{{ strlen($course->module) > 40 ? HelperActions::makeMultipleLineString($course->module, 25) : $course->module }}
								</td>
								<td data-title="Exam ID">
									{{ Course::getExamID($course->module_id)}}
								</td>
								<td data-title="Courses">
									{{ strlen($course->course) > 40 ? HelperActions::makeMultipleLineString($course->course, 25) : $course->course }}
								</td>
								<td data-title="Credits">
									{{ $course->credits }}
								</td>
								<td data-title="Site" >
									{{ Site::getSiteName($course->site)}}
								</td>
								<td data-title="Semester" >
									{{ $course->semester }}
								</td>
							
								@if(Session::get(Auth::id()) <= Roole::getRoleId('Assistant'))
									<td>
										{{--*/ $edurl = 'editcourses/'.$course->id.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
										{{ Form::open(array('url' => $edurl, 'method' => 'post')) }}
										  <button type="submit" class="btn btn-default btn-xs">
										  	<span class="glyphicon glyphicon-edit"></span>
										  	Edit
										  </button>
										{{ Form::close() }}
									</td>
								@endif

								@if(Session::get(Auth::id()) <= Roole::getRoleId('Coordinator'))
									<td>
										{{--*/ $logsurl = 'historylogs/'.$course->id.'/courses/'.$course->course /*--}}
										{{ Form::open(array('url' => $logsurl, 'method' => 'post')) }}									
										  <button type="submit" class="btn btn-default btn-xs">
										  	<span class="glyphicon glyphicon-time"></span>
										  	Logs
										  </button>
										{{ Form::close() }}
									</td>
								@endif

								@if($course->visible == 1 && Course::checkCurrentSemesterCourse($course->id)!=1 && Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
                  <td>
										{{--*/ $deleteUrl = 'deletecourse/'.$course->id.'/'.$course->course /*--}}
										{{ Form::open(array('url' => $deleteUrl, 'method' => 'post')) }}                  
                     <button type="submit" class="btn btn-default  btn-xs pull-right">
                       <span class="glyphicon glyphicon-remove"></span>
                        Delete
                     </button>
										{{ Form::close() }}
                  </td>
                @elseif($course->visible == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
                  <td>
										{{--*/ $undoDeleteUrl = 'undodeleteedcourse/'.$course->id.'/'.$course->course /*--}}
										{{ Form::open(array('url' => $undoDeleteUrl, 'method' => 'post')) }}                    
                     <button type="submit" class="btn btn-default  btn-xs pull-right">
                       <span class="glyphicon glyphicon-repeat"></span>
                        Undo Delete 
                     </button>
										{{ Form::close() }}
                  </td>

                  @elseif($course->visible == 1 && Course::checkCurrentSemesterCourse($course->id)==1 && Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
                  <td>
			                   
                     <button type="submit" class="btn btn-disabled  btn-xs pull-right">
                       <span class="glyphicon glyphicon-remove"></span>
                       Delete 
                     </button>
								
                  </td>
                  
                 @endif
                 
                 {{--*/ $icon = 'twiter_bootstrap/icon/' /*--}}
                 @if($course->video_recording== 1)
                 <td>
                 	{{ HTML::image($icon.'video_recording.png') }}
                 </td>
                 @endif

                 @if($course->lecture_hall_transmission== 1)
                 <td>
                 	{{ HTML::image($icon.'lecture_hall_transmission.png') }}
                 </td> 
                 @endif

                 @if($course->live_stream== 1)
                 <td>
                 	{{ HTML::image($icon.'live_stream.png') }}
                 </td> 
                 @endif

                 @if($course->interactive_live_stream== 1)
                 <td>
                 	{{ HTML::image($icon.'interactive_live_stream.png') }}
                 </td> 
                 @endif

                 @if($course->attendance_required== 1)
                 <td>
                 	{{ HTML::image($icon.'attendance_required.png') }}
                 </td>
                 @elseif($course->partial_attendance_required== 1) 
                  <td>
                 	{{ HTML::image($icon.'partial_attendance_required.png') }}
                 </td> 
                 @else
                 <td>
                 	{{ HTML::image($icon.'attendance_off.png') }}
                 </td> 
                 @endif

                 @if($course->video_recording== 0)
                 <td>
                 	{{ HTML::image($icon.'video_off.png') }}
                 </td>
                 @endif

                 @if($course->lecture_hall_transmission== 0)
                 <td>
                 	{{ HTML::image($icon.'no_lecture_hall_transmission.jpg') }}
                 </td> 
                 @endif

                 @if($course->live_stream== 0)
                 <td>
                 	{{ HTML::image($icon.'stream_off.png') }}
                 </td> 
                 @endif

                 @if($course->interactive_live_stream== 0)
                 <td>
                 	{{ HTML::image($icon.'no_interactive_live_stream.jpg') }}
                 </td> 
                 @endif

               
						</tr>
					@endif
				@endforeach
			
			</tbody>
		</table>
		@endif
	
	</div>
	<br/>
	<div class="raw">
		<?php echo $courses->links(); ?>
	</div>
</div>
@stop
@extends('layouts.main')

@section('content')
@include('layouts.navBar')
  <div class = "container">
    <div class = "raw">
      <h3> <center>Update Module</center></h3>
      <hr/>
      
      {{ Form::open(array('url' => 'updatemodule', 'class' => 'form-horizontal')) }}

        @foreach ($module as $m)
        
        {{ Form::hidden('id', ($m->id)) }} 

        <div class="form-group ">
          <label for="" class = "col-sm-3 control-label">Module</label>
          <div class="col-sm-8">
            {{ Form::text('module', ($m->module), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('module')) <p class="text-danger"><b>{{ $errors->first('module') }}</b></p> @endif
          </div>
        </div>

        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Module Id</label>
          <div class="col-sm-8">
            {{ Form::text('module_no', ($m->module_no), array('placeholder' => '', 'class' =>'form-control ')) }}
            @if ($errors->has('module_no')) <p class="text-danger"><b>{{ $errors->first('module_no') }}</b></p> @endif
          </div>
        </div>

        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Exam Id</label>
          <div class="col-sm-8">
            {{ Form::text('examId', ($m->examId), array('placeholder' => '', 'class' =>'form-control ')) }}
            @if ($errors->has('examId')) <p class="text-danger"><b>{{ $errors->first('examId') }}</b></p> @endif
          </div>
        </div>

        <div class="form-group">
          <label for="" class = "col-sm-3 control-label">Topic Category</label>
          <div class="col-sm-8">
            {{ Form::select('topic_category', Category::makeCategoryArray(), $m->topic_category, array('class' =>'form-control ')) }}
      
            @if ($errors->has('topic_category')) <p class="text-danger"><b>{{ $errors->first('topic_category') }}</b></p> @endif  
          </div> 
          
        </div>


        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Weekly Composition</label>
        <div class="col-sm-8">
          {{ Form::text('weekly_composition', ($m->weekly_composition), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('weekly_composition')) <p class="text-danger"><b>{{ $errors->first('weekly_composition') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Exam</label>
        <div class="col-sm-8">
          {{ Form::text('exam', ($m->exam), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('exam')) <p class="text-danger"><b>{{ $errors->first('exam') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Exam Duration</label>
        <div class="col-sm-8">
          {{ Form::text('exam_duration', ($m->exam_duration), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('exam_duration')) <p class="text-danger"><b>{{ $errors->first('exam_duration') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">ECTS Credits</label>
        <div class="col-sm-8">
          {{ Form::text('ECTS_credits', ($m->ECTS_credits), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('ECTS_credits')) <p class="text-danger"><b>{{ $errors->first('ECTS_credits') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Required Hours of Work</label>
        <div class="col-sm-8">
          {{ Form::text('required_hours_of_work', ($m->required_hours_of_work), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('required_hours_of_work')) <p class="text-danger"><b>{{ $errors->first('required_hours_of_work') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Required Work in Hours</label>
        <div class="col-sm-8">
          {{ Form::text('required_work_in_hours', ($m->required_work_in_hours), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('required_work_in_hours')) <p class="text-danger"><b>{{ $errors->first('required_work_in_hours') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Site</label>
        <div class="col-sm-8">
          {{ Form::select('site', Site::makeSiteArray(), $m->site, array('class' =>'form-control ')) }}
            @if ($errors->has('site')) <p class="text-danger"><b>{{ $errors->first('site') }}</b></p> @endif  
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Site_2</label>
        <div class="col-sm-8">
        {{ Form::select('site_2', Site::makeSiteArray(), $m->site_2, array('class' =>'form-control ')) }}
            @if ($errors->has('site_2')) <p class="text-danger"><b>{{ $errors->first('site_2') }}</b></p> @endif  
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Person Responsible</label>
        <div class="col-sm-8">
          {{ Form::text('person_responsible', ($m->person_responsible), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('person_responsible')) <p class="text-danger"><b>{{ $errors->first('person_responsible') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Person Responsible_2</label>
        <div class="col-sm-8">
          {{ Form::text('person_responsible_2', ($m->person_responsible_2), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('person_responsible_2')) <p class="text-danger"><b>{{ $errors->first('person_responsible_2') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Semester</label>
        <div class="col-sm-8">
          {{ Form::select('semester', array(null => 'Year Semster', 
                                            'SoSe 2012' => '2012   SS', 
                                            'WiSe 2012/13' => '2012 WS', 
                                            'SoSe 2013' => '2013   SS', 
                                            'WiSe 2013/14' => '2013 WS', 
                                            'SoSe 2014' => '2014   SS', 
                                            'WiSe 2014/15' => '2014 WS', 
                                            'SoSe 2015' => '2015   SS', 
                                            'WiSe 2015/16' => '2015 WS', 
                                            'SoSe 2016' => '2016   SS', 
                                            'WiSe 2016/17' => '2016 WS', 
                                            'SoSe 2017' => '2017   SS', 
                                            'WiSe 2017/18' => '2017 WS', 
                                            'SoSe 2018' => '2018   SS', 
                                            'WiSe 2018/19' => '2018 WS'),
                                               $m->semester, array('class' =>'form-control ')) }}
             @if ($errors->has('semester')) <p class="text-danger"><b>{{ $errors->first('semester') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Teaching Methods</label>
        <div class="col-sm-8">
          {{ Form::text('teaching_methods', ($m->teaching_methods), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('teaching_methods')) <p class="text-danger"><b>{{ $errors->first('teaching_methods') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Module Description</label>
        <div class="col-sm-8">
          {{ Form::text('module_description',($m->module_description), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('module_description')) <p class="text-danger"><b>{{ $errors->first('module_description') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Miscellaneous Comments</label>
        <div class="col-sm-8">
          {{ Form::text('miscellaneous_Comments', ($m->miscellaneous_Comments), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('miscellaneous_Comments')) <p class="text-danger"><b>{{ $errors->first('miscellaneous_Comments') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Recommended Prerequisite Knowledge</label>
        <div class="col-sm-8">
          {{ Form::text('recommended_prerequisite_knowledge', ($m->recommended_prerequisite_knowledge), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('recommended_prerequisite_knowledge')) <p class="text-danger"><b>{{ $errors->first('recommended_prerequisite_knowledge') }}</b></p> @endif
        </div>
        </div>

        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Recommended Literature</label>
        <div class="col-sm-8">
          {{ Form::text('recommended_literature', ($m->recommended_literature), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('recommended_literature')) <p class="text-danger"><b>{{ $errors->first('recommended_literature') }}</b></p> @endif
        </div>
        </div>
        
        <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Outcomes</label>
        <div class="col-sm-8">
          {{ Form::text('outcomes', ($m->outcomes), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('outcomes')) <p class="text-danger"><b>{{ $errors->first('outcomes') }}</b></p> @endif
        </div>
        </div>


        <button type="submit" class="btn btn-primary pull-right" style = "margin-left: 11px;margin-right: 100px;margin-bottom: 11px;">
          <span class="glyphicon glyphicon-cloud-upload"></span>
          Update Module
        </button>

        @endforeach
      {{ Form::close() }}

      @if($m->visible == 1)
      {{--*/ $dmurl = 'deletemodule/'.$m->id.'/'.$m->module /*--}}
      {{ Form::open(array('url' => $dmurl)) }}
          <button type="submit" class="btn btn-warning pull-right">
            <span class="glyphicon glyphicon-remove"></span>
             Delete Module
          </button>
      {{ Form::close() }}
      
      @elseif($m->visible == 0)
      {{--*/ $udeurl = 'undodeleteedmodule/'.$m->id.'/'.$m->module /*--}}
      {{ Form::open(array('url' => $udeurl)) }}
          <button type="submit" class="btn btn-warning pull-right">
            <span class="glyphicon glyphicon-repeat"></span>
             Undo Delete Module 
          </button>
        {{ Form::close() }}
      @endif 

    </div>
  </div>
  @stop
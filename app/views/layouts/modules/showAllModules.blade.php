@extends('layouts.main')

@section('content')
@include('layouts.navBar')

{{--*/ $msg = HelperActions::getMessageToSession(); /*--}}
@if($msg)
	<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success" style="background-color: #AEDB43;padding: 0;margin-top: -20;">
		<p>
			<p> <b><center>
        <span class="glyphicon glyphicon-ok"></span>
				{{ trans('localization.Success') }}
			</center></b> </p>
			<p><center>{{ $msg }}</center></p>
		</p>
	</div>
@endif  
    <div class="container">
	<div class ="row">

		<div class="col-sm-12 col-md-3 col-lg-3">
				{{ Form::open(array('url' => 'showallmodules', 'method' => 'post', 'class' => 'form-inline')) }}
		    <div class="input-group">
		      <span class="input-group-btn">
		        <button class="btn btn-default disabled btn-sm"> <b>Modules of ITIS</b> </button>
		      </span>		    	
		      	{{ Form::text('searchInput', null, array('placeholder' => '', 'class' =>'form-control input-sm')) }}
		      <span class="input-group-btn">
		        <button class="btn btn-default btn-sm" type="submit">
		        	<span class="glyphicon glyphicon-search"> </span>&nbsp;
		        </button>
		      </span>	
		    </div>
		    {{ Form::close() }}
		</div>

		<div class="col-sm-12 col-md-4 col-lg-4">
			{{ Form::open(array('url' => 'sortbyreqmodule', 'method' => 'post', 'class' => 'form-inline')) }}
				<div class="form-group">
					{{ Form::select('categoryName', Category::makeCategoryArray(),
                          null, array('class' =>'form-control input-sm')) }}
			  </div>
			  <button type="submit" class="btn btn-default btn-sm">
			  	<span class = "glyphicon glyphicon-refresh"></span> &nbsp;
			  </button>		  
		  {{ Form::close() }}
		</div>

		<div class="col-sm-12 col-md-2 col-lg-2">
	   {{ Form::open(array('url' => 'exportmodule', 'method' => 'post', 'class' => 'form-inline')) }}
          <div class="form-group">
            {{ Form::select('exportType', array(
                                              ''  => 'Export',
                                              'xml' => 'XML',
                                              'csv' => 'CSV',
                                              'pdf' => 'PDF',
                                          ), 
                             null, array('class' =>'form-control input-sm')) }}
          </div>
          <button type="submit" class="btn btn-default btn-sm">
            <span class = "glyphicon glyphicon-cloud-download"></span>&nbsp;
          </button>     
		  {{ Form::close() }}
		</div>

	   <div class="col-sm-12 col-md-1 col-lg-1">
			@if(Session::get(Auth::id()) == Roole::getRoleId('Studiendekan'))
		   {{ Form::open(array('url' => 'import', 'method' => 'get')) }}
               <button type="submit" class="btn btn-default btn-sm">
                 <span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;
                   Import
               </button>
		  	{{ Form::close() }}
            @endif
		</div>

	  <div class="col-sm-12 col-md-2 col-lg-2">
		 @if(Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
		   {{ Form::open(array('url' => 'createmodule', 'method' => 'get')) }}
          <button type="submit" class="btn btn-default btn-sm">
            <span class="glyphicon glyphicon-plus"></span>
            Add Module
          </button>
		  	{{ Form::close() }}
        @endif			
	  </div>

	</div>
    @if (HelperActions::getViewMode())
    <div id="table-responsive">
            
        <table class="table table-bordered table-striped">
    
        @foreach ($modules as $module)
            @if (($module->visible) == 1)
                {{--*/ $strikecls = ""; /*--}}
            @else
                {{--*/ $strikecls = "strike-text"; /*--}}
            @endif

            @if ((($module->visible) == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Admin')) || ($module->visible == 1))
        <table class="table table-bordered table-striped">
            
                    <tr>
                        <td class="col-md-3 col-lg-3 {{$strikecls}} {{Module::moduleStyleClass($module->module_no)}}" data-title="Module"> Module </td>
                        <td class="col-md-9 col-lg-9 {{$strikecls}} {{Module::moduleStyleClass($module->module_no)}} {{$strikecls}}"> {{ strlen($module->module) > 400 ? HelperActions::makeMultipleLineString($module->module, 400) : $module->module }} </td>
                    </tr>
                    <tr>
                        <td data-title="Exam Id">Exam Id</td>
                        <td class="{{$strikecls}}">{{ strlen($module->examId) > 400 ? HelperActions::makeMultipleLineString($module->examId, 400) : $module->examId }}</td>
                    </tr>
                    <tr>
                        <td data-title="Module Nr">Module Nr</td>
                        <td class="{{$strikecls}}"> {{ $module->module_no }} </td>
                    </tr>
                    <tr>
                        <td data-title="Topic Category">Topic Category</td>
                        <td class="{{$strikecls}}"> {{ Category::getCategoryName($module->topic_category)}} </td>
                    </tr>
                    <tr>
                        <td data-title="Type">Type</td>
                        <td class="{{$strikecls}}"> {{ $module->type }} </td>
                    </tr>
                    <tr>
                        <td data-title="Weekly Composition">Weekly Composition</td>
                        <td class="{{$strikecls}}"> {{ $module->weekly_composition }} </td>
                    </tr>
                    <tr>
                        <td data-title="Exam">Exam</td>
                        <td class="{{$strikecls}}"> {{ $module->exam }} </td>
                    </tr>
                    <tr>
                        <td data-title="Exam Duration">Exam Duration</td>
                        <td class="{{$strikecls}}"> {{ $module->exam_duration }} </td>
                    </tr>
                    <tr>
                        <td data-title="ECTS Credits">ECTS Credits</td>
                        <td class="{{$strikecls}}"> {{ $module->ECTS_credits }} </td>
                    </tr>
                    <tr>
                        <td data-title="Required Hours of Work">Required Hours of Work</td>
                        <td class="{{$strikecls}}"> {{ $module->required_hours_of_work }} </td>
                    </tr>
                    <tr>
                        <td data-title="Person Responsible">Person Responsible</td>
                        <td class="{{$strikecls}}"> {{ $module->person_responsible }} </td>
                    </tr>
                    <tr>
                        <td data-title="Person Responsible 2">Person Responsible 2</td>
                        <td class="{{$strikecls}}"> {{ $module->person_responsible_2 }} </td>
                    </tr>
                    <tr>
                        <td data-title="Semester">Semester</td>
                        <td class="{{$strikecls}}"> {{ $module->semester }} </td>
                    </tr>
                    <tr>
                        <td data-title="Teaching Methods">Teaching Methods</td>
                        <td class="{{$strikecls}}"> {{ $module->teaching_methods }} </td>
                    </tr>
                    <tr>
                        <td data-title="Module Description">Module Description</td>
                        <td class="{{$strikecls}}"> {{ $module->module_description }} </td>
                    </tr>
                    <tr>
                        <td data-title="Miscellaneous Comments">Miscellaneous Comments</td>
                        <td class="{{$strikecls}}"> {{ $module->miscellaneous_Comments }} </td>
                    </tr>
                    <tr>
                        <td data-title="Recommended Prerequisite Knowledge">Recommended Prerequisite Knowledge</td>
                        <td class="{{$strikecls}}"> {{ $module->recommended_prerequisite_knowledge }} </td>
                    </tr>
                    <tr>
                        <td data-title="Recommended Literature">Recommended Literature</td>
                        <td class="{{$strikecls}}"> {{ $module->recommended_literature }} </td>
                    </tr>
                    <tr>
                        <td data-title="Outcomes">Outcomes</td>
                        <td class="{{$strikecls}}"> {{ $module->outcomes }} </td>
                    </tr>
                    <tr>
                        <td data-title="Site">Site</td>
                        <td class="{{$strikecls}}"> {{ Site::getSiteName($module->site)}} </td>
                    </tr>
                    <tr>
                        <td data-title="Site 2">Site 2 </td>
                        <td class="{{$strikecls}}"> {{ Site::getSiteName($module->site_2)}} </td>
                    </tr>
                </table>
                <table class="table table-bordered table-striped">
            
                    <tr>
                            @if(Session::get(Auth::id()) <= Roole::getRoleId('Coordinator'))
                            <td class="col-md-1 col-lg-1">
                                {{--*/ $editUrl = 'editmodules/'.$module->id.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
                              {{ Form::open(array('url' => $editUrl, 'method' => 'post')) }}                    
                                  <button type="submit" class="btn btn-default btn-xs">
                                    <span class="glyphicon glyphicon-edit"></span>
                                    Edit
                                  </button>
                    {{ Form::close() }}
                            </td>
                            <td class="col-md-1 col-lg-1">
                                {{--*/ $logsUrl = 'historylogs/'.$module->id.'/modules/'.$module->module /*--}}
                              {{ Form::open(array('url' => $logsUrl, 'method' => 'post')) }}                    
                                    <button type="submit" class="btn btn-default btn-xs">
                                      <span class="glyphicon glyphicon-time"></span>
                                        Logs
                                    </button>
                    {{ Form::close() }}
                            </td>
                            @endif
                            @if($module->visible == 1 && Session::get(Auth::id()) <= Roole::getRoleId('Coordinator'))
                            <td class="col-md-1 col-lg-1">
                                {{--*/ $delurl = 'deletemodule/'.$module->id.'/'.$module->module /*--}}
                                {{ Form::open(array('url' => $delurl, 'method' => 'post')) }}
                     <button type="submit" class="btn btn-default pull-right btn-xs">
                       <span class="glyphicon glyphicon-remove"></span>
                         Delete 
                    </button>
                {{ Form::close() }}
                            </td>
                            @elseif($module->visible == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Coordinator'))
                            <td class="col-md-1 col-lg-1">
                                {{--*/ $udelurl = 'undodeleteedmodule/'.$module->id.'/'.$module->module /*--}}
                                {{ Form::open(array('url' => $udelurl, 'method' => 'post')) }}
                   <button type="submit" class="btn btn-default pull-right btn-xs">
                    <span class="glyphicon glyphicon-repeat"></span>
                        Undo Delete
                   </button>
                 {{ Form::close() }}
                            </td>
                            @endif
                                                  </tr>
                </table>
            @endif
            <br/>
        @endforeach
    @else
	<div id="table-responsive">
            
        <table class="col-md-12 col-lg-12 table table-hover cf">
            <thead class="cf">
                
                <tr>
                    <td><b>{{ trans('localization.Exam Id') }}</b></td>
                    <td><b>{{ trans('localization.Modules') }}</b></td>
                    <td><b>{{ trans('localization.Credits') }}</b></td>
                    <td><b>{{ trans('localization.Topic Category') }}</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </thead>
            <tbody>
        
            @foreach ($modules as $module)
                @if (($module->visible) == 1)
                    {{--*/ $strikecls = ""; /*--}}
                @else
                    {{--*/ $strikecls = "strike-text"; /*--}}
                @endif

                @if ((($module->visible) == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Admin')) || ($module->visible == 1))
                <tr class = '{{$strikecls}} {{Module::moduleStyleClass($module->module_no)}}'>
                    <td data-title="Id">
                        {{ $module->examId }}
                    </td>
                    <td data-title="Module">
                        {{ strlen($module->module) > 40 ? HelperActions::makeMultipleLineString($module->module, 40) : $module->module }}
                    </td>
                    <td data-title="Credit">
                        {{ $module->ECTS_credits }}
                    </td>

                    <td data-title="Topic Category">
                        {{ Category::getCategoryName($module->topic_category)}}
                    </td>
          @if(Session::get(Auth::id()) <= Roole::getRoleId('Coordinator'))
                    <td>
                {{--*/ $editUrl = 'editmodules/'.$module->id.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
                      {{ Form::open(array('url' => $editUrl, 'method' => 'post')) }}                    
                          <button type="submit" class="btn btn-default btn-xs">
                            <span class="glyphicon glyphicon-edit"></span>
                            Edit
                          </button>
            {{ Form::close() }}
                    </td>

                    <td>
                {{--*/ $logsUrl = 'historylogs/'.$module->id.'/modules/'.$module->module /*--}}
                      {{ Form::open(array('url' => $logsUrl, 'method' => 'post')) }}                    
                            <button type="submit" class="btn btn-default btn-xs">
                              <span class="glyphicon glyphicon-time"></span>
                                Logs
                            </button>
            {{ Form::close() }}
                    </td>
                    @endif

          @if($module->visible == 1 && Session::get(Auth::id()) <= Roole::getRoleId('Coordinator'))
                  <td>
                        {{--*/ $delurl = 'deletemodule/'.$module->id.'/'.$module->module /*--}}
                                    {{ Form::open(array('url' => $delurl, 'method' => 'post')) }}
                       <button type="submit" class="btn btn-default pull-right btn-xs">
                         <span class="glyphicon glyphicon-remove"></span>
                           Delete 
                      </button>
                  {{ Form::close() }}
                  </td>

          @elseif($module->visible == 0 && Session::get(Auth::id()) <= Roole::getRoleId('Coordinator'))
               <td>
                {{--*/ $udelurl = 'undodeleteedmodule/'.$module->id.'/'.$module->module /*--}}
                                {{ Form::open(array('url' => $udelurl, 'method' => 'post')) }}
                   <button type="submit" class="btn btn-default pull-right btn-xs">
                    <span class="glyphicon glyphicon-repeat"></span>
                        Undo Delete
                   </button>
                 {{ Form::close() }}
               </td>
    
               @endif
                </tr>
                @endif
                @endforeach
                @endif

            </tbody>
        </table>
	<div class ="row">
		<?php echo $modules->links(); ?>
	</div>
</div>
@stop
<?php //echo "<pre>"; print_r($mentors); echo "</pre>"; die(); ?>
@extends('layouts.main')
@section('content')
@include('layouts.navBar')
{{ HTML::script('twiter_bootstrap/js/jquery-2.1.3.min.js') }}
{{ HTML::style('twiter_bootstrap/css/bootstrap.min.admin.css') }}
<?php
#$study_plans = StudyPlan::getAllPlans($keyword);
?>
<?php if(Session::has('msg')) {
    $msg = Session::pull('msg', 'default');
?>
<script type="text/javascript">
    $( document ).ready(function() {
        var html = '<span class="glyphicon glyphicon-ok"></span><strong> Success</strong></br><?php echo $msg;?>';
        $('.js-alert-success').show().html(html);
    });
</script>
<?php } ?>
<script type="text/javascript">
    $(function () {
        $("#go").on('click', function () {
            var keyword = $.trim($('#keyword').val());
            location.href =ROOT+ 'select_mentor?keyword=' + keyword;
        });
        $('.js-mentor').on('change', function(){
            /*var _this = $(this);
            var id = $(this).prop('id');
            var is_new = $('#js-mentor-name-'+id).text();
            console.log(is_new.length)
            var msg = is_new.length >1 ? 'Mentor name has been Updated :)' : 'Mentor name has been selected :)';
            var data = {
                'id' : id,
                'mentor' : $(this).val(),
                'msg' : msg
            };
            $.post(ROOT+'update_mentor', data, function(res){
                $('#js-mentor-name-'+data.id).html(data.mentor+'&nbsp;');
                $('.js-alert-success').show().html('<span class="glyphicon glyphicon-ok"></span><strong> Success</strong></br>'+msg);
                _this.closest('tr').removeClass('pink').addClass('green');
                _this.closest('tr').find('td:last').html('<form role="form" action="/itis_db/historylogs/'+id+'/study_plans/Mentor Name" method = "post"><button type="submit" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-edit"></span>Logs</button></form>');
            });*/
            
            var id = $(this).prop('id');
            var is_new = $('#js-mentor-name-'+id).text();
            var msg = is_new.length >1 ? 'Mentor name has been Updated :)' : 'Mentor name has been selected :)';
            $(this).closest('form').find('input[name=msg]').val(msg);
            $(this).closest('form').submit();
        });
    });
</script>
<div class="alert alert-success js-alert-success" style="text-align: center; display:none;background-color: #AEDB43;padding: 6px;">
        </div>
<div class="container">
    <div class="white">        
        <div class="form-horizontal" style="padding-top:20px;">
            <button class="btn btn-default" type="button" onclick="location.href='studyplan';">
                <span class="glyphicon glyphicon-arrow-left" style="margin-right: 10px;"></span>&nbsp;Overview
            </button>
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-default disabled btn-sm"> <b>Students</b> </button>
                      </span>
                      <input placeholder="" class="form-control input-sm" name="searchInput" type="text" id="keyword">
                      <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="button" id="go">
                            <span class="glyphicon glyphicon-search"> </span>&nbsp;
                        </button>
                      </span>
                </div>
            </div>
            <div class="form-group">
                <!--                <label for="inputEmail3" class="col-sm-1 control-label">Keyword</label>-->
                <!-- <div class="col-sm-3">
                    <input type="text" id="keyword" class="form-control" name="titlex">
                </div>
                <div class="col-sm-1">
                    <input type="button" id="go" class="btn btn-info" value="Go">
                </div> -->
            </div>
        </div>
        <div class="paging pull-right">
        </div>
        <h2></h2>
        <table class="col-md-12 col-lg-12 table table-hover cf">
            <thead class="cf">
            <tr>
                <!-- <th>#</th> -->
                <th>Student Name</th>
                <th>Present Mentor</th>
                <th class="actions">Add / Update Mentor</th>
                <th class="actions">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            //$mentors = array('Prof. Dr. Wolfgang Nejdl','Prof. Dr. Wolf-Tilo Balke','Prof. Dr. Sven Hartmann', 'Prof. Dr. Dieter Hogrefe');
            if(!empty($study_plans)){
                foreach ($study_plans as $key=>$item){
                    $serial = $key+1;
                    if(!empty($item->mentor_name)){
                        $class = 'green';
                    } else{
                        $class = 'pink';
                    }
                    ?>
                    <tr class="<?php echo $class?>">
                        <!-- <td><?php echo $serial; ?>&nbsp;</td> -->
                        <td><?php echo $item->first_name . ' ' . $item->last_name; ?>&nbsp;</td>
                        <td id="js-mentor-name-<?php echo $item->id; ?>"><?php echo $item->mentor_name; ?>&nbsp;</td>
                        <td class="actions">
                            <form action="update_mentor" method="post">
                                <input type="hidden" value="<?php echo $item->id; ?>" name="id">
                                <input type="hidden" value="" name="msg">
                                <span class="mentor"><select class="js-mentor" name="mentor">
                            <?php
                                foreach($mentors as $mentor){
                                    /*if(strcasecmp(trim($item->mentor_name),trim($mentor->mentors_name))==0) {
                                        echo '<span class="mentor"><input type="radio" checked name="mentor" class="js-mentor" value="'.$mentor->mentors_name.'" id="'.$item->id.'">'.$mentor->mentors_name.'</span>';
                                    }else
                                        echo '<span class="mentor"><input type="radio" name="mentor" class="js-mentor" value="'.$mentor->mentors_name.'" id="'.$item->id.'">'.$mentor->mentors_name.'</span>';
                                    echo "<br/>";*/

                                    if( $item->mentor_id == $mentor->id ) {
                                        echo '<option selected value="'.$mentor->id.'" id="'.$item->id.'">'.$mentor->mentors_name.'</option>';
                                    }else
                                        echo '<option value="'.$mentor->id.'" id="'.$item->id.'">'.$mentor->mentors_name.'</option>';

                                }
                            ?>
                                </select></span>
                            </form>
                        </td>
                        <td class="actions">
                            <?php
                            if (!empty ($item->mentor_name)) {
                                ?>
                                <form role="form" action="{{url()}}/historylogs/{{$item->id}}/{{'study_plans'}}/{{'Mentor Name'}}" method = "post">
                                <button type="submit" class="btn btn-default btn-xs">
                                    <span class="glyphicon glyphicon-edit"></span>
                                    Logs
                                </button>
                               </form>

                                <?php
                            }
                            ?>

                        </td>
                    </tr>
                <?php
                }
            }else{
                ?>
                <tr><td align="justify">No Result Found</td></tr>
            <?php
            }
            ?>
            </tbody>
        </table>
        <div class="text-right">
            <?php echo $study_plans->links()?>
        </div>
    </div>
</div>

<style type="text/css">
    form {
        margin-bottom: 0px;
    }
    .mentor{
        margin-right: 20px;
    }
    .navbar{
         margin-bottom: 0px !important;
    }
</style>
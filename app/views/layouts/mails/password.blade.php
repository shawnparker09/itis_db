<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<p>
Dear {{ $name }}, <br/>
Please find your new ITISDB password: {{$password}} <br/>
You can login now in this link: <a href="{{ URL::to('/') }}">{{ URL::to('/') }}</a> <br/><br/>
Thank You <br/> <br/>
Best Regards <br/>
ITIS Admin Team
</p>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<p>
Dear {{ $name }}, <br/>
We didn't receive your study plan. Please send your study plan as soon as possible. <br/>
You can login now in this link: <a href="{{ URL::to('/') }}">{{ URL::to('/') }}</a> and submit your study plan. <br/><br/>
<br/><b>N.B</b> This is an auto generated mail. Please don't reply.
Thank You <br/> <br/>
Best Regards <br/>
ITIS Admin Team
</p>
</body>
</html>
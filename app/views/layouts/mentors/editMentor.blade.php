@extends('layouts.main')

@section('content')
@include('layouts.navBar')
 <div class = "container">
  <div class = "raw">

      <div class = "row">
        <div class = "col-sm-11">
            <button class="btn btn-default" type="button" onclick="location.href='showallmentors';">
                <span class="glyphicon glyphicon-arrow-left" style="margin-right: 10px;"></span>&nbsp;Mentors Page
            </button>
        </div>
      </div>

    <h3><center>Update Mentor</center></h3>
    <hr/>
    {{ Form::open(array('url' => 'updatementor', 'class' => 'form-horizontal')) }}
      
      {{ Form::hidden('id', ($mentor->id)) }}

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">Title</label>
        <div class="col-sm-8">
          {{ Form::text('mentor_name', ($mentor->mentors_name), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('mentor_name')) <p class="text-danger"><b>{{ $errors->first('mentor_name') }}</b></p> @endif
        </div>
      </div>

      <button type="submit" class="btn btn-primary pull-right" style = "margin-left: 11px;margin-right: 100px;margin-bottom: 11px;">
        <span class="glyphicon glyphicon-cloud-upload"></span>
        Update Mentor
      </button>

    {{ Form::close() }}
  </div>
</div>
  @stop
@extends('layouts.main')

@section('content')
@include('layouts.navBar')

{{--*/ $msg = HelperActions::getMessageToSession(); /*--}}
@if($msg)
	<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success" style="background-color: #AEDB43;padding: 0;margin-top: -20;">
		<p>
			<p> <b><center>
        <span class="glyphicon glyphicon-ok"></span>
        Success
			</center></b> </p>
			<p><center>{{ $msg }}</center></p>
		</p>
	</div>
@endif
<div class = "container">

    <div class="raw">
        <div style="margin-right: 30px;" class="col-sm-12 col-md-1 col-lg-1">
            <button class="btn btn-default" type="button" onclick="location.href='studyplan';">
                <span class="glyphicon glyphicon-arrow-left" style="margin-right: 10px;"></span>&nbsp;Overview
            </button>
        </div>
        <div class="col-sm-12 col-md-1 col-lg-1">
            {{ Form::open(array('url' => 'creatementor', 'method' => 'get')) }}
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-plus"></span> Add mentor
            </button>
            {{ Form::close() }}
        </div>
    </div>

     <div id="table-responsive">
        <table class="col-md-12 col-lg-12 table table-hover cf">
                <thead class="cf">

				<tr>
					<td><b>Id</b></td>
					<td><b>Name</b></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>


			    </thead>
			    <tbody>
			
				@foreach ($mentors as $mentor)
						<tr>
								<td data-title="Id">
									{{ $mentor->id }}
								</td>
								<td data-title="Name">
									{{ strlen($mentor->mentors_name) > 40 ? HelperActions::makeMultipleLineString($mentor->mentors_name, 25) : $mentor->mentors_name }}
								</td>

                                <td>
                                    {{--*/ $edurl = 'editmentor/'.$mentor->id.'/'.HelperActions::getPageNumber($_SERVER['REQUEST_URI']) /*--}}
                                    {{ Form::open(array('url' => $edurl, 'method' => 'post')) }}
                                      <button type="submit" class="btn btn-default btn-xs">
                                        <span class="glyphicon glyphicon-edit"></span>
                                        Edit
                                      </button>
                                    {{ Form::close() }}
                                </td>
                                <td>
                                    {{--*/ $deleteUrl = 'deletementor/'.$mentor->id /*--}}
                                    {{ Form::open(array('url' => $deleteUrl, 'method' => 'post')) }}
                                     <button type="submit" class="btn btn-default  btn-xs pull-right">
                                       <span class="glyphicon glyphicon-remove"></span>
                                        Delete
                                     </button>
                                    {{ Form::close() }}
                                </td>
						</tr>
			    @endforeach
			
			    </tbody>
		</table>
	
	</div>
	<br/>
	<div class="raw">
		<?php echo $mentors->links(); ?>
	</div>
</div>
@stop
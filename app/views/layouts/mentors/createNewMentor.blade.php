@extends('layouts.main')

@section('content')
@include('layouts.navBar')

<div class = "container">
  <div class = "raw">
      <button class="btn btn-default" type="button" onclick="location.href='showallmentors';">
          <span class="glyphicon glyphicon-arrow-left" style="margin-right: 10px;"></span>&nbsp;Mentors Page
      </button>
  </div>

  <div class = "raw">
    <h3> <center>Create New Mentor</center> </h3>
    <hr/>
    {{ Form::open(array('url' => 'creatementor', 'method' => 'post', 'class' => 'form-horizontal')) }}
      
      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">Mentor name</label>
        <div class="col-sm-8">
          {{ Form::text('mentor_name', Input::old('mentor_name'), array('placeholder' => '', 'class' =>'form-control ')) }}
             @if ($errors->has('mentor_name')) <p class="text-danger"><b>{{ $errors->first('mentor_name') }}</b></p> @endif
        </div>
      </div>
   
        <button type="submit" class="btn btn-primary pull-right" style = "margin-left: 11px;margin-right: 100px;margin-bottom: 11px;">
          <span class="glyphicon glyphicon-ok"></span>
          Create New Mentor
        </button>

        <button type="reset" action = "" class="btn btn-warning pull-right">
          <span class="glyphicon glyphicon-refresh"></span>
          Reset
        </button>
    {{ Form::close() }}
  </div>
</div>
@stop
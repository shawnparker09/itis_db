@extends('layouts.main')

@section('content')
@include('layouts.navBar')
<div class="container" xmlns="http://www.w3.org/1999/html">
<div class="col-md-12 js-instruction">
    <h3 class="text-center"><b>Instruction</b></h3>
        <p><h4>1. Module.csv should be imported first because coursees need module. Hence, without module we can not import courses.</h4></p>
        <p><h4>2. Afterwards, we can import course.csv file. </h4></p>
        <p><h4>3. In all cases, the format of the csv file should be like "course.csv" not like "course(1).csv" or "course1.csv" or "Course.csv".</h4></p>
        <p><h4>4. Each row of database table is unique. Hence, can not enter dulicate row. </h4></p>
    </p> 
 </div>
</div>
<div class = "col-sm-2"> </div>
<div class = "col-sm-8" style = "padding-top: 150px;">
 	<div class="well">
 			<h3>
 				Upload CSV 
 				<span class="glyphicon glyphicon-cloud-upload"></span>
 			</h3>
    	{{ Form::open(array('url'=>'/upload','method'=>'POST', 'files'=>true)) }}

    	<div style = "padding-top: 15px;">
				<table class="table">
					<tr>
						<td>
	        		{{ Form::file('toImport', array('class'=>'btn btn-lg btn-primary')) }}
						</td>
						<td>
			      	{{ Form::submit('Import', array('class'=>'btn btn-lg btn-success')) }}
					  	<p>{{$errors->first('toImport')}}</p>
						</td>
					</tr>
				</table>
    	</div>

      {{ Form::close() }}
	</div>
</div>
<div class = "col-sm-2"> </div>

@stop


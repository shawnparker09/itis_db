@extends('layouts.main')

@section('content')
@include('layouts.navBar')
<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger" style="background-color: #FF6E6E; padding: 6px; margin-top: -20;">
	<center>
    	<span class="glyphicon glyphicon-remove"></span>
			You have {{sizeof($errors)}} error(s) <br/> Please correct these error(s) and try again
	</center>
</div>

<br/>
<div class = "col-sm-2"> </div>
<div class = "col-sm-8">
	<table class="table table-striped">
		<thead>
		  <tr>
		     <th>Row Nr.</th>
		     <th>Column</th>
		     <th>Error</th>
		  </tr>
	 </thead>
		@foreach ($errors as $e)
			<tr>
				<td> {{ $e[0] }} </td>
				<td> {{ $e[1] }} </td>
				<td> {{ $e[2] }} </td>
			</tr>
		@endforeach
	</table>
		<br/>
		<br/>
		<br/>
		<br/>
</div>
<div class = "col-sm-2"> </div>

@stop


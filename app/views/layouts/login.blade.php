@extends('layouts.main')

@section('content')
<div class="col-sm-3"></div>
<div class="col-sm-6">

<div style = "padding-top: 180;" class="form-horizontal">
	{{ Form::open(array('url' => 'login', 'class' => 'form-horizontal')) }}
		
		<h1 style = "padding-left: 91px;padding-bottom: 10px;">
			Login
			<span class = "glyphicon glyphicon-log-in"></span>
		</h1>

		<!-- if there are login errors, show them here -->
		<p>
			{{ $errors->first('email') }}
			{{ $errors->first('password') }}
		</p>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<div class="form-group">
			 <label for="email" class="col-sm-2 control-label">Email</label>
			<div class = "col-sm-10">
				{{ Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com', 'class' =>'form-control')) }}
			</div>
		</div>

			<div class="form-group">
			 <label for="password" class="col-sm-2 control-label">Password</label>
			<div class = "col-sm-10">
				{{ Form::password('password', array('class' =>'form-control')) }}
			</div>
		</div>

		<br>
		<div style = "" class = "pull-right">
			<p>{{ Form::submit('Login', ['class' => 'btn btn-primary btn-sm']) }}
			{{ Form::button('Cancel', ['class' => 'btn btn-default btn-sm']) }}</p>
		</div>
	{{ Form::close() }}

</div>
</div>
<div class="col-sm-3"></div>
@stop
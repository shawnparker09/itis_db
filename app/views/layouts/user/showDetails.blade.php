@extends('layouts.main')

@section('content')
@include('layouts.navBar')

<div class = "container">
	
	<div class="raw">
		<div class="col-sm-12 col-md-1 col-lg-1">
	    <form action="/itis_db/createuser">
	      <button type="submit" class="btn btn-default btn-sm">
	        <span class="glyphicon glyphicon-plus"></span> Add User
	      </button>
	    </form>
		</div>

		<div class="col-sm-12 col-md-3 col-lg-3">
			<form class="form-inline" action="/itis_db/showallusers" method = "post">
		    <div class="input-group">
		      <span class="input-group-btn">
		        <button class="btn btn-default disabled btn-sm"> <b>Users of ITIS</b> </button>
		      </span>		    	
		      {{ Form::text('searchInput', null, array('placeholder' => '', 'class' =>'form-control input-sm')) }}
		      <span class="input-group-btn">
		        <button class="btn btn-default btn-sm" type="submit">
		        	<span class="glyphicon glyphicon-search"> </span>&nbsp;
		        </button>
		      </span>	
		    </div>
			</form>			
		</div>


		<div class="col-sm-12 col-md-3 col-lg-3">
			<form class="form-inline" action="/itis_db/sortbyrequser" method = "post">
				<div class="form-group">
	          {{ Form::select('siteName', Site::makeSiteArray(),
                          null, array('class' =>'form-control input-sm')) }}
			  </div>
			  <button type="submit" class="btn btn-default btn-sm">
			  	<span class = "glyphicon glyphicon-refresh"></span>&nbsp;
			  </button>		  
			</form>
		</div>

		<div class="col-sm-12 col-md-2 col-lg-2">
			<form class="form-inline" action="/itis_db/sortbyrequser" method = "post">
				<div class="form-group">
	        {{ Form::select('roleType', array(
																						''  => 'Role Type',
                                            '1' => 'Super Admin',
                                            '2' => 'Admin',
                                            '3' => 'Professor', 
                                            '4' => 'Assistant',
                                            '5' => 'Student'
	                                      ), 
	                         null, array('class' =>'form-control input-sm')) }}
			  </div>
			  <button type="submit" class="btn btn-default btn-sm">
			  	<span class = "glyphicon glyphicon-refresh"></span>&nbsp;
			  </button>		  
			</form>			
		</div>

		<div class="col-sm-12 col-md-2 col-lg-2">
      <form class="form-inline" action="/itis_db/exportuser" method = "post">
        <div class="form-group">
          {{ Form::select('exportType', array(
                                            ''  => 'Export',
                                            'xml' => 'Xml',
                                            'csv' => 'Csv',
                                            'pdf' => 'PDF',
                                        ), 
                           null, array('class' =>'form-control input-sm')) }}
        </div>
        <button type="submit" class="btn btn-default btn-sm">
          <span class = "glyphicon glyphicon-cloud-download"></span>&nbsp;
        </button>     
      </form>			
	   </div>

	   <div class="col-sm-12 col-md-1 col-lg-1">
			@if(Session::get(Auth::id()) == 1)
				<form action="/itis_db/import">
               <button type="submit" class="btn btn-default btn-sm">
                 <span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;
                   Import
               </button>
             </form>
            @endif
		</div>

	</div>

	<div class="raw" id="no-more-tables">
			
		<table class="col-md-12 table cf">
			<thead class="cf">
				<tr>
					<td><b>Title</b></td>
					<td><b>First Name</b></td>
					<td><b>Last Name</b></td>
					<td><b>Email</b></td>
					<td><b>Role</b></td>
					<td><b>University</b></td>
					<td><b>Institute URL</b></td>
					<td><b>personal_website_url</b></td>
					<td><b>photo_url</b></td>
					<td><b>location</b></td>
					<td><b>academic_career</b></td>
					<td><b>occupation</b></td>
					<td><b>Cooperation_practice</b></td>
				</tr>
			</thead>
			<tbody>
				@foreach ($users as $user)
						@if (($user->visible) == 1)
							<tr class="bg-info">
						@elseif (($user->visible) == 0)
							<tr class="bg-warning">
						@endif
								<td data-title="Title">
									{{ $user->title }}
								</td>
								<td data-title="First Name">
									{{ $user->first_name }}
								</td>
								<td data-title="Last Name">
									{{ $user->last_name }}
								</td>
								<td data-title="Email">
									{{ $user->email }}
								</td>
								<td data-title="Role">
									@if (($user->roole_id) == 1)
									    Super Admin
									@elseif (($user->roole_id) == 2)
									    Admin
									@elseif (($user->roole_id) == 3)
									    Professor
									@elseif (($user->roole_id) == 4)
									    Assistant
									@elseif (($user->roole_id) == 5)
									    Student
									@endif
								</td>
								<td data-title="University">
									{{ Site::getSiteName($user->university)}}
								</td>
								<td data-title="Institute URL">
						      {{HTML::link(($user->institute_url), 'Official Website')}}
								</td>
								<td data-title="personal_website_url">
									{{ $user->personal_website_url }}
								</td>
								<td data-title="photo_url">
									{{ $user->photo_url }}
								</td>
								<td data-title="location">
									{{ $user->location }}
								</td>
								<td data-title="academic_career">
									{{ $user->academic_career}}
								</td>
								<td data-title="occupation">
									{{ $user->occupation}}
								</td>
								<td data-title="Cooperation_practice">
									{{ $user->Cooperation_practice}}
								</td>
                                
                                <td>
									<form role="form" action="/itis_db/showallusers" method = "post">
										  <button type="submit" class="btn btn-default btn-xs">
										  	<span class="glyphicon glyphicon-edit" type="submit" ></span>
										  	Overview
										  </button>
										</form>
									</td>
									<td>

								@if (Session::get(Auth::id()) <= $user->roole_id)
									<td>
										<form role="form" action="edituser/{{$user->id}}" method = "post">
										  <button type="submit" class="btn btn-default btn-xs">
										  	<span class="glyphicon glyphicon-edit" type="submit" ></span>
										  	Edit
										  </button>
										</form>
									</td>
									<td>
										<form role="form" 
													action="/itis_db/historylogs/{{$user->id}}/{{'users'}}/{{User::userMitName($user->id)}}" 
													method = "post">
										  <button type="submit" class="btn btn-default btn-xs">
										  	<span class="glyphicon glyphicon-time"></span>
										  	Logs
										  </button>
										</form>										
									</td>
								@endif
						</tr>
				@endforeach
								
			</tbody>
		</table>
	</div>

	
	<div class = "raw">
		<?php echo $users->links(); ?>
	</div>
</div>


@stop
@extends('layouts.main')

@section('content')
<div class = "col-sm-3"></div>

<div class = "col-sm-6">
  <br/>
  <br/>
  <br/>
  <center><h3 style = "">Please Create a SuperAdmin</h3></center>
  <hr/>
  {{ Form::open(array('url' => 'createusersuperadmin2412008900', 'method' => 'post', 'class' => 'form-horizontal')) }}
    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Title</label>
      <div class="col-sm-8">
      {{ Form::text('title', Input::old('title'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('title')) <p class="text-danger"><b>{{ $errors->first('title') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group ">
      <label for="" class = "col-sm-3 control-label">First Name</label>
      <div class="col-sm-8">
        {{ Form::text('first_name', Input::old('first_name'), array('placeholder' => '', 'class' =>'form-control ')) }}
           @if ($errors->has('first_name')) <p class="text-danger"><b>{{ $errors->first('first_name') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Last Name</label>
      <div class="col-sm-8">
        {{ Form::text('last_name', Input::old('last_name'), array('placeholder' => '', 'class' =>'form-control ')) }}
           @if ($errors->has('last_name')) <p class="text-danger"><b>{{ $errors->first('last_name') }}</b></p> @endif
      </div>
    </div>


    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Email</label>
      <div class="col-sm-8">
      {{ Form::text('email', Input::old('email'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('email')) <p class="text-danger"><b>{{ $errors->first('email') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Institute</label>
      <div class="col-sm-8">
      {{ Form::text('institute', Input::old('institute'), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('institute')) <p class="text-danger"><b>{{ $errors->first('institute') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group ">
      <label for="" class = "col-sm-3 control-label">University</label>
      <div class="col-sm-8">
        {{ Form::select('university', array(
                                              '' => '',
                                              'TU Clausthal' => 'TU Clausthal', 
                                              'TU Braunschweig' => 'TU Braunschweig',
                                              'Hannover' => 'Hannover',
                                              'Gottingen' => ' Gottingen'), 
                                              Input::old('university'), array('class' =>'form-control ')) }}
        @if ($errors->has('university')) <p class="text-danger"><b>{{ $errors->first('university') }}</b></p> @endif                                              
      </div>
    </div> 


      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">Password</label>
        <div class="col-sm-8">
          {{ Form::password('newPassword', array('class' => 'form-control')) }}
            @if ($errors->has('password')) <p class="text-danger"><b>{{ $errors->first('password') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">ReType Password</label>
        <div class="col-sm-8">
          {{ Form::password('conformPassword', array('class' => 'form-control')) }}
            @if ($errors->has('conformPassword')) <p class="text-danger"><b>{{ $errors->first('conformPassword') }}</b></p> @endif
        </div>
      </div> 

    <span class = "pull-right" style = "padding-right: 121px;">
      <button type="submit" class="btn btn-md green-background">
        <span class="glyphicon glyphicon-user"></span>
        Create New User
      </button>

      <button type="reset" action = "" class="btn btn-md red-background">
        <span class="glyphicon glyphicon-refresh"></span>
        Reset
      </button>
    </span>
  {{ Form::close() }}
</div>
<div class = "col-sm-3"></div>
@stop
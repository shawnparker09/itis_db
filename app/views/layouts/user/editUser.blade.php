@extends('layouts.main')

@section('content')
@include('layouts.navBar')
 <div class = "container">
  <div class = "raw">

      <div class = "row">
        <div class = "col-sm-11">

            @if(Session::get(Auth::id()) <=  Roole::getRoleId('Admin'))
            <div class = "col-xs-3 pull-right">
              {{--*/ $passwordUrl = 'generatePasswordAndSend/'.$user['id'] /*--}}
              {{ Form::open(array('url' => $passwordUrl, 'class' => 'form-horizontal', 'method' => 'post')) }}
                <button type="submit" class="btn btn-default btn-xs pull-right">
                  <span class="glyphicon glyphicon-cog"></span>
                    Generate Password  send to user
                </button>
              {{ Form::close() }}
            </div>        
            @endif

            @if($user['id'] == Auth::id())
            <div class = "col-xs-3 pull-right">
              {{ Form::open(array('url' => 'editpassword', 'class' => 'form-horizontal', 'method' => 'get')) }}
                <button type="submit" class="btn btn-default btn-xs pull-right">
                  <span class="glyphicon glyphicon-edit"></span>
                    Edit Password
                </button>
              {{ Form::close() }}
            </div>
            @endif

        </div>
      </div>

    <h3><center>Update User</center></h3>
    <hr/>
    {{ Form::open(array('url' => 'updateuser', 'class' => 'form-horizontal')) }}
      
      {{ Form::hidden('id', ($user['id'])) }} 

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">Title</label>
        <div class="col-sm-8">
          {{ Form::text('title', ($user['title']), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('title')) <p class="text-danger"><b>{{ $errors->first('title') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">First Name</label>
        <div class="col-sm-8">
          {{ Form::text('first_name', ($user['first_name']), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('first_name')) <p class="text-danger"><b>{{ $errors->first('first_name') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">Last Name</label>
        <div class="col-sm-8">
          {{ Form::text('last_name', ($user['last_name']), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('last_name')) <p class="text-danger"><b>{{ $errors->first('last_name') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">Email</label>
        <div class="col-sm-8">
          {{ Form::text('email', ($user['email']), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('email')) <p class="text-danger"><b>{{ $errors->first('email') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">Telephone Number</label>
        <div class="col-sm-8">
          {{ Form::text('telephone', ($user['telephone']), array('placeholder' => '', 'class' =>'form-control ')) }}
          @if ($errors->has('telephone')) <p class="text-danger"><b>{{ $errors->first('telephone') }}</b></p> @endif
        </div>
      </div>

      @if (Session::get(Auth::id()) <= Roole::getRoleId('Admin'))
        <div class="form-group ">
          <label for="" class = "col-sm-3 control-label">Role</label>
          <div class="col-sm-8">
              {{ Form::select('role_id', Roole::getRooles(), 
                             ($user['roole_id']), array('class' =>'form-control')) }}
              @if ($errors->has('role_id')) <p class="text-danger"><b>{{ $errors->first('role_id') }}</b></p> @endif
          </div>
        </div>
      @endif  

      <div class="form-group">
        <label for="" class = "col-sm-3 control-label">Institute</label>
        <div class="col-sm-8">
        {{ Form::text('institute', ($user['institute']), array('placeholder' => '', 'class' =>'form-control ')) }}
           @if ($errors->has('institute')) <p class="text-danger"><b>{{ $errors->first('institute') }}</b></p> @endif
        </div>
      </div>

      <div class="form-group ">
        <label for="" class = "col-sm-3 control-label">University</label>
        <div class="col-sm-8">
          {{ Form::select('university', Site::makeSiteArray(), 
                         ($user['university']), array('class' =>'form-control ')) }}
                                        
          @if ($errors->has('university')) <p class="text-danger"><b>{{ $errors->first('university') }}</b></p> @endif   
        </div>
      </div>

      <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Institute_URL</label>
      <div class="col-sm-8">
      {{ Form::text('institute_url', ($user['institute_url']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('institute_url')) <p class="text-danger"><b>{{ $errors->first('institute_url') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Personal Website</label>
      <div class="col-sm-8">
      {{ Form::text('personal_website_url', ($user['personal_website_url']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('personal_website_url')) <p class="text-danger"><b>{{ $errors->first('personal_website_url') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Photo_URL</label>
      <div class="col-sm-8">
      {{ Form::text('photo_url', ($user['photo_url']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('photo_url')) <p class="text-danger"><b>{{ $errors->first('photo_url') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Location</label>
      <div class="col-sm-8">
      {{ Form::text('location', ($user['location']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('location')) <p class="text-danger"><b>{{ $errors->first('location') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Academic Career</label>
      <div class="col-sm-8">
      {{ Form::text('academic_career', ($user['academic_career']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('academic_career')) <p class="text-danger"><b>{{ $errors->first('academic_career') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Occupation</label>
      <div class="col-sm-8">
      {{ Form::text('occupation', ($user['occupation']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('occupation')) <p class="text-danger"><b>{{ $errors->first('occupation') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Research Development Projects</label>
      <div class="col-sm-8">
      {{ Form::text('research_development_projects', ($user['research_development_projects']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('research_development_projects')) <p class="text-danger"><b>{{ $errors->first('research_development_projects') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Cooperation Practice</label>
      <div class="col-sm-8">
      {{ Form::text('Cooperation_practice', ($user['Cooperation_practice']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('Cooperation_practice')) <p class="text-danger"><b>{{ $errors->first('Cooperation_practice') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Patents Intellectual Property</label>
      <div class="col-sm-8">
      {{ Form::text('Patents_intellectual_property', ($user['Patents_intellectual_property']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('Patents_intellectual_property')) <p class="text-danger"><b>{{ $errors->first('Patents_intellectual_property') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Publications</label>
      <div class="col-sm-8">
      {{ Form::text('num_of_publications', ($user['num_of_publications']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('num_of_publications')) <p class="text-danger"><b>{{ $errors->first('num_of_publications') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Selected Publications</label>
      <div class="col-sm-8">
      {{ Form::text('selected_publications', ($user['selected_publications']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('selected_publications')) <p class="text-danger"><b>{{ $errors->first('selected_publications') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Activity</label>
      <div class="col-sm-8">
      {{ Form::text('activity', ($user['activity']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('activity')) <p class="text-danger"><b>{{ $errors->first('activity') }}</b></p> @endif
      </div>
    </div>

    <div class="form-group">
      <label for="" class = "col-sm-3 control-label">Post Address</label>
      <div class="col-sm-8">
      {{ Form::text('post_address', ($user['post_address']), array('placeholder' => '', 'class' =>'form-control ')) }}
         @if ($errors->has('post_address')) <p class="text-danger"><b>{{ $errors->first('post_address') }}</b></p> @endif
      </div>
    </div> 

      <button type="submit" class="btn btn-primary pull-right" style = "margin-left: 11px;margin-right: 100px;margin-bottom: 11px;">
        <span class="glyphicon glyphicon-cloud-upload"></span>
        Update User
      </button>

    {{ Form::close() }}
    
    @if(($user['visible'] == 1) && (Auth::id() != $user['id']))
        {{--*/ $deurl = 'deleteuser/'.$user['id'] /*--}}
        {{ Form::open(array('url' => $deurl)) }}
          <button type="submit"  class="btn btn-warning pull-right">
            <span class="glyphicon glyphicon-remove"></span>
            Delete User
          </button>
        {{ Form::close() }}

      @elseif(($user['visible'] == 0) && (Auth::id() != $user['id']))
        {{--*/ $udeurl = 'undodeleteeduser/'.$user['id'] /*--}}
        {{ Form::open(array('url' => $udeurl)) }}
          <button type="submit"  class="btn btn-warning pull-right">
            <span class="glyphicon glyphicon-repeat"></span>
            Undo Delete 
          </button>
        {{ Form::close() }}
    @endif  
  </div>
</div>
  @stop
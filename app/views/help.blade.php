@extends('layouts.main')
@section('content')
@include('layouts.navBar')

<div class="container" xmlns="http://www.w3.org/1999/html">
<div class="col-md-12 js-instruction">
    <h3 class="text-center">Help</h3>
    <p>
        Only the Professors, Assistant and the students of <b>Internet Technologies and Information Systems</b> can use the information of ITISDB website. There are five type of User roles.
    </p>  

    <ol>
        <li class=""><strong>Studiendekan</strong> </li>
        <li class=""><strong>Admin</strong> </li>
        <li class=""><strong>Coordinator</strong> </li>
        <li class=""><strong>Professor</strong> </li>
        <li class=""><strong>Assistant</strong> </li>
        <li class=""><strong>Student</strong> </li>
        <li class=""><strong>Candidate</strong> </li>
        
    </ol>
    <hr>
    <p> Each User can have only one role. Each role has fixed rights. No one can access the website more than their right. </p>
    <ol>
        <li class=""><strong>Studiendekan:</strong> Studiendekan can access the following things.</li>
        <ol type="a">
          <li>Can see and edit all user's details.</li>
          <li>Can see and edit all course's details.</li>
          <li>Can see and edit all module's details. </li>
          <li>Can see and use logs that contains details of all changes</li>
          <li>Can export tables data and also import data into table</li>
        </ol>
        <br/>
        <li class=""><strong>Admin:</strong> Admin can access the following things.</li>
        <ol type="a">
          <li>Can see and edit all user's details expect Studiendekan.</li>
          <li>Can see and edit all course's details.</li>
          <li>Can see and edit all module's details. </li>
          <li>Can see and use logs that contains details of all changes</li>
          <li>Can export tables data</li>
        </ol>
        <li class=""><strong>Coordinator:</strong> Coordinator can access the following things.</li>
        <ol type="a">
          <li>Can see all contact details.</li>
          <li>Can see and edit all course's details.</li>
          <li>Can see and edit all module's details. </li>
          <li>Can export tables data</li>
        </ol>
        <li class=""><strong>Professor:</strong> Professor can access the following things.</li>
        <ol type="a">
          <li>Can see all contact details.</li>
          <li>Can see and edit all course's details.</li>
          <li>Can see and edit all module's details. </li>
          <li>Can export tables data</li>
        </ol>
        <li class=""><strong>Assistant:</strong> Assistant can access the following things.</li>
        <ol type="a">
          <li>Can see all contact details.</li>
          <li>Can see and edit all course's details.</li>
          <li>Can see and edit all module's details. </li>
          <li>Can export tables data</li>
        </ol>
        <li class=""><strong>Student:</strong> Student can access the following things.</li>
        <ol type="a">
          <li>Can see all contact details.</li>
          <li>Can see all course's details.</li>
          <li>Can create and manage study plan with a Professor.</li>
        </ol>
        <li class=""><strong>Candidate:</strong> Candidate can access the following things.</li>
        <ol type="a">
          <li>Can see all contact details.</li>
          <li>Can see all course's details.</li>
        </ol>         
    </ol>

</div>
</div>
@stop
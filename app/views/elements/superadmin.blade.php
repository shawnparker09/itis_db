{{ HTML::style('twiter_bootstrap/css/bootstrap.min.admin.css') }}
<!-- {{ HTML::style('twiter_bootstrap/css/style_admin.css') }} -->

<?php 
#$study_plans = StudyPlan::getAllPlans($keyword);
$msg = '';
$display = 'display:none;';
if(Session::has('mail_success')) {
    $mail_success = Session::pull('mail_success', 'default');
    if($mail_success) { 
            $msg = '<span class="glyphicon glyphicon-ok"></span><strong> Success</strong></br>Email has been sent.';                      
            $display = 'display:block;     background-color: #f2dede !important;color: #3c763d; ';
    }
    else{        
        //$msg = '<span class="glyphicon glyphicon-warning-sign"></span><strong> Failed</strong></br>No Email to send.';
        $msg = '<span class="glyphicon glyphicon-warning-sign"></span><strong> Failed</strong></br>No Email to send.';
        $display = 'display:block; background-color: #fcf8e3; color: black; ';
    }
    
}
?>
    <script type="text/javascript">
        /*$( document ).ready(function() {
            var html = '<span class="glyphicon glyphicon-ok"></span><strong> Success</strong></br><?php echo $msg;?>';
            $('.js-alert-success').show().html(html);
        });*/
    </script>

<?php //} ?>

<script type="text/javascript">
    $(function () {
        $("#go").on('click', function () {
            var keyword = $.trim($('#keyword').val());
            location.href =ROOT +'studyplan?keyword=' + keyword;
        });
        /*$("#mail").on('click', function () {
            $.get(ROOT+'mail',function(response){
                if(response.success){
                     $('.js-alert-success').show().html('<span class="glyphicon glyphicon-ok"></span><strong> Success</strong></br>Email has been sent.');
                }
            },'json');
        });*/
    });
</script>
<div class="alert alert-success js-alert-success" style="text-align: center; <?php echo $display?> padding: 6px;">
    <?php echo $msg?>
</div>
<div class="container">
    <div class="white">    
        <div class="form-horizontal" style="padding-top:20px;">

        <div class="col-sm-12 col-md-4 col-lg-4">            
            <div class="input-group">
              <span class="input-group-btn">
                <button class="btn btn-default disabled btn-sm"> <b>Students</b> </button>
                  </span>               
                  <input placeholder="" class="form-control input-sm" name="searchInput" type="text" id="keyword">
                  <span class="input-group-btn">
                    <button class="btn btn-default btn-sm" type="button" id="go">
                    <span class="glyphicon glyphicon-search"> </span>&nbsp;
                </button>
              </span>   
            </div>            
        </div>
            <div class="form-group">
                <!--                <label for="inputEmail3" class="col-sm-1 control-label">Keyword</label>-->                

                <!-- <div class="col-sm-3">
                    <input type="text" id="keyword" class="form-control" name="titlex">
                </div>
                <div class="col-sm-1">
                    <input type="button" id="go" class="btn btn-info" value="Go">
                </div> -->
                <?php
                $sessionRole = UsersRoole::getRooleId(Auth::id());
                $role_id = $sessionRole['roole_id'];
                if ($role_id == Roole::getRoleId('Studiendekan') || $role_id == Roole::getRoleId('Admin')) {
                ?>
                    <div class="pull-right col-sm-2">
                        <a href="showallmentors">
                            <button type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-ok"></span>
                                All Mentors
                            </button>
                        </a>
                    </div>

                <?php
                }
                if ($role_id == Roole::getRoleId('Studiendekan') || $role_id == Roole::getRoleId('Admin')) {
                    ?>
    				<div class="pull-right col-sm-2">
                        <a href="mail"  id="mail">
                        <button type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-envelope"></span>
                            Mail Reminder 
                      </button>
                        </a>
                    </div>
                <?php
                }
                if ($role_id <= Roole::getRoleId('Coordinator')) {
                    ?>
                    <div class="pull-right col-sm-2">                
                        <a href="select_mentor">
                        <button type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-ok"></span>
                            Select Mentor
                          </button>
                        </a>
                    </div>
                     <?php
                }
                ?>
            </div>
        </div>
        <div class="paging pull-right">
        </div>
        <h2></h2>
        <table class="col-md-12 col-lg-12 table table-hover cf">
            <thead class="cf">
            <tr>
                <!-- <th>#</th> -->
                <th>Student Name</th>
                <th>Matriculation</th>
                <th>Mentor Name</th>
                <th>Created Date</th>
                <th>Modified Date</th>
                <th class="actions">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($study_plans)){
                foreach ($study_plans as $key => $item):
                    $serial = $key+1;
                    if(!empty($item->id) && !empty($item->mentor_name)){
                        $class = 'background-color:green !important;';
                    } elseif(!empty($item->id) && empty($item->mentor_name)){
                        $class = 'background-color: red  !important;';
                    } else{
                       $class = 'background-color:#f5f5f5 !important;';
                    }
                    $rool = UsersRoole::getRool($item->user_table_id);
                    #BaseController::_setTrace($rool);
                    $sessionRole = UsersRoole::getRooleId(Auth::id());
                    $role_id = $sessionRole['roole_id'];
                    if($role_id >= Roole::getRoleId('Professor')) {
                        if($rool=='Student' && $item->mentor_id==Auth::id() ){
                    ?>
                    <tr style="<?php echo $class?>">
                        <!-- <td><?php echo $serial ?>&nbsp;</td> -->
                        <td><?php echo $item->first_name . ' ' . $item->last_name; ?>&nbsp;</td>
                        <td><?php echo $item->matriculation; ?>&nbsp;</td>
                        <td><?php echo $item->mentor_name; ?>&nbsp;</td>
                        <td><?php echo date_format(date_create($item->created_at), 'd M Y'); ?>&nbsp;</td>
                        <td><?php echo date_format(date_create($item->updated_at), 'd M Y'); ?>&nbsp;</td>
                        <td class="actions">
                            <?php
                                if(!empty($item->user_id)){
                                    ?>
                                    <a href="editstudyplan/<?php echo $item->user_id ?>">
                                        <button type="submit" class="btn btn-default btn-xs">
                                            <span class="glyphicon glyphicon-edit"></span>
                                            Edit Plan
                                        </button>
                                    </a>
                                     <!--<form role="form" action="{{url()}}/historylogs/{{$item->id}}/{{'study_plans'}}/{{'Study Plan'}}" method = "post">
                                        <button type="submit" class="btn btn-default btn-xs">
                                            <span class="glyphicon glyphicon-edit"></span>
                                            Logs
                                        </button>
                                    </form>-->
                                    <?php
                                }else{
                                    echo '<span class="label label-warning">Study Plan hasn\'t been submit yet!</span>';
                                }
                            ?>
                        </td>
                    </tr>
                    <?php
                    }
                    } else {
                        if($rool=='Student' ){
                        ?>
                        <tr style="<?php echo $class?>">
                            <!-- <td><?php echo $serial ?>&nbsp;</td> -->
                            <td><?php echo $item->first_name . ' ' . $item->last_name; ?>&nbsp;</td>
                            <td><?php echo $item->matriculation; ?>&nbsp;</td>
                            <td><?php echo $item->mentor_name; ?>&nbsp;</td>
                            <td><?php echo date_format(date_create($item->created_at), 'd M Y'); ?>&nbsp;</td>
                            <td><?php echo date_format(date_create($item->updated_at), 'd M Y'); ?>&nbsp;</td>
                            <td class="actions">
                                <?php
                                if(!empty($item->user_id)){
                                    ?>
                                    <a href="editstudyplan/<?php echo $item->user_id ?>">
                                        <button type="submit" class="btn btn-default btn-xs">
                                            <span class="glyphicon glyphicon-edit"></span>
                                            Edit Plan
                                        </button>
                                    </a>
                                    <!--<form role="form" action="{{url()}}/historylogs/{{$item->id}}/{{'study_plans'}}/{{'Study Plan'}}" method = "post">
                                       <button type="submit" class="btn btn-default btn-xs">
                                           <span class="glyphicon glyphicon-edit"></span>
                                           Logs
                                       </button>
                                   </form>-->
                                <?php
                                }else{
                                    echo '<span class="label label-warning">Study Plan hasn\'t been submit yet!</span>';
                                }
                                ?>
                            </td>
                        </tr>
                    <?php
                    }
                    }
                endforeach;
            }else{
                ?>
                <tr><td align="justify">No Result Found</td></tr>
            <?php
            }
            ?>
            </tbody>
        </table>
        <div class="text-right">
            <?php echo $study_plans->links()?>
        </div>
    </div>
</div>
<style type="text/css">
    .actions a{
        float:left;
    }
    form {
        margin-bottom: 0px;
        float:left;
    }
    table tr td {
        color: black;
    }
    .navbar{
         margin-bottom: 0px !important;
    }
</style>